package com.wadia.iise.codesmelldetectors;

//http://msdn.microsoft.com/en-us/magazine/dn519928.aspx
public class ItemSet {
	public int N; // data items are [0..N-1]
    public int k; // number of items
    public int[] data; // ex: [0 2 5]
    public int hashValue; // "0 2 5" -> 520 (for hashing)
    public int ct; // num times this occurs in transactions

    public ItemSet(int N, int[] items, int ct)
    {
      this.N = N;
      this.k = items.length;
      this.data =  items.clone();
      this.hashValue = ComputeHashValue(items);
      this.ct = ct;
    }

    private static int ComputeHashValue(int[] data)
    {
      int value = 0;
      int multiplier = 1;
      for (int i = 0; i < data.length; ++i) // actually working backward
      {
        value = value + (data[i] * multiplier);
        multiplier = multiplier * 10;
      }
      return value;
    }
    
    public boolean IsSubsetOf(int[] trans)
    {
      // 'trans' is an ordered transaction like [0 1 4 5 8]
      int foundIdx = -1;
      for (int j = 0; j < this.data.length; ++j)
      {
        foundIdx = IndexOf(trans, this.data[j], foundIdx + 1);
        if (foundIdx == -1) return false;
      }
      return true;
    }
    
    private static int IndexOf(int[] array, int item, int startIdx)
    {
      for (int i = startIdx; i < array.length; ++i)
      {
        if (i > item) return -1; // i is past where the target could possibly be
        if (array[i] == item) return i;
      }
      return -1;
    }
    
    @Override
    public String toString()
    {
      String s = "{ ";
      for (int i = 0; i < data.length; ++i)
        s += data[i] + " ";
      return s + "}" + "   ct = " + ct; 
    }
}
