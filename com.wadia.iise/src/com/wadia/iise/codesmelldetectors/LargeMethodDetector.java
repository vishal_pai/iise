package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class LargeMethodDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		
		int numIssues = 0;
		
		for (MethodDetail methodDetail: codeSmellVisitor.methodDetails) {
			
			if (methodDetail.numberOfLines > SmellConstants.METHOD_LENGTH) {
				
				numIssues++;
				Utility.createMarker(codeSmellVisitor.unit, 
									 "Method has become too big. Consider refactoring.", 
									 methodDetail.lineNumber,
									 display);
			}
		}
		
		return numIssues;
	}
}
