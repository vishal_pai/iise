package com.wadia.iise.codesmelldetectors;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.helper.Utility;


public class DivergentChangeDetector {
	public static int detectAndMark(IProject project, 
									 IPackageFragment [] packages,
									 Display display,
									 final Label lbl,
									 final ProgressBar bar) throws JavaModelException, SQLException {
		
		int numIssues = 0;
		Project p = ProjectModel.getProjectByName(project.getName());
		ArrayList<SourceUnit> sourceUnits = SourceUnitModel.getAllSourceUnits(p.id);
	    
		HashMap<String,ArrayList<Long>> changeDetails = new HashMap<>();
		for (SourceUnit sourceUnit: sourceUnits) {
			if (changeDetails.containsKey(sourceUnit.path)) {
	    		ArrayList<Long> linesOfCode = changeDetails.get(sourceUnit.path);
	    		linesOfCode.add(sourceUnit.numLines);
	    		changeDetails.put(sourceUnit.path, linesOfCode);
	    	}
	    	else {
	    		ArrayList<Long> linesOfCode = new ArrayList<>();
	    		linesOfCode.add(sourceUnit.numLines);
	    		changeDetails.put(sourceUnit.path, linesOfCode);
	    	}
		}
		
	    display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(93);
				}
			}
		});
	    
	    HashMap<String,Float> numberOfTimesModified = new HashMap<>();
	    for (Map.Entry<String,ArrayList<Long>> entry1: changeDetails.entrySet()) {
    	    String key1 = entry1.getKey();
    	    ArrayList<Long> lines = entry1.getValue();
    	    Long count = (long) 0;
    	    for (int lineIndex = 0; lineIndex < lines.size() - 1; lineIndex++) {
    	    	if (lines.get(lineIndex).longValue() != lines.get(lineIndex + 1).longValue()) {
    	    		count++;
    	    	}
    	    }
    	    
    	    Float changePercentage = (float) ((count * 100) / lines.size());
    	    
    	    numberOfTimesModified.put(key1, changePercentage);
	    }
	    
	    for (IPackageFragment mypackage : packages) {
	    	
	    	if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
	    		
	    		for (Map.Entry<String,Float> entry1: numberOfTimesModified.entrySet()) {
	        	    final String key1 = entry1.getKey();
	        	    Float changePercentage = entry1.getValue();
	        	    
	        	    display.asyncExec(new Runnable() {
	        			
	        			@Override
	        			public void run() {
	        				if (lbl.isDisposed() == false) {
	        					lbl.setText("Divergent change detection: " + key1);
	        					lbl.getParent().layout();
	        				}
	        			}
	        		});
	        	    
	        	    
		    		for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
		    			 
		    			if (key1.contains(unit.getResource().getRawLocation().toString())) {
		    				
		    				if (changePercentage > SmellConstants.DIVERSION_PERCENTAGE) {
		    					
		    					numIssues++;
		    					
		    					Utility.createMarker(unit,
		    										 "Unit has been affected by diversion " + key1,
		    										 1,
		    										 display);
		    				}
		    			}
		    		}
	    		}
	    	}
	    }
	    
	    display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(95);
				}
			}
		});
	    
	    return numIssues;
	}
}
