package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class FieldNameLengthDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		
		boolean isUnitAffected = false;
		
		for (int index = 0; index < codeSmellVisitor.fields.size(); index++) {
    		FieldDetail detail = codeSmellVisitor.fields.get(index);
    		if (detail.name.length() < SmellConstants.MIN_FIELD_LENGTH) {
    			isUnitAffected = true;
    			Utility.createMarker(codeSmellVisitor.unit, 
    								 "Field length too small. Should be greater than " + SmellConstants.MIN_FIELD_LENGTH + " characters.", 
    								 detail.lineNumber,
    								 display);
    		}
    		else if (detail.name.length() > SmellConstants.MAX_FIELD_LENGTH) {
    			isUnitAffected = true;
    			Utility.createMarker(codeSmellVisitor.unit, 
						 "Field length too large. Should be less than " + SmellConstants.MAX_FIELD_LENGTH + " characters.", 
						 detail.lineNumber,
						 display);
    		}
    	}
		
		if (isUnitAffected == true) {
			return 1;
		}
		
		return 0;
	}
}
