package com.wadia.iise.codesmelldetectors;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

import com.wadia.iise.helper.Utility;

//http://codebetter.com/patricksmacchia/2012/06/12/an-original-algorithm-to-find-net-code-duplicate/
public class DuplicateCodeDetector {
	public static int detectAndMark(HashMap<String, 
									 MethodDetail> allMethods,
									 IProject project,
									 IPackageFragment[] packages,
									 Display display,
									 final Label lbl,
									 final ProgressBar bar) throws JavaModelException {
		
		int numIssues = 0;
		HashMap<String, Long> alreadySeen = new HashMap<>();
		HashMap<String, Boolean> duplicateMethodTracker = new HashMap<>();
		
		for (Map.Entry<String, MethodDetail> entry1: allMethods.entrySet()) {
    	    String key1 = entry1.getKey();
    	    final MethodDetail methodDetail1 = entry1.getValue();
    	    
    	    display.asyncExec(new Runnable() {
				
				@Override
				public void run() {
					if (lbl.isDisposed() == false) {
						lbl.setText("Duplicate method detection: " + methodDetail1.unitPathCorrectCase);
						lbl.getParent().layout();
					}
				}
			});

    	    if(methodDetail1.numberOfLines < SmellConstants.DUPLICATE_METHOD_MIN_LINES) {
    	    	continue;
    	    }
    	    
    	    for (Map.Entry<String, MethodDetail> entry2: allMethods.entrySet()) {
    	    	String key2 = entry2.getKey();
        	    MethodDetail methodDetail2 = entry2.getValue();
        	    
        	    if(methodDetail2.numberOfLines < SmellConstants.DUPLICATE_METHOD_MIN_LINES) {
        	    	continue;
        	    }
        	    
        	    if (key1.equals(key2)) {
        	    	continue;
        	    }
        	    
        	    if (alreadySeen.containsKey(key2)) {
        	    	continue;
        	    }
        	    
        	    int count = 0;
        	    
        	    for (String str1: methodDetail1.lhs) {
        	    	for (String str2: methodDetail2.lhs) {
        	    		if (str1.equals(str2)) {
        	    			count++;
        	    		}
        	    	}
        	    }
        	    
        	    for (String str1: methodDetail1.rhs) {
        	    	for (String str2: methodDetail2.rhs) {
        	    		if (str1.equals(str2)) {
        	    			count++;
        	    		}
        	    	}
        	    }
        	    
        	    for (String str1: methodDetail1.methodInvocations) {
        	    	for (String str2: methodDetail2.methodInvocations) {
        	    		if (str1.equals(str2)) {
        	    			count++;
        	    		}
        	    	}
        	    }
        	    
        	    for (String v1: methodDetail1.variables) {
        	    	for (String v2: methodDetail2.variables) {
        	    		if (v1.equals(v2)) {
        	    			count++;
        	    		}
        	    	}
        	    }
        	    
        	    for (String v1: methodDetail1.parameters) {
        	    	for (String v2: methodDetail2.parameters) {
        	    		if (v1.equals(v2)) {
        	    			count++;
        	    		}
        	    	}
        	    }
        	    
        	    float totalCount = 0;
        	    if (methodDetail1.lhs != null && methodDetail2.lhs != null) {
        	    	totalCount += ((methodDetail1.lhs.size() + methodDetail2.lhs.size())/2);
        	    }
        	    
        	    if (methodDetail1.rhs != null && methodDetail2.rhs != null) {
        	    	totalCount += ((methodDetail1.rhs.size() + methodDetail2.rhs.size())/2);
        	    }
        	    
        	    if (methodDetail1.methodInvocations != null && methodDetail2.methodInvocations != null) {
        	    	totalCount += ((methodDetail1.methodInvocations.size() + methodDetail2.methodInvocations.size())/2);
        	    }
        	    
        	    if (methodDetail1.variables != null && methodDetail2.variables != null) {
        	    	totalCount += ((methodDetail1.variables.size() + methodDetail2.variables.size())/2);
        	    }
        	    
        	    if (methodDetail1.parameters != null && methodDetail2.parameters != null) {
        	    	totalCount += ((methodDetail1.parameters.size() + methodDetail2.parameters.size())/2);
        	    }
        	    
        	    float duplicatePercentage = (count * 100) / totalCount;
        	    
        	    if (duplicatePercentage > SmellConstants.DUPLICATE_PERCENTAGE) {
        	    	
        	    	duplicateMethodTracker.put(key1, true);
        	    	duplicateMethodTracker.put(key2, true);
        	    	
        	    	numIssues++;
        	    	
        	    	if (numIssues < SmellConstants.MAX_ERRORS) {
	        	    	Utility.createMarker(project,
	        	    						 packages,
	        	    						 methodDetail1.unitPathCorrectCase,
	        	    						 methodDetail1.lineNumber,
	        	    						 methodDetail2.unitPathCorrectCase, 
	        	    						 methodDetail2.lineNumber,
					             			 "Duplicate methods detected. Method " + methodDetail2.name +
					             			 " in file " + methodDetail2.unitPathCorrectCase +
					             			 " and method " + methodDetail1.name + 
					             			 " in file " + methodDetail1.unitPathCorrectCase +
					             			 ". Consider refactoring.",
					             			 display);
        	    	}
        	    }
    	    }
    	    
    	    alreadySeen.put(key1, (long)1);
	    }
		
		display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(90);
				}
			}
		});
		
		return duplicateMethodTracker.size();
	}
}
