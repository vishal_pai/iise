package com.wadia.iise.codesmelldetectors;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.helper.Utility;

//http://msdn.microsoft.com/en-us/magazine/dn519928.aspx
public class ShotGunSurgeryDetector {
	public static int detectAndMark(IProject project,
									IPackageFragment [] packages,
									 Display display,
									 final Label lbl,
									 final ProgressBar bar) throws SQLException, JavaModelException {
		
		HashMap<String, Boolean> unitTracker = new HashMap<>();
		
		display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (lbl.isDisposed() == false) {
					lbl.setText("Shotgun surgery detection...");
				}
			}
		});

		//Construct the transaction set
		//Every time project is analyzed we know the LOC changes
		//We are making best guess on the LOC changes
		ArrayList<ArrayList<String>> unitPaths = new ArrayList<>();
		Project p = ProjectModel.getProjectByName(project.getName());
		if (p == null) {
			return 0;
		}
		
		ArrayList<Timestamp> distinctTimestampList = ProjectModel.getAllTimeStamps(p.id);
		if (distinctTimestampList.size() < 2) {
			return 0;
		}
		
		ArrayList<SourceUnit> units1 = SourceUnitModel.getAllSourceUnitsByTimeStamp(p.id, distinctTimestampList.get(0));
		ArrayList<SourceUnit> units2 = null;
		
		for (int index = 0; index < distinctTimestampList.size() - 1 ; index++) {
			units2 = SourceUnitModel.getAllSourceUnitsByTimeStamp(p.id, distinctTimestampList.get(index + 1));
			ArrayList<String> pathData = new ArrayList<>();
			for (SourceUnit unit1 : units1) {
				for (SourceUnit unit2: units2) {
					if(unit1.path.equals(unit2.path)) {
						if (unit1.numLines.longValue() != unit2.numLines.longValue()) {
							pathData.add(unit1.path);
						}
					}
				}
			}
			
			if (pathData.size() > 1) {
				unitPaths.add(pathData);
			}
			
			units1.clear();
			units1 = units2;
		}
		
		if (unitPaths.size() == 0) {
			return 0;
		}
		
		HashMap<String, Integer> pathToNumberEncoding = new HashMap<>();
		HashMap<Integer, String> numberToPathEncoding = new HashMap<>();
		
		int uniqueNumber = 0;
		
		//Convert unit path into number...
		for (ArrayList<String> items: unitPaths) {
			for (String unitPath: items) {
				if(pathToNumberEncoding.containsKey(unitPath) == false) {
					pathToNumberEncoding.put(unitPath,uniqueNumber);
					numberToPathEncoding.put(uniqueNumber, unitPath);
					uniqueNumber++;
				}
			}
		}
		
		List<int[]> transactions = new ArrayList<>();
		
		//Create List<int[]> from ArrayList<ArrayList<String>> unitPaths
		for (ArrayList<String> items: unitPaths) {
			
			int [] data = new int [items.size()];
			
			int dataIndex = 0;
			for (String unitPath: items) {
				data[dataIndex] = pathToNumberEncoding.get(unitPath);
			}
			
			java.util.Arrays.sort(data);
			transactions.add(data);
		}
		
		List<ItemSet> frequentlyChangingSets = GetFrequentItemSets(pathToNumberEncoding.size(), transactions, 0.30, 2, 100);
		for (ItemSet item: frequentlyChangingSets) {
			if (item.ct < SmellConstants.SHOTGUN_THRESHHOLD) {
				continue;
			}
			
			String message = "Potential shotgun surgery candidates. These units are frequently changing together. Unit names are ";
			String unitMessage = "";
			
			for (int data: item.data) {
				if (unitMessage.length() > 0) {
					unitMessage += ",";
				}
				
				unitMessage += numberToPathEncoding.get(data);
			}
			
			message += unitMessage;
			
			for (int data: item.data) {
				for (IPackageFragment mypackage : packages) {
			    	
			    	if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
			    		
			    		for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
			    			 
			    			if (numberToPathEncoding.get(data).contains(unit.getResource().getRawLocation().toString())) {
			    				
			    				unitTracker.put(unit.getResource().getRawLocation().toString(), true);
			    				
			    				Utility.createMarker(unit,
			    									 message,
			    									 1,
			    									 display);
			    			}	
			    		}
			    	}
				}
			}
		}
		
		display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(95);
				}
			}
		});
		
		return unitTracker.size();
	}
	
	static void unitTest() {
        String[] rawItems = new String[] { "apples", "bread ", "celery", "donuts", "eggs  ", "flour ",
        "grapes", "honey ", "icing ", "jelly ", "kale  ", "lettuce" };

	  int N = rawItems.length; // total number of items to deal with ( [0..11] )
	
	  String[][] rawTransactions = new String[10][];
	  rawTransactions[0] = new String[] { "apples", "bread ", "celery", "flour " };   // 0 1 2 5
	  rawTransactions[1] = new String[] { "bread ", "eggs  ", "flour " };             // 1 4 5
	  rawTransactions[2] = new String[] { "apples", "bread ", "donuts", "eggs  " };             // 0 1 3 4 
	  rawTransactions[3] = new String[] { "celery", "donuts", "flour ", "grapes" };   // 2 3 5 6
	  rawTransactions[4] = new String[] { "donuts", "eggs  " };                       // 3 4
	  rawTransactions[5] = new String[] { "donuts", "eggs  ", "jelly " };             // 3 4 9
	  rawTransactions[6] = new String[] { "apples", "bread ", "donuts", "icing " };   // 0 1 3 8
	  rawTransactions[7] = new String[] { "bread ", "grapes", "honey " };                     // 1 6 7
	  rawTransactions[8] = new String[] { "apples", "bread ", "celery", "flour ", "kale  " }; // 0 1 2 5 10
	  rawTransactions[9] = new String[] { "apples", "bread ", "celery", "flour " };           // 0 1 2 5
	  
	  List<int[]> transactions = new ArrayList<int[]>();
	    transactions.add(new int[] { 0, 1, 2, 5 });
	    transactions.add(new int[] { 1, 4, 5 });
	    transactions.add(new int[] { 0, 1, 3, 4 });
	    transactions.add(new int[] { 2, 3, 5, 6 });
	    transactions.add(new int[] { 3, 4 });
	    transactions.add(new int[] { 3, 4, 9 });
	    transactions.add(new int[] { 0, 1, 3, 8 });
	    transactions.add(new int[] { 1, 6, 7 });
	    transactions.add(new int[] { 0, 1, 2, 5, 10 });
	    transactions.add(new int[] { 0, 1, 2, 5 });
	    
	    List<ItemSet> frequentlyChangingSets = GetFrequentItemSets(rawItems.length, transactions, 0.30, 2, 20);
	}
	
	static List<ItemSet> GetFrequentItemSets(int N, 
											 List<int[]> transactions, 
											 double minSupportPct,
											 int minItemSetLength, 
											 int maxItemSetLength)
		    {
		      // create a List of frequent ItemSet objects that are in transactions
		      // frequent means occurs in minSupportPct percent of transactions
		      // N is total number of items
		      // uses a variation of the Apriori algorithm
		      int minSupportCount = (int)(transactions.size() * minSupportPct);

		      HashMap<Integer, Boolean> frequentDict = new HashMap<Integer, Boolean>(); // key = int representation of an ItemSet, val = is in List of frequent ItemSet objects
		      List<ItemSet> frequentList = new ArrayList<ItemSet>(); // item set objects that meet minimum count (in transactions) requirement 
		      List<Integer> validItems = new ArrayList<Integer>(); // inidividual items/values at any given point in time to be used to construct new ItemSet (which may or may not meet threshhold count)

		      // get counts of all individual items
		      int[] counts = new int[N]; // index is the item/value, cell content is the count
		      for (int i = 0; i < transactions.size(); ++i)
		      {
		        for (int j = 0; j < transactions.get(i).length; ++j)
		        {
		          int v = transactions.get(i)[j];
		          ++counts[v];
		        }
		      }
		      // for those items that meet support threshold, add to valid list, frequent list, frequent dict
		      for (int i = 0; i < counts.length; ++i)
		      {
		        if (counts[i] >= minSupportCount) // frequent item
		        {
		          validItems.add(i); // i is the item/value
		          int[] d = new int[1]; // the ItemSet ctor wants an array
		          d[0] = i;
		          ItemSet ci = new ItemSet(N, d, 1); // an ItemSet with size 1, ct 1
		          frequentList.add(ci); // it's frequent
		          frequentDict.put(ci.hashValue, true); // 
		        } // else skip this item
		      }

		      boolean done = false; // done if no new frequent item-sets found
		      for (int k = 2; k <= maxItemSetLength && done == false; ++k) // construct all size  k = 2, 3, 4, . .  frequent item-sets
		      {
		        done = true; // assume no new item-sets will be created
		        int numFreq = frequentList.size(); // List size modified so store first

		        for (int i = 0; i < numFreq; ++i) // use existing frequent item-sets to create new freq item-sets with size+1
		        {
		          if (frequentList.get(i).k != k - 1) continue; // only use those ItemSet objects with size 1 less than new ones being created

		          for (int j = 0; j < validItems.size(); ++j)
		          {
		            int[] newData = new int[k]; // data for a new item-set

		            for (int p = 0; p < k - 1; ++p)
		              newData[p] = frequentList.get(i).data[p]; // old data in

		            if (validItems.get(j) <= newData[k - 2]) continue; // because item-values are in order we can skip sometimes

		            newData[k - 1] = validItems.get(j); // new item-value
		            ItemSet ci = new ItemSet(N, newData, -1); // ct to be determined

		            if (frequentDict.containsKey(ci.hashValue) == true) // this new ItemSet has already been added
		              continue;
		            int ct = CountTimesInTransactions(ci, transactions); // how many times is the new ItemSet in the transactuions?
		            if (ct >= minSupportCount) // we have a winner!
		            {
		              ci.ct = ct; // now we know the ct
		              frequentList.add(ci);
		              frequentDict.put(ci.hashValue, true);
		              done = false; // a new item-set was created, so we're not done
		            }
		          } // j
		        } // i

		        // update valid items -- quite subtle
		        validItems.clear();;
		        HashMap<Integer, Boolean> validDict = new HashMap<Integer, Boolean>(); // track new list of valid items
		        for (int idx = 0; idx < frequentList.size(); ++idx)
		        {
		          if (frequentList.get(idx).k != k) continue; // only looking at the just-created item-sets
		          for (int j = 0; j < frequentList.get(idx).data.length; ++j)
		          {
		            int v = frequentList.get(idx).data[j]; // item
		            if (validDict.containsKey(v) == false)
		            {
		              //Console.WriteLine("adding " + v + " to valid items list");
		              validItems.add(v);
		              validDict.put(v, true);
		            }
		          }
		        }
		        
		        Collections.sort(validItems);// keep valid item-values ordered so item-sets will always be ordered
		        
		      } // next k

		      // transfer to return result, filtering by minItemSetCount
		      List<ItemSet> result = new ArrayList<ItemSet>();
		      for (int i = 0; i < frequentList.size(); ++i)
		      {
		        if (frequentList.get(i).k >= minItemSetLength)
		          result.add(new ItemSet(frequentList.get(i).N, frequentList.get(i).data, frequentList.get(i).ct));
		      }

		      return result;
		    }

		    static int CountTimesInTransactions(ItemSet itemSet, List<int[]> transactions)
		    {
		      // number of times itemSet occurs in transactions
		      int ct = 0;
		      for (int i = 0; i < transactions.size(); ++i)
		      {
		        if (itemSet.IsSubsetOf(transactions.get(i)) == true)
		          ++ct;
		      }
		      return ct;
		    }
}
