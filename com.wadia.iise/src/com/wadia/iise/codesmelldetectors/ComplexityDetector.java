package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class ComplexityDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		
		int numIssues = 0;
		
		for (MethodDetail methodDetail: codeSmellVisitor.methodDetails) {
			
			if (methodDetail.complexity > SmellConstants.MAX_COMPLEXITY) {
				
				if (numIssues < SmellConstants.MAX_ERRORS) {
					Utility.createMarker(codeSmellVisitor.unit, 
										 "Method's complexity is " + methodDetail.complexity + ". Maximum complexity should be " + SmellConstants.MAX_COMPLEXITY, 
										 methodDetail.lineNumber,
										 display);
				}
				
				numIssues++;
			}
		}
		
		return numIssues;
	}
}
