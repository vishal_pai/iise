package com.wadia.iise.codesmelldetectors;

public class SmellConstants {
	public static int PARAM_NUM = 3;
	public static int METHOD_LENGTH = 200;
	public static int CLASS_LENGTH = 500;
	public static int MIN_FIELD_LENGTH = 3;
	public static int MAX_FIELD_LENGTH = 20;
	public static int NUM_SWITCHES = 3;
	public static int CLUMPS_MIN_SIZE = 1;
	public static float DUPLICATE_PERCENTAGE = 80;
	public static int DUPLICATE_METHOD_MIN_LINES = 2;
	public static int LAZY_NUM_OF_METHODS = 3;
	public static float DIVERSION_PERCENTAGE = 65;
	public static int IS_COMMENT_JAVA_CODE_THRESHHOLD = 3;
	public static int MAX_COMPLEXITY = 3;
	public static int SHOTGUN_THRESHHOLD = 5;
	public static int MAX_ERRORS = 50;
	
	public static String KEY_INCORRECT_FIELD = "% Units affected by incorrect field names";
	public static String KEY_DATA_CLASS = "% Units which are data classes";
	public static String KEY_LARGE_CLASS = "% Units that are large";
	public static String KEY_TOO_MANY_SWITCH = "% Units with too many switches";
	public static String KEY_LARGE_METHOD = "% Methods that are large";
	public static String KEY_TOO_MANY_PARAMS = "% Methods with too many parameters";
	public static String KEY_COMMENTEDOUT_CODE = "% Units with commented code";
	public static String KEY_FEATURE_ENVY = "% Methods affected by feature envy";
	public static String KEY_COMPLEXITY = "% Methods that are complex";
	public static String KEY_CLUMPS = "% Units affected by clumps";
	public static String KEY_DUPLICATE_METHOD = "% Methods that are duplicate";
	public static String KEY_DIVERGENT_UNIT = "% Units affected by divergent changes";
	public static String KEY_SHOTGUN_SURGERY = "% Units affected by shotgun surgery";

}
