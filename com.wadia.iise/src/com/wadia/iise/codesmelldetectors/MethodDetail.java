package com.wadia.iise.codesmelldetectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MethodDetail {
	public String name;
	public String unitPathCorrectCase;
	public int lineNumber;
	public int numberOfLines;
	public List<String> parameters;
	public ArrayList<String> variables;
	public ArrayList<String> lhs;
	public ArrayList<String> rhs;
	public ArrayList<String> methodInvocations;
	public HashMap<String, Long> featureUsage = new HashMap<>();
	public Long complexity;
}
