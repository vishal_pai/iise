package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class SwitchDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		
		int numIssues = 0;
		
		int totalCount = codeSmellVisitor.numberOfIfStatements + codeSmellVisitor.numberOfSwitchStatements;
		if (totalCount > SmellConstants.NUM_SWITCHES) {
    		
			numIssues++;
			
			Utility.createMarker(codeSmellVisitor.unit, 
					 			 "Too much branching found in the code. Sum of switch statements and if statements found is " + 
					 			 totalCount + ". Optimum number is " + SmellConstants.NUM_SWITCHES, 
					 			 codeSmellVisitor.parse.getLineNumber(codeSmellVisitor.parse.getStartPosition()),
					 			 display);
    	}
		
		return numIssues;
	}
}
