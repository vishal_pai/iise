package com.wadia.iise.codesmelldetectors;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.Comment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CommentVisitor;


public class CommentedOutCodeDetector {
	public static int detectAndMark(CompilationUnit parse, 
									ICompilationUnit unit,
									Display display) throws JavaModelException {
		
		HashMap<String,Boolean> unitTracker = new HashMap<>();
		
		for (Comment comment : (List<Comment>) parse.getCommentList()) {
    		CommentVisitor commentVisitor = new CommentVisitor(parse, unit, unit.getSource());
    		comment.accept(commentVisitor);
    		
    		for (CommentDetail detail: commentVisitor.details) {
    			String data = detail.comment.toLowerCase();
    			
    			data = data.replace("/*", "");
    			data = data.replace("*/", "");
    			data = data.replace("//", "");
    			
    			//Statements that are usually not used in english...
    			String [] patternWords = {"for\\s*\\(", "if\\s*\\(", "else\\s*\\(", "while\\s*\\(", "switch\\s*\\("};
    			for (String patternWord: patternWords) {
	    			Pattern pattern = Pattern.compile(patternWord, Pattern.MULTILINE);
	    			Matcher matcher = pattern.matcher(data);
	    			if (matcher.find()) {
	    				
	    				unitTracker.put(unit.getPath().toString(), true);
	    				Utility.createMarker(unit, 
											 "Dead source code found. Consider removing as soon as possible.", 
											 detail.lineNumber,
											 display);
	    				break;
	    			}
    			}
    			
    			//Operators usually not used in english...
    			String [] operatorSymbols = {"||", "&&", "++", "--", "+=", "-=", "*=", "/="};
    			for (String keyWord: operatorSymbols) {
    				if (data.contains(keyWord)) {
    					unitTracker.put(unit.getPath().toString(), true);
    					Utility.createMarker(unit, 
				   							 "Dead source code found. Consider removing as soon as possible.", 
				   							 detail.lineNumber,
				   							 display);
    					break;
    				}
    			}
    			
    			//Usually english language text does not terminate with below symbols...
    			String [] lineTerminateKeyWords = {";", "{", "}"};
    			for (String keyWord: lineTerminateKeyWords) {
    				if (data.endsWith(keyWord)) {
    					unitTracker.put(unit.getPath().toString(), true);
    					Utility.createMarker(unit, 
   							 "Dead source code found. Consider removing as soon as possible.", 
   							 detail.lineNumber,
   							 display);
    					break;
    				}
    			}
    			
    			//Usually english language text does not contain the below words and symbols...
    			int matchCount = 0;
    			String [] words = data.split(" ");
    			String [] keyWords = {"public", "private", 
    					              "protected", "abstract", "class", 
    					              "extends","implements","try","throw",
    					              "catch","final","int","boolean","double",
    					              "float","while","continue",
    					              "super","synchronized","return","enum","assert",
    					              "package","this.","instanceof","interface","static",
    					              "void","long","true","false","switch","[","]",
    					              "(",")","="};
    			
    			for (String word: words) {
    				for (String keyWord: keyWords) {
    					if (word.equals(keyWord)) {
    						matchCount++;
    					}
    				}
    			}
    			
    			if (matchCount > SmellConstants.IS_COMMENT_JAVA_CODE_THRESHHOLD) {
    				unitTracker.put(unit.getPath().toString(), true);
    				Utility.createMarker(unit, 
  							 "Dead source code found. Consider removing as soon as possible.", 
  							 detail.lineNumber,
  							 display);
   				}
    		}
    	}
		
		return unitTracker.size();
	}
}
