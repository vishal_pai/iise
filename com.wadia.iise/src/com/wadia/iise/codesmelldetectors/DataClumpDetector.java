package com.wadia.iise.codesmelldetectors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;

import com.wadia.iise.helper.Utility;

public class DataClumpDetector {
	static int numDataClumps = 0;
	static int numParamClumps = 0;
	
	public static int detectAndMark(HashMap<String, MethodDetail> allMethods, 
									 HashMap<String, ArrayList<FieldDetail>> allFields,
									 IProject project,
									 IPackageFragment[] packages,
									 Display display,
									 final Label lbl,
									 final ProgressBar bar) throws JavaModelException {
		
		HashMap<String, Boolean> clumpUnitTracker = new HashMap<>();
		
		HashMap<String, Long> alreadySeen = new HashMap<>();
		
		for (Map.Entry<String, MethodDetail> entry1: allMethods.entrySet()) {
			
			final String key1 = entry1.getKey();
    	    MethodDetail methodDetail1 = entry1.getValue();
    	    
    	    display.asyncExec(new Runnable() {
				
				@Override
				public void run() {
					if (lbl.isDisposed() == false) {
						lbl.setText("Parameters clump: " + key1);
						lbl.getParent().layout();
					}
				}
			});
    	    
    	    if (methodDetail1.parameters.size() == 0) {
    	    	continue;
    	    }
    	    
    	    for (Map.Entry<String, MethodDetail> entry2: allMethods.entrySet()) {
    	    	String key2 = entry2.getKey();
    	    	MethodDetail methodDetail2 = entry2.getValue();
        	    int count = 0;
        	    
        	    if (methodDetail2.parameters.size() == 0) {
        	    	continue;
        	    }
        	    
        	    if (key1.equals(key2)) {
        	    	continue;
        	    }
        	    
        	    if (alreadySeen.containsKey(key2)) {
        	    	continue;
        	    }
        	    
        	    for (String v1: methodDetail1.parameters) {
        	    	for (String v2: methodDetail2.parameters) {
        	    		if (v1.equals(v2)) {
        	    			count++;
        	    			break;
        	    		}
        	    	}
        	    }
        	    
        	    if (count > SmellConstants.CLUMPS_MIN_SIZE) {
        	    	
        	    	clumpUnitTracker.put(methodDetail1.unitPathCorrectCase.toLowerCase(), true);
        	    	clumpUnitTracker.put(methodDetail2.unitPathCorrectCase.toLowerCase(), true);
        	    	
        	    	if (numParamClumps < SmellConstants.MAX_ERRORS)
        	    	{
	        	    	Utility.createMarker(project,
	        	    						 packages,
	        	    						 methodDetail1.unitPathCorrectCase, 
	        	    						 methodDetail1.lineNumber,
	        	    						 methodDetail2.unitPathCorrectCase,
	        	    						 methodDetail2.lineNumber,
	        	    						 "Data clump found. Same parameters being passed in methods " + 
	        	    	        	    	 methodDetail1.name + " at line number " + methodDetail1.lineNumber + 
	        	    	        	    	 " in the unit " + methodDetail1.unitPathCorrectCase + " and " + 
	        	    	        	    	 methodDetail2.name + " at line number " + methodDetail2.lineNumber + 
	        	    	        	    	 " in the unit " + methodDetail2.unitPathCorrectCase + ".",
	        	    	        	    	 display);
        	    	}
	        	    
        	    	numParamClumps++;
        	    }
    	    }
    	    
    	    alreadySeen.put(key1, (long)1);
	    }
	    
		
		display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(60);
				}
			}
		});
		
		alreadySeen.clear();
		
	    //Data clumps and fields list...
	    for (Map.Entry<String, ArrayList<FieldDetail>> entry1: allFields.entrySet()) {
    	    final String key1 = entry1.getKey();
    	    ArrayList<FieldDetail> fieldDetails1 = entry1.getValue();
    	    
    	    display.asyncExec(new Runnable() {
				
				@Override
				public void run() {
					if (lbl.isDisposed() == false) {
						lbl.setText("Fields clump: " + key1);
						lbl.getParent().layout();
					}
				}
			});

    	    for (Map.Entry<String, ArrayList<FieldDetail>> entry2: allFields.entrySet()) {
    	    	String key2 = entry2.getKey();
    	    	
    	    	if (key1.equals(key2)) {
    	    		continue;
    	    	}
    	    	
    	    	if (alreadySeen.containsKey(key2)) {
        	    	continue;
        	    }
    	    	
    	    	ArrayList<FieldDetail> fieldDetails2 = entry2.getValue();
        	    int count = 0;
        	    
        	    for (FieldDetail f1: fieldDetails1) {
        	    	for (FieldDetail f2: fieldDetails2) {
        	    		if (f1.name.equals(f2.name)) {
        	    			count++;
        	    			break;
        	    		}
        	    	}
        	    }
        	    
        	    if (count > SmellConstants.CLUMPS_MIN_SIZE) {
        	    	
        	    	clumpUnitTracker.put(key1.toLowerCase(), true);
        	    	clumpUnitTracker.put(key2.toLowerCase(), true);
        	    	
        	    	if (numDataClumps < SmellConstants.MAX_ERRORS) {
	        	    	Utility.createMarker(project,
	        	    						 packages,
	        	    						 fieldDetails1.get(0).unitPath, 
	        	    						 1,
	        	    						 fieldDetails2.get(0).unitPath,
	        	    						 1,
								             "Data clump found. Same fields being used in units " + 
								            fieldDetails2.get(0).unitPath +
								            " and " + fieldDetails1.get(0).unitPath + 
								            ". Creating a base class with the common attributes might help refactor.",
								            display);
        	    	}
        	    	
        	    	numDataClumps++;
        	    }
    	    }
    	    
    	    alreadySeen.put(key1, (long)1);
	    }
	    
	    display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				if (bar.isDisposed() == false) {
					bar.setSelection(70);
				}
			}
		});
	    
	    return clumpUnitTracker.size();
	}
}
