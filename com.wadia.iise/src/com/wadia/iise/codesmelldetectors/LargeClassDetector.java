package com.wadia.iise.codesmelldetectors;

import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class LargeClassDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor,Display display) {
		int numIssues = 0;
		
		try {
			
			if (Utility.splitLines(codeSmellVisitor.unit.getSource()).size() > SmellConstants.CLASS_LENGTH) {
				numIssues++;
				
				Utility.createMarker(codeSmellVisitor.unit, 
						 "Class has become too big. Consider refactoring.", 
						 codeSmellVisitor.parse.getLineNumber(codeSmellVisitor.parse.getStartPosition()),
						 display);
			}
		} catch (JavaModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return numIssues;
	}
}
