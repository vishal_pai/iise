package com.wadia.iise.codesmelldetectors;

import java.util.Map;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class FeatureEnvyDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		int numIssues = 0;
		
		for (MethodDetail methodDetail: codeSmellVisitor.methodDetails) {
			Long currentClassFeatureUsage = (long) 0;
	    	Long maxClassFeatureUsage = (long) 0;
	    	String maxClassName = "";
	    	for (Map.Entry<String, Long> entry : methodDetail.featureUsage.entrySet()) {
	    	    String key = entry.getKey();
	    	    Long value = entry.getValue();
	    	    
	    	    if (key.equals(codeSmellVisitor.unit.getPath().toString()) == true) {
	    	    	currentClassFeatureUsage = value;
	    	    }
	    	    else {
	    	    	if (value > maxClassFeatureUsage) {
	    	    		maxClassFeatureUsage = value;
	    	    		maxClassName = key;
	    	    	}
	    	    }
	    	}

	    	if (maxClassFeatureUsage > currentClassFeatureUsage) {
	    		
	    		numIssues++;
	    		
	    		Utility.createMarker(codeSmellVisitor.unit, 
						 "Method Name " + methodDetail.name + " should be present in the class " + maxClassName, 
						 methodDetail.lineNumber,
						 display);
	    	}
		}
		
		return numIssues;
	}
}
