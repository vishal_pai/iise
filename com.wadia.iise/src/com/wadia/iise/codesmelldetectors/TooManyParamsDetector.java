package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class TooManyParamsDetector {
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		int numIssues = 0;
		
		for (MethodDetail methodDetail: codeSmellVisitor.methodDetails) {
			
			if (methodDetail.parameters.size() > SmellConstants.PARAM_NUM) {
    			
				numIssues++;
				
				Utility.createMarker(codeSmellVisitor.unit, 
						 			 "Method has too many parameters. Consider refactoring.", 
						 			 methodDetail.lineNumber,
						 			 display);
    		}
		}
		
		return numIssues;
	}
}
