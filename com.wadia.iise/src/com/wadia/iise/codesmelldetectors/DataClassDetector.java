package com.wadia.iise.codesmelldetectors;

import org.eclipse.swt.widgets.Display;

import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

public class DataClassDetector {
	
	public static int detectAndMark(CodeSmellVisitor codeSmellVisitor, Display display) {
		
		int numIssues = 0;
		
		if (codeSmellVisitor.methodDetails.size() == 0) {
    		Utility.createMarker(codeSmellVisitor.unit, 
								 "Class with no methods found. This is also called Data class code smell.", 
								 1,
								 display);
    		return 1;
    	}
    	
    	if (codeSmellVisitor.methodDetails.size() < SmellConstants.LAZY_NUM_OF_METHODS) {
    		Utility.createMarker(codeSmellVisitor.unit, 
					 "Lazy class detected. Number of methods should be greater than " + SmellConstants.LAZY_NUM_OF_METHODS, 
					 1,
					 display);
    		return 1;
    	}
    	
    	boolean isDataClass = true;
    	
    	for (MethodDetail method: codeSmellVisitor.methodDetails)
		{
			if ((method.name.startsWith("get")) && 
				(method.parameters.size() == 0) &&
				(method.complexity == 1)) {
				isDataClass = true;
			}
			else if ((method.name.startsWith("set")) && 
					 (method.parameters.size() == 1) &&
					 (method.complexity == 1)) {
				isDataClass = true;
			}
			else {
				isDataClass = false;
				break;
			}
		}
    	
    	if (isDataClass == true) {
    		
    		numIssues++;
    		
    		Utility.createMarker(codeSmellVisitor.unit, 
					 "Class with only getter and setter methods found. This is also called Data class code smell. This is also a lazy class.", 
					 1,
					 display);
    	}
    	
    	return numIssues;
	}
}
