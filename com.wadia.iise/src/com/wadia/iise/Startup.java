package com.wadia.iise;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.DDLScripts;
import com.wadia.iise.db.ProjectModel;

/*
 * Purpose: 
 * When a project is created in eclipse, we have to trap that event and create project entry into our db.
 * Hence we need to hook into eclipse startup
 */

public class Startup implements org.eclipse.ui.IStartup {

	@Override
	public void earlyStartup() {
		try {
		   IWorkspace workspace = ResourcesPlugin.getWorkspace();
		   if (workspace == null) {
			   throw new Exception("Workspace is null. project creation will not be tracked.");
		   }
		  
		   IResourceChangeListener rcl = new IResourceChangeListener() {
		       public void resourceChanged(IResourceChangeEvent event) {
		    	  try {
			    	  if (event == null) {
			    		  throw new Exception("Event object is null. project creation will not be tracked.");
			    	  }
			    	  
			    	  event.getDelta().accept(new DeltaPrinter());
				  }
		    	  catch (Exception ex) {
		  				ex.printStackTrace();
		  		  }
		      }
		   };
		   
		   workspace.addResourceChangeListener(rcl,IResourceChangeEvent.POST_CHANGE);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
	
	private class DeltaPrinter implements IResourceDeltaVisitor {
	      public boolean visit(IResourceDelta delta) {
	    	 IResource res = delta.getResource();
	         
	    	 if (res.getType() == IResource.PROJECT) {
	    		 IProject project = (IProject) res;
	    		 
	    		 switch (delta.getKind()) {
		            
	    		 	//Find a clean way to handle project rename...
	    		 	//Right now after renaming the project iisee will consider the renamed project to be a new
	    		 	//project...
	    		 	case IResourceDelta.ADDED:
		            	 Calendar cal = new GregorianCalendar();
		            	 cal.set(Calendar.HOUR_OF_DAY, 0);
			             cal.set(Calendar.MINUTE, 0);
			             cal.set(Calendar.SECOND, 0);
			             cal.set(Calendar.MILLISECOND, 0);
		                
		                 Date date = cal.getTime();
		                
		                 DDLScripts.createTables();
		                
		                 Project prj = null;
		                
						 try {
							 prj = ProjectModel.getProjectByName(project.getName());
						 } catch (SQLException e) {
							 DBHelper.clearInterruptFlag(e);
							 e.printStackTrace();
						 }
		                
						 if (prj == null) {
			                 ProjectModel.createProject(project.getName(), 
			            							   "To be entered", 
			            							   "To be entered", 
			            							   date, date);
						 }
						
						break;
	    		 }
	    	 }
	    	 
	    	 return true; // visit the children
	      }
	}
}