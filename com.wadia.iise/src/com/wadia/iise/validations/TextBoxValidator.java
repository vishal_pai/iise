package com.wadia.iise.validations;

import org.eclipse.swt.widgets.Text;

import com.wadia.iise.helper.Utility;

//Validate helper
public class TextBoxValidator {
	public Text txtControl;
	public int minLength;
	public int maxLength;
	public Boolean isNumericOnly;
	
	public TextBoxValidator(Text txtControl, 
							int minLength, 
							int maxLength,
							Boolean isNumericOnly) {
		this.txtControl = txtControl;
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.isNumericOnly = isNumericOnly;
	}
	
	public Boolean isValid() {
		if (txtControl == null) {
			return false;
		}
		
		String txt = txtControl.getText();
		
		if (txt.length() < minLength){
			return false;
		}
		
		if (txt.length() >= maxLength) {
			return false;
		}
		
		if (isNumericOnly == true) {
			if (Utility.isNumeric(txt) == false) {
				return false;
			}
		}
		
		return true;
	}
}
