package com.wadia.iise.validations;

import java.util.Date;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.MultiValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.views.ViewAddEditProject.ProjectDetailsValidator;

/*
 * Purpose: 
 * Validation of start date and end date e.g. start date <= end date...
 */

public class DateRangeValidator extends MultiValidator {
	private IObservableValue start;
	private IObservableValue end;
	private String errorMessage;
	private ProjectDetailsValidator dialogValidator;

	public DateRangeValidator(IObservableValue start, 
							  IObservableValue end,
							  String errorMessage,
							  ProjectDetailsValidator dialogValidator) {
		this.start = start;
		this.end = end;
		this.errorMessage = errorMessage;
		this.dialogValidator = dialogValidator;
	}

	protected IStatus validate() {
		Date startDate = (Date) start.getValue();
		Date endDate = (Date) end.getValue();
		if (startDate.compareTo(endDate) > 0) {
			dialogValidator.enableOKButton();
			return ValidationStatus.error(errorMessage);
		}
		
		dialogValidator.enableOKButton();
		return ValidationStatus.ok();
	}
}