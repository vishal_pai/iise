package com.wadia.iise.validations;

/*
 * Purpose: 
 * Min and max valid lengths for text boxes...
 */

public class TextFieldLength {
	public static int QUESTION_MIN_LENGTH = 3;
	public static int QUESTION_MAX_LENGTH = 490;
	
	public static int PROJECT_NAME_MIN_LENGTH = 1;
	public static int PROJECT_NAME_MAX_LENGTH = 90;
	
	public static int PROJECT_PROBLEM_MIN_LENGTH = 3;
	public static int PROJECT_PROBLEM_MAX_LENGTH = 4990;
	
	public static int PROJECT_SOLUTION_MIN_LENGTH = 3;
	public static int PROJECT_SOLUTION_MAX_LENGTH = 4990;
	
	public static int TEAM_MEMBER_NAME_MIN_LENGTH = 3;
	public static int TEAM_MEMBER_NAME_MAX_LENGTH = 90;
	
	public static int TEAM_MEMBER_ROLLNO_MIN_LENGTH = 1;
	public static int TEAM_MEMBER_ROLLNO_MAX_LENGTH = 15;
	
	public static int PROJECT_REQUIREMENT_NAME_MIN_LENGTH = 3;
	public static int PROJECT_REQUIREMENT_NAME_MAX_LENGTH = 490;
	
	public static int PROJECT_REQUIREMENT_DESCRIPTION_MIN_LENGTH = 3;
	public static int PROJECT_REQUIREMENT_DESCRIPTION_MAX_LENGTH = 4990;
	
	public static int REQUIREMENT_DETAILS_MIN_LENGTH = 1;
	public static int REQUIREMENT_DETAILS_MAX_LENGTH = 4990;
	
	public static int GLOSSARY_NAME_MIN_LENGTH = 3;
	public static int GLOSSARY_NAME_MAX_LENGTH = 90;
	
	public static int GLOSSARY_DESCRIPTION_MIN_LENGTH = 3;
	public static int GLOSSARY_DESCRIPTION_MAX_LENGTH = 4990;
	
	public static int USER_NAME_MIN_LENGTH = 6;
	public static int USER_NAME_MAX_LENGTH = 50;
	
	public static int USER_PASSWORD_MIN_LENGTH = 6;
	public static int USER_PASSWORD_MAX_LENGTH = 50;
	
	public static int URL_MIN_LENGTH = 1;
	public static int URL_MAX_LENGTH = 255;
	
	public static int STAKEHOLDER_NAME_MIN_LENGTH = 1;
	public static int STAKEHOLDER_NAME_MAX_LENGTH = 100;
	
	public static int STAKEHOLDER_TYPE_MIN_LENGTH = 1;
	public static int STAKEHOLDER_TYPE_MAX_LENGTH = 100;
}
