package com.wadia.iise.validations;

import com.richclientgui.toolbox.validation.validator.IFieldValidator;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.views.IEnableOKButton;

/*
 * Purpose: 
 * Text field is required. Min valid length and Max valid length...
 */

public class RequiredTextFieldValidator implements IFieldValidator<String> {
	private String data = "";
	private Integer minLength = 0;
	private Integer maxLength = 0;
	private Boolean isNumberOnly = false;
	
	public IEnableOKButton dialogValidator = null;
	
	public RequiredTextFieldValidator(IEnableOKButton dialogValidator, 
									  int minLength, 
									  int maxLength,
									  Boolean isNumberOnly) {
		this.dialogValidator = dialogValidator;
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.isNumberOnly = isNumberOnly;
	}
	
	@Override
	public String getErrorMessage() {
		if (data.length() < minLength) {
			return "This text box must contain more than " + minLength.toString() + " characters";
		}
		
		if (data.length() >= maxLength) {
			return "This text box must not contain more than " + maxLength.toString() + " characters";
		}
		
		if (isNumberOnly == true) {
			if (Utility.isNumeric(data) == false) {
				return "This text box should only contain numbers";
			}
		}
		
		return "";
	}

	@Override
	public String getWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValid(String contents) {
		Boolean isValidFlag = true;
		
		data = contents;
		if (contents.length() < minLength) {
			isValidFlag = false;
		}
		
		if (contents.length() >= maxLength) {
			isValidFlag = false;
		}
		
		if (isNumberOnly == true) {
			if (Utility.isNumeric(data) == false) {
				isValidFlag = false;
			}
		}
		
		dialogValidator.enableOKButton();
		
		return isValidFlag;
	}

	@Override
	public boolean warningExist(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}