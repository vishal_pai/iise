package com.wadia.iise.sourcecodevisitors;

import java.util.ArrayList;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BlockComment;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.LineComment;

import com.wadia.iise.codesmelldetectors.CommentDetail;

public class CommentVisitor extends ASTVisitor {

	public ArrayList<CommentDetail> details = new ArrayList<CommentDetail>();
    CompilationUnit compilationUnit;
    ICompilationUnit unit;

    private String source;

    public CommentVisitor(CompilationUnit compilationUnit, ICompilationUnit unit, String source) {

        super();
        this.compilationUnit = compilationUnit;
        this.source = source;
        this.unit = unit;
    }

    public boolean visit(LineComment node) {
    	int startLineNumber = compilationUnit.getLineNumber(node.getStartPosition()) - 1;
    	int start = node.getStartPosition();
		int end = start + node.getLength();
		String comment = source.substring(start, end);
		
		CommentDetail detail = new CommentDetail();
        detail.comment = comment;
        detail.lineNumber = startLineNumber + 1;
        details.add(detail);
        
		return true;
    }

    public boolean visit(BlockComment node) {
    	int startLineNumber = compilationUnit.getLineNumber(node.getStartPosition()) - 1;
    	int start = node.getStartPosition();
		int end = start + node.getLength();
		String comment = source.substring(start, end);
        
        CommentDetail detail = new CommentDetail();
        detail.comment = comment.toString();
        detail.lineNumber = startLineNumber + 1;
        details.add(detail);
        
        return true;
    }
}
