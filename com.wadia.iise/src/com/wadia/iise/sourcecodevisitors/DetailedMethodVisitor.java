package com.wadia.iise.sourcecodevisitors;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.wadia.iise.Activator;
import com.wadia.iise.codesmelldetectors.MethodDetail;
import com.wadia.iise.helper.Utility;

public class DetailedMethodVisitor extends ASTVisitor{
	private MethodDetail methodDetail;
	private ICompilationUnit unit;
	
	public DetailedMethodVisitor(MethodDetail detail, ICompilationUnit unit) {
		this.methodDetail = detail;
		this.unit = unit;
	}

	public boolean visit(VariableDeclarationFragment node) {
		methodDetail.variables.add(node.getName().getFullyQualifiedName());
		return true; 
	}
	
	public boolean visit(MethodInvocation node) {
		methodDetail.methodInvocations.add(node.getName().getFullyQualifiedName());
		
		IBinding binding = node.resolveMethodBinding();
		if (binding == null) {
			return true;
		}
		
	    IMethod method = (IMethod)binding.getJavaElement();
	    if (method == null) {
	    	return true;
	    }
	    
	    ICompilationUnit callerUnit = method.getCompilationUnit();
	    
	    if (callerUnit == null) {
	    	return true;
	    }
	    
	    if (methodDetail.featureUsage.containsKey(callerUnit.getPath().toString()) == false) {
	    	methodDetail.featureUsage.put(callerUnit.getPath().toString(), (long)1);
		}
		else {
			methodDetail.featureUsage.put(callerUnit.getPath().toString(), methodDetail.featureUsage.get(callerUnit.getPath().toString()) + 1);
		}
		
		return true; 
	}
	
	private String getCallerUnitName(Expression exp) {
		IVariableBinding b = null;
		String callerName = null;
		
		try {
			if (exp instanceof QualifiedName) {
	    	    QualifiedName fieldAccess=(QualifiedName)exp;
	    	    b=(IVariableBinding)fieldAccess.resolveBinding();
	    	    if (b == null) {
	    	    	return null;
	    	    }
	    	    
	    	    IField field = (IField)b.getJavaElement();
	    	    if (field == null) {
	    	    	return null;
	    	    }
	    		ICompilationUnit callerUnit = field.getCompilationUnit();
	    		if (callerUnit != null) {
	    			callerName = callerUnit.getPath().toString();
	    		}
	    	}
	    	else if (exp instanceof SimpleName) {
	    		b=(IVariableBinding)(((SimpleName)exp).resolveBinding());
	    		if (b == null) {
	    	    	return null;
	    	    }
	    		
	    		try {
	    			IField field = (IField)b.getJavaElement();
	    			if (field == null) {
		    	    	return null;
		    	    }
	    			ICompilationUnit callerUnit = field.getCompilationUnit();
		    		if (callerUnit != null) {
		    			callerName = callerUnit.getPath().toString();
		    		}
	    		}
	    		catch (Exception ex) {
	    			callerName = unit.getPath().toString();
	    		}
	    	}
	    	else if (exp instanceof FieldAccess) {
	    		FieldAccess fieldAccess=(FieldAccess)exp;
	    		b=fieldAccess.resolveFieldBinding();
	    		if (b == null) {
	    	    	return null;
	    	    }
	    		
	    		IField field = (IField)b.getJavaElement();
	    		if (field == null) {
	    	    	return null;
	    	    }
	    		ICompilationUnit callerUnit = field.getCompilationUnit();
	    		if (callerUnit != null) {
	    	    	callerName = callerUnit.getPath().toString();
	    	    }
	    	}
			
			return callerName;
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(ex), IStatus.ERROR);
			return null;
		}
	}
	
	private String getExpressionName(Expression exp) {
		IVariableBinding b = null;
		
		try {
			if (exp instanceof QualifiedName) {
	    	    QualifiedName fieldAccess=(QualifiedName)exp;
	    	    b=(IVariableBinding)fieldAccess.resolveBinding();
	    	}
	    	else if (exp instanceof SimpleName) {
	    		b=(IVariableBinding)(((SimpleName)exp).resolveBinding());
	    	}
	    	else if (exp instanceof FieldAccess) {
	    		FieldAccess fieldAccess=(FieldAccess)exp;
	    		b=fieldAccess.resolveFieldBinding();
	    	}
			
			if (b == null)
				return null;
			
			return b.getName() + b.getType().getName();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(ex), IStatus.ERROR);
			return null;
		}
	}
	
	public boolean visit(Assignment node) {
		Expression lhs = node.getLeftHandSide();
		Expression rhs = node.getRightHandSide();
		
		String lhsName = getExpressionName(lhs);
		String rhsName = getExpressionName(rhs);
		
		if (lhsName != null) {
			methodDetail.lhs.add(lhsName);
		}
		
		if (rhsName != null) {
			methodDetail.rhs.add(rhsName);
		}
		
		String lhsUnitName = getCallerUnitName(lhs);
		String rhsUnitName = getCallerUnitName(rhs);
		
		if (lhsUnitName != null) {
			if (methodDetail.featureUsage.containsKey(lhsUnitName) == false) {
				methodDetail.featureUsage.put(lhsUnitName, (long)1);
			}
			else {
				methodDetail.featureUsage.put(lhsUnitName, methodDetail.featureUsage.get(lhsUnitName) + 1);
			}
		}
		
		if (rhsUnitName != null) {
			if (methodDetail.featureUsage.containsKey(rhsUnitName) == false) {
				methodDetail.featureUsage.put(rhsUnitName, (long)1);
			}
			else {
				methodDetail.featureUsage.put(rhsUnitName, methodDetail.featureUsage.get(rhsUnitName) + 1);
			}
		}
		
		return true; 
	}
	
	
}