package com.wadia.iise.sourcecodevisitors;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

public class VariableVisitor extends ASTVisitor{
	private Long numTokens = (long) 0;
	private Long numFields = (long) 0;
	private ArrayList<String> unit_fields = new ArrayList<>();

	public boolean visit(VariableDeclarationFragment node) {
		if (node.getParent() instanceof FieldDeclaration) {
			numFields = numFields + 1;
			unit_fields.add(node.getName().getFullyQualifiedName());
		}
		else if (node.getParent() instanceof VariableDeclarationStatement) {
			numTokens = numTokens + 1;
		}
		     
		return false; 
	}
	
	public Long getNumberOfTokens() {
		return numTokens;
	}
	
	public Long getNumberOfFields() {
		return numFields;
	}
	
	public ArrayList<String> getFieldNames() {
		return unit_fields;
	}
	
}
