package com.wadia.iise.sourcecodevisitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import com.wadia.iise.codesmelldetectors.FieldDetail;
import com.wadia.iise.codesmelldetectors.MethodDetail;
import com.wadia.iise.helper.Utility;

public class CodeSmellVisitor extends ASTVisitor {
	public ICompilationUnit unit;
	public CompilationUnit parse;
	public String path;
	
	public ArrayList<FieldDetail> fields = new ArrayList<FieldDetail>();
	public HashMap<String, ArrayList<FieldDetail>> fieldsByUnit = new HashMap<>();
	
	public ArrayList<MethodDetail> methodDetails = new ArrayList<>();
	public HashMap<String, MethodDetail> methodsByUnit = new HashMap<>();
	  
	public int numberOfSwitchStatements = 0;
	public int numberOfIfStatements = 0;
	
	public CodeSmellVisitor(ICompilationUnit unit, 
							CompilationUnit parse) {
		this.unit = unit;
		this.parse = parse;
		this.path = unit.getPath().toString();
	}
	
	public boolean visit(VariableDeclarationFragment node) {
		
		if (node.getParent() instanceof FieldDeclaration) {
			FieldDetail detail = new FieldDetail();
			detail.name = node.getName().getFullyQualifiedName();
			detail.lineNumber = parse.getLineNumber(node.getStartPosition());
			detail.unitPath = path;
			fields.add(detail);
			
			if (fieldsByUnit.containsKey(unit.getPath().toString().toString())) {
				fieldsByUnit.get(unit.getPath().toString().toString()).add(detail);
			}
			else {
				ArrayList<FieldDetail> fieldDetails = new ArrayList<>();
				fieldDetails.add(detail);
				fieldsByUnit.put(unit.getPath().toString().toString(), fieldDetails);
			}
		}
		
		return true;
	}
	
	public boolean visit(MethodDeclaration methodDeclaration) {
		
		MethodDetail methodDetail = new MethodDetail();
		methodDetail.name = methodDeclaration.getName().getFullyQualifiedName();
		methodDetail.lineNumber = parse.getLineNumber(methodDeclaration.getStartPosition());
		methodDetail.unitPathCorrectCase = unit.getPath().toString();
		methodDetail.parameters = new ArrayList<String>();
		
		String keyParamPortion = "";
		for (SingleVariableDeclaration v: (List<SingleVariableDeclaration>)methodDeclaration.parameters()) {
			methodDetail.parameters.add(v.getName().getFullyQualifiedName() + v.getType().resolveBinding().getName());
			keyParamPortion += (v.getName().getFullyQualifiedName() + v.getType().resolveBinding().getName());
		}
		
		methodDetail.numberOfLines = 0;
		methodDetail.variables = new ArrayList<>();
		methodDetail.methodInvocations = new ArrayList<>();
		methodDetail.lhs = new ArrayList<>();
		methodDetail.rhs = new ArrayList<>();
		methodDetail.featureUsage = new HashMap<>();
		methodDetail.complexity = (long) 0;
		
		methodDetails.add(methodDetail);
		
		String methodKey = unit.getResource().getRawLocation().toString() + methodDetail.name;
		methodKey += keyParamPortion;
		
		methodsByUnit.put(methodKey, methodDetail);
		
		if (methodDeclaration.getBody() != null) {
			try {
				IMethod otherRepresentation = (IMethod) methodDeclaration.resolveBinding().getJavaElement();
				methodDetail.numberOfLines = Utility.splitLines(otherRepresentation.getSource()).size();
				methodDeclaration.getBody().accept(new DetailedMethodVisitor(methodDetail, unit));
				
				MccabeVisitor mccabeVisitor = new MccabeVisitor(otherRepresentation.getCompilationUnit().getSource());
				methodDeclaration.accept(mccabeVisitor);
				methodDetail.complexity = mccabeVisitor.getComplexity();
			
			} catch (JavaModelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	public boolean visit(SwitchStatement node) {
		numberOfSwitchStatements++;
		return true;
	}
	
	public boolean visit(IfStatement node) {
		numberOfIfStatements++;
		return true;
	}
}
