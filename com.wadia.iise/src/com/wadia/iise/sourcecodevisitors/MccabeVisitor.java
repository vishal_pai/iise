package com.wadia.iise.sourcecodevisitors;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.WhileStatement;

/*
 * Credits: http://sourceforge.net/p/metrics2/code/HEAD/tree/trunk/net.sourceforge.metrics/src/net/sourceforge/metrics/calculators/McCabe.java#l84
 */

public class MccabeVisitor extends ASTVisitor {

	private Long cyclomatic = (long) 1;
	private String source;

	public MccabeVisitor(String source) {
		this.source = source;
	}

	public Long getComplexity() {
		return cyclomatic;
	}
	
	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		return false; // XXX
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		return false; // XXX same as above
	}

	@Override
	public boolean visit(AnnotationTypeDeclaration node) {
		return false; // XXX same as above
	}

	@Override
	public boolean visit(EnumDeclaration node) {
		return false; // XXX same as above
	}

	@Override
	public boolean visit(CatchClause node) {
		cyclomatic++;
		return true;
	}

	@Override
	public boolean visit(ConditionalExpression node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(DoStatement node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(ForStatement node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(IfStatement node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(SwitchCase node) {
		if (!node.isDefault()) {
			cyclomatic++;
		}
		return true;
	}

	@Override
	public boolean visit(WhileStatement node) {
		cyclomatic++;
		inspectExpression(node.getExpression());
		return true;
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		inspectExpression(node.getExpression());
		return false;
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		inspectExpression(node.getInitializer());
		return true;
	}

	private void inspectExpression(Expression ex) {
		if ((ex != null) && (source != null)) {
			int start = ex.getStartPosition();
			int end = start + ex.getLength();
			String expression = source.substring(start, end);
			char[] chars = expression.toCharArray();
			for (int i = 0; i < chars.length - 1; i++) {
				char next = chars[i];
				if ((next == '&' || next == '|') && (next == chars[i + 1])) {
					cyclomatic++;
				}
			}
		}
	}
}