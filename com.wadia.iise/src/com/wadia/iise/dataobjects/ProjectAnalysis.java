package com.wadia.iise.dataobjects;

import java.sql.Timestamp;

/*
 * Purpose: 
 * In memory representation of project analysis information
 * There is no corresponding table in database
 */

public class ProjectAnalysis {
	public Timestamp analysisDateTime;
	public Long project_id;
	public String name;
}
