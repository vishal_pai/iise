package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Represents master question information
*/

public class MasterQuestion  extends CloudSynch {
	public Long id;
	public String question;
	
	//Hardware, Software, Development, Economic, Technical, Operational Feasibility
	public String type;
}
