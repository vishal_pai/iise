package com.wadia.iise.dataobjects;

import java.util.Date;

/*
 * Purpose: 
 * Represents project information
 */

public class Project  extends CloudSynch {
	public Long id;
	public String name;
	public Date startDate;
	public Date endDate;
	public String problem;
	public String solution;
}
