package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Represents feasibility information
 */

public class Feasibility extends CloudSynch {
	public Long id;
	public String description;
	
	//Economic, Operational and Technical are the types supported for now...
	public String type;
	
	public Long question_id;
	public String question;
	
	public Long project_id;
	public String project_cloudid;
}
