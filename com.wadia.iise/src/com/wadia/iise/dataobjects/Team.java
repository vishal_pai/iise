package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Represents team information
 */
public class Team  extends CloudSynch {
	public Long id;
	public String name;
	public String rollno;
	
	public Long project_id;
	public String project_cloudid;
}
