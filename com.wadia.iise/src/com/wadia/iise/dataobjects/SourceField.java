package com.wadia.iise.dataobjects;

import java.sql.Timestamp;

/*
 * Purpose: 
 * Represents data member information of a class
 */

public class SourceField  extends CloudSynch {
	public Long id;
	public Timestamp dateTime;
	public String name;
	
	public Long unit_id;
	public String unit_cloudid;
}
