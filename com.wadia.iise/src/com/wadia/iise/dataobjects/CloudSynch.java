package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Base data object to support synch to cloud
 * Data objects to be synched to cloud derive from this class      
 */
public class CloudSynch {
	public String cloudid;
	public Long synched;
}
