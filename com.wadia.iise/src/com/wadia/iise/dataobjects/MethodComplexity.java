package com.wadia.iise.dataobjects;

import java.sql.Timestamp;


/*
 * In memory representation of method's complexity
 * There is no corresponding database table for it
 */
public class MethodComplexity {
	public Long id;
	public Long unitid;
	public Timestamp analysisdate;
	public String methodName;
	public String unitName;
	public Long complexity;
}
