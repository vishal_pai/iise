package com.wadia.iise.dataobjects;

import java.sql.Timestamp;

/*
 * Purpose: 
 * Represents member methods of class
 */

public class SourceMethod  extends CloudSynch {
	public Long id;
	public Timestamp dateTime;
	public String name;
	public Long numLines;
	public Long numComments;
	public Long numBlanks;
	public Long numTokens;
	public Long complexity;
	
	public Long unit_id;
	public String unit_cloudid;
}
