package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Keeps track of entity deleted from local database
 * Information stored contains type of the entity and the cloud id to be deleted
 * During synch to cloud this information is used to delete information from the cloud     
 */

public class DeleteTrack {
	public Long id;
	public String type;
	public String cloudid;
}
