package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Represents glossary information
 */

public class Glossary  extends CloudSynch {
	public Long id;
	public String name;
	public String description;
	
	public Long project_id;
	public String project_cloudid;
}
