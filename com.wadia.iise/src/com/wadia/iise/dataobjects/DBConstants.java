package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Database table names to be used in code...       
 */

public class DBConstants {
	public static String VERSION_TABLE = "VERSION";
	public static String SYNCH_SETUP_TABLE = "SYNCH_SETUP";
	public static String STAKEHOLDER_TABLE = "STAKEHOLDER";
	public static String MASTER_STAKEHOLDER_TABLE = "MASTER_STAKEHOLDER";
	public static String PROJECT_TABLE = "PROJECT";
	public static String TEAM_TABLE = "TEAM";
	public static String MASTER_QUESTION_TABLE = "MASTER_QUESTION";
	public static String DELETE_TRACKER_TABLE = "DELETE_TRACKER";
	public static String PROJECT_REQUIREMENT_TABLE = "PROJECT_REQUIREMENT";
	public static String REQUIREMENT_TABLE = "REQUIREMENT";
	public static String FEASIBILITY_TABLE = "FEASIBILITY";
	public static String GLOSSARY_TABLE = "GLOSSARY";
	public static String SOURCE_UNIT_TABLE = "SOURCE_UNIT";
	public static String SOURCE_METHOD_TABLE = "SOURCE_METHOD";
	public static String SOURCE_FIELD_TABLE = "SOURCE_FIELD";
	public static String SOURCE_TOKEN_TABLE = "SOURCE_TOKEN";
	
	
	public static String HARDWARE_REQUIREMENTS = "HARDWARE_REQUIREMENTS";
	public static String SOFTWARE_REQUIREMENTS = "SOFTWARE_REQUIREMENTS";
	public static String DEVELOPER_REQUIREMENTS = "DEVELOPER_REQUIREMENTS";
	public static String NON_FUNCTIONAL_REQUIREMENTS = "NON_FUNCTIONAL_REQUIREMENTS";
	
	
	public static String ECONOMIC_FEASIBILITY = "ECONOMIC_FEASIBILITY";
	public static String OPERATIONAL_FEASIBILITY = "OPERATIONAL_FEASIBILITY";
	public static String TECHNICAL_FEASIBILITY = "TECHNICAL_FEASIBILITY";
}
