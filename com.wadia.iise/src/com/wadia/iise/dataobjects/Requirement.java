package com.wadia.iise.dataobjects;

/*
 * Purpose: 
 * Represents requirement information
 */

public class Requirement  extends CloudSynch {
	public Long id;
	public String details;
	
	//Hardware, Software, Development...
	public String type;
	
	public Long question_id;
	public String question;
	
	public Long project_id;
	public String project_cloudid;
}
