package com.wadia.iise.dataobjects;

import java.sql.Timestamp;

/*
 * Purpose: 
 * Represents class information
 */

public class SourceUnit  extends CloudSynch {
	public Long id;
	public Timestamp dateTime;
	public String name;
	public String path;
	public Long numLines;
	public Long numComments;
	public Long numBlanks;
	public Long numMethods;
	public Long numFields;
	public Long numTokens;
	public Long callerCount;
	public Long calleeCount;
	
	public Long project_id;
	public String project_cloudid;
}
