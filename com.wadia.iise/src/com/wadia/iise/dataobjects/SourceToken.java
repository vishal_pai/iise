package com.wadia.iise.dataobjects;

import java.sql.Timestamp;

/*
 * Purpose: 
 * Represents local variables used inside method in the class
 */

public class SourceToken  extends CloudSynch {
	public Long id;
	
	public Timestamp dateTime;
	public String name;
	
	public Long method_id;
	public String method_cloudid;
}
