package com.wadia.iise.views;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.SynchSetup;
import com.wadia.iise.db.BaseModel;
import com.wadia.iise.db.DBConnection;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.DeleteTrackModel;
import com.wadia.iise.db.FeasibilityModel;
import com.wadia.iise.db.GlossaryModel;
import com.wadia.iise.db.MasterQuestionModel;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.db.RequirementModel;
import com.wadia.iise.db.SourceFieldModel;
import com.wadia.iise.db.SourceMethodModel;
import com.wadia.iise.db.SourceTokenModel;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.db.StakeHolderModel;
import com.wadia.iise.db.TeamMemberModel;
import com.wadia.iise.helper.CloudSynch;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;

/*
 * Purpose: 
 * Handles synch to cloud...
 */

public class ViewSynchronizeData  extends TitleAreaDialog {

	Label lbl = null;
	private ProgressBar bar = null;

	private Text txtName = null;
	private Text txtPassword = null;
	
	private String synchURL = "";
	
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	public class SynchToCloudValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtName,
			        TextFieldLength.USER_NAME_MIN_LENGTH,
			        TextFieldLength.USER_NAME_MAX_LENGTH,
			        false));

			validator.add(new TextBoxValidator(txtPassword,
			        TextFieldLength.USER_PASSWORD_MIN_LENGTH,
			        TextFieldLength.USER_PASSWORD_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
		}
	};
	
	private SynchToCloudValidator dialogValidator = new SynchToCloudValidator();
	
	public ViewSynchronizeData(Shell parentShell, SynchSetup setup) {
		super(parentShell);
		synchURL = setup.url;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Synch To Cloud");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtName = Utility.createFormFields(area,
				         "User Name", 
				         "", 
				         TextFieldLength.USER_NAME_MIN_LENGTH,
				         TextFieldLength.USER_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		txtPassword = Utility.createFormFields(area,
				         "Password", 
				         "", 
				         TextFieldLength.USER_PASSWORD_MIN_LENGTH,
				         TextFieldLength.USER_PASSWORD_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         true);
		
		lbl = new Label(area, SWT.NONE);
		lbl.setText("Synch with cloud");
		lbl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		bar = new ProgressBar (area, SWT.SMOOTH);
		bar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		return area;
	}
	
	private void SynchDeletedWithCloud(String url,
									   String name, 
									   String password,
									   String data,
									   String tableName) {

		Connection conn = null;
		
		try {
			
			if (data.equals("[]")) {
				return;
			}
			
			Map<String, String> urlParameters = new HashMap<String, String>();
			urlParameters.put("data", data);
			urlParameters.put("username", name);
			urlParameters.put("password",password);
			
			url = synchURL + url;
			
			String response = CloudSynch.postData(url, urlParameters);
			
			String [] ids = response.split(",");
			if (ids.length <= 1) {
				return;
			}
			
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			for(int i = 0; i < ids.length; i+=2) {
				DeleteTrackModel.deleteEntity(DBConstants.DELETE_TRACKER_TABLE, Long.parseLong(ids[i]));
			}
			
			conn.commit();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
					DBHelper.clearInterruptFlag(e);
				}
			}
		}
	}
	
	private void SynchWithCloud(String url,
								String name, 
								String password,
								String data,
								String tableName) {
		
		Connection conn = null;
		
		try {
			
			if (data.equals("[]")) {
				return;
			}
			
			Map<String, String> urlParameters = new HashMap<String, String>();
			urlParameters.put("data", data);
			urlParameters.put("username", name);
			urlParameters.put("password",password);
			
			url = synchURL + url;
			
			String response = CloudSynch.postData(url, urlParameters);
			
			String [] ids = response.split(",");
			if (ids.length <= 1) {
				return;
			}
			
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			for(int i = 0; i < ids.length; i+=2) {
				BaseModel.synchToCloudSuccessful(tableName, Long.parseLong(ids[i]), ids[i + 1]);
			}
			conn.commit();
		}
		catch (Exception ex) {
			ex.printStackTrace();
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					DBHelper.clearInterruptFlag(e);
				}
			}
		}
	}
	
	
	
	@Override
	protected void okPressed() {
	    String name = txtName.getText().trim();
	    String password = txtPassword.getText().trim();
	    
	    String data = "";
	    
	    GsonBuilder builder = new GsonBuilder();
	    builder.setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    Gson gson = builder.create();
	    
		
	    //Sync Project...
	    bar.setSelection(10);
	    lbl.setText("Synchronizing projects...");
	    data = gson.toJson(ProjectModel.getNotSynchedProjects());
		SynchWithCloud(URLConstants.PROJECT, name, password, data, DBConstants.PROJECT_TABLE);
		
		//Sync Teams...
		bar.setSelection(20);
	    lbl.setText("Synchronizing team information...");
		data = gson.toJson(TeamMemberModel.getNotSynchedTeamMembers());
		SynchWithCloud(URLConstants.TEAM, name, password, data, DBConstants.TEAM_TABLE);
	    
		//Synch feasibility...
		bar.setSelection(40);
	    lbl.setText("Synchronizing feasibility information...");
	    data = gson.toJson(FeasibilityModel.getAllNotSynchedFeasibility());
		SynchWithCloud(URLConstants.FEASIBILITY, name, password, data, DBConstants.FEASIBILITY_TABLE);
		
		//Synch requirement...
		bar.setSelection(50);
	    lbl.setText("Synchronizing requirements information...");
	    data = gson.toJson(RequirementModel.getAllNotSynchedRequirements());
		SynchWithCloud(URLConstants.REQUIREMENT, name, password, data, DBConstants.REQUIREMENT_TABLE);
		
		//Synch glossary...
		bar.setSelection(60);
	    lbl.setText("Synchronizing glossary...");
	    data = gson.toJson(GlossaryModel.getAllNotSynchedGlossary());
		SynchWithCloud(URLConstants.GLOSSARY, name, password, data, DBConstants.GLOSSARY_TABLE);
		
	    //Synch master...
		bar.setSelection(70);
	    lbl.setText("Synchronizing master data...");
	    data = gson.toJson(MasterQuestionModel.getAllNotSynchedMasterQuestions());
		SynchWithCloud(URLConstants.MASTER_QUESTION, name, password, data, DBConstants.MASTER_QUESTION_TABLE);
		
		//Synch source units..
		bar.setSelection(75);
	    lbl.setText("Synchronizing source units...");
	    data = gson.toJson(SourceUnitModel.getAllNotSynchedSourceUnits());
		SynchWithCloud(URLConstants.SOURCE_UNIT, name, password, data, DBConstants.SOURCE_UNIT_TABLE);
		
		//Synch source field..
		bar.setSelection(80);
	    lbl.setText("Synchronizing source fields...");
	    data = gson.toJson(SourceFieldModel.getAllNotSynchedSourceField());
		SynchWithCloud(URLConstants.SOURCE_FIELD, name, password, data, DBConstants.SOURCE_FIELD_TABLE);
		
		//Synch source method..
		bar.setSelection(85);
	    lbl.setText("Synchronizing source methods...");
	    data = gson.toJson(SourceMethodModel.getAllNotSynchedSourceMethods());
		SynchWithCloud(URLConstants.SOURCE_METHOD, name, password, data, DBConstants.SOURCE_METHOD_TABLE);
				
		//Synch source tokens..
		bar.setSelection(90);
	    lbl.setText("Synchronizing source tokens...");
	    data = gson.toJson(SourceTokenModel.getAllNotSynchedSourceToken());
		SynchWithCloud(URLConstants.SOURCE_TOKEN, name, password, data, DBConstants.SOURCE_TOKEN_TABLE);
				
		//Sync other stakeholders...
		bar.setSelection(95);
	    lbl.setText("Synchronizing stakeholders...");
	    data = gson.toJson(StakeHolderModel.getAllNotSynchedStakeHolders());
		SynchWithCloud(URLConstants.STAKEHOLDER, name, password, data, DBConstants.STAKEHOLDER_TABLE);
		
		//Delete on cloud...
		bar.setSelection(100);
	    lbl.setText("Synchronizing deletion...");
		data = gson.toJson(DeleteTrackModel.getAllNotSynchedDeleteTracker());
		SynchDeletedWithCloud(URLConstants.DELETE_TRACKER, name, password, data, DBConstants.DELETE_TRACKER_TABLE);
		
		super.okPressed();
	}
}
