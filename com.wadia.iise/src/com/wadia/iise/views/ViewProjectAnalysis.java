package com.wadia.iise.views;

import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.ProjectAnalysis;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.helper.DendrogramViewer;
import com.wadia.iise.helper.TableHelper;


/*
 * Purpose: 
 * View complexity
 */
public class ViewProjectAnalysis extends TitleAreaDialog {
	private TableViewer viewer = null;
	private ComboViewer comboProject = null;
	ArrayList<Project> projects = null;
	private String type;
	
	public ViewProjectAnalysis(Shell parentShell, 
							   ArrayList<Project> projects,
							   String type) {
		super(parentShell);
		this.projects = projects;
		this.type = type;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void create() {
	    super.create();
	    
	    if (type == "Complexity") {
	    	setTitle("View Complexity");
	    }
	    else if (type == "Dendrogram") {
	    	setTitle("View Dendrogram");
	    }
	    else if (type == "PowerLaw") {
	    	setTitle("View Power Law");
	    }
	    else if (type == "AnalyzedData") {
	    	setTitle("View Analyzed Data");
	    }
	    else if (type == "Coordinator") {
	    	setTitle("View Coordinator Modules");
	    }
	    else if (type == "Utility") {
	    	setTitle("View Utility Modules");
	    }
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 2; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		
		Label lblProject = new Label(container1, SWT.NONE);
		lblProject.setText("Project");
		
		comboProject = new ComboViewer(container1,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name;
			    }
			    return super.getText(element);
			}
		});
		
		comboProject.setSelection(new StructuredSelection(projects.get(0)));
		
		comboProject.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				Project project = (Project) selection.getFirstElement();
				try {
					viewer.setInput(ProjectModel.getProjectAnalysis(project.id));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
				   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		createColumns(viewer);
		
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		try {
			IStructuredSelection selection = (IStructuredSelection) comboProject.getSelection();
			Project project = (Project) selection.getFirstElement();
			viewer.setInput(ProjectModel.getProjectAnalysis(project.id));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				TableItem[] items = viewer.getTable().getSelection();
				
				if (items.length == -1) {
				return;
				}
				
				TableItem item = items[0];
				ProjectAnalysis pa = (ProjectAnalysis) item.getData();
				
				if (type == "Complexity") {
					ViewComplexityDetails dialog = new ViewComplexityDetails(getShell(), pa.project_id, pa.analysisDateTime);
					dialog.create();
					dialog.open();
				}
				else if (type == "Dendrogram") {
					DendrogramViewer dv = new DendrogramViewer(pa.project_id, pa.analysisDateTime);
					dv.showChart();
				}
				else if (type == "PowerLaw") {
					ViewPowerLawPlot dialog = new ViewPowerLawPlot(pa.project_id, pa.analysisDateTime);
				}
				else if (type == "AnalyzedData") {
					ViewAnalyzedCode dialog = new ViewAnalyzedCode(getShell(), pa.project_id, pa.analysisDateTime);
					dialog.create();
					dialog.open();
				}
				else if (type == "Coordinator") {
					ViewModuleDetails dd = new ViewModuleDetails(getShell(), pa.project_id, pa.analysisDateTime, true);
					dd.create();
					dd.open();
				}
				else if (type == "Utility") {
					ViewModuleDetails dd = new ViewModuleDetails(getShell(), pa.project_id, pa.analysisDateTime, false);
					dd.create();
					dd.open();
				}
			}
		});
		
		return area;
	}
	
	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Analyzed Date", "Project Name"};
	    int[] bounds = { 300, 300};
	    
	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        ProjectAnalysis pa = (ProjectAnalysis) element;
	        return pa.name;
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  ProjectAnalysis pa = (ProjectAnalysis) element;
		      return pa.analysisDateTime.toString();
	      }
	    });
	}
}
