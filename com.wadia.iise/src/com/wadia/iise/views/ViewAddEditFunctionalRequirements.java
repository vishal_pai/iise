package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.FunctionalRequirement;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.FunctionalRequirementModel;
import com.wadia.iise.helper.TextBoxKeyListener;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;

/*
 * Purpose: 
 * CRUD Functional requirements
 */
public class ViewAddEditFunctionalRequirements extends TitleAreaDialog {

	private Text txtName = null;
	private Text txtDescription = null;
	
	private FunctionalRequirement projectRequirement = null;
	private Long project_id = (long) 0;
	private ArrayList<String> glossary = new ArrayList<String>();
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	Button deleteButton = null;
	
	public class ProjectRequirementsValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtName,
			        TextFieldLength.PROJECT_REQUIREMENT_NAME_MIN_LENGTH,
			        TextFieldLength.PROJECT_REQUIREMENT_NAME_MAX_LENGTH,
			        false));

			validator.add(new TextBoxValidator(txtDescription,
			        TextFieldLength.PROJECT_REQUIREMENT_DESCRIPTION_MIN_LENGTH,
			        TextFieldLength.PROJECT_REQUIREMENT_DESCRIPTION_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
				
		}
	};
	
	public ProjectRequirementsValidator dialogValidator = new ProjectRequirementsValidator();
	
	public ViewAddEditFunctionalRequirements(Shell parent, FunctionalRequirement data, Long project_id) {
		super(parent);
		
		this.projectRequirement = data;
		this.project_id = project_id;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent){ 
		super.createButtonsForButtonBar(parent);
		
		if (projectRequirement != null) {
			deleteButton = createButton(parent, 2,"Delete",true); 
			
			deleteButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					DBHelper.deleteEntity(DBConstants.PROJECT_REQUIREMENT_TABLE, projectRequirement.id, projectRequirement.cloudid);
					close();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					
				}
			});
			
			Button okCancel = getButton(IDialogConstants.CANCEL_ID);
			if (okCancel != null) {
				okCancel.setFocus();
			}
		}
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Project Requirements");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtName = Utility.createFormFields(area,
				         "Name", 
				         "", 
				         TextFieldLength.PROJECT_REQUIREMENT_NAME_MIN_LENGTH,
				         TextFieldLength.PROJECT_REQUIREMENT_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		txtDescription = Utility.createFormFields(area,
				         "Description", 
				         "", 
				         TextFieldLength.PROJECT_REQUIREMENT_DESCRIPTION_MIN_LENGTH,
				         TextFieldLength.PROJECT_REQUIREMENT_DESCRIPTION_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         true,
				         false,
				         false);
		
		if (projectRequirement != null) {
			txtName.setText(projectRequirement.name);
			txtDescription.setText(projectRequirement.description);
		}
		
		TextBoxKeyListener listener1 = new TextBoxKeyListener(project_id,txtName, glossary);
		txtName.addListener(SWT.KeyUp,listener1);
		
		TextBoxKeyListener listener2 = new TextBoxKeyListener(project_id,txtDescription, glossary);
		txtDescription.addListener(SWT.KeyUp,listener2);
		
		return area;
	}
	
	@Override
	protected void okPressed() {
	    String name = txtName.getText().trim();
	    String description = txtDescription.getText().trim();
	    
	    if (projectRequirement == null) {
	    	FunctionalRequirementModel.createProjectRequirement(project_id, name, description);
	    }
	    else {
	    	FunctionalRequirementModel.updateProjectRequirement(projectRequirement.id,name,description);
	    }
	    
		super.okPressed();
	}
}

