package com.wadia.iise.views;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxisSet;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries.SeriesType;

/*
 * Purpose: 
 * CRUD project
 */
public class ViewSmellCodeReport extends Dialog {
	private Chart chart = null;
	HashMap<String, Double> smellCountMap;
	
	public ViewSmellCodeReport(Shell parent, 
							   HashMap<String, Double> smellCountMap) {
		super(parent);
		this.smellCountMap = smellCountMap;
		
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite area = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(area, SWT.NULL);
        container.setLayout(new GridLayout(1, true));
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
        String [] labels = new String[smellCountMap.size()];
        double [] actualValues = new double[smellCountMap.size()];
        int index = 0;
        for (Map.Entry<String, Double> entry: smellCountMap.entrySet()) {
        	labels[index] = entry.getKey();
        	actualValues[index] = entry.getValue();
        	index++;
        }
		
        createChart(container, "Code Smells", "Code Smell", "Summary", labels, actualValues);
        chart.setLayoutData(new GridData(1200, 500));
        
        
		return area;
	}
	
	@Override
    protected Point getInitialSize() {
      return new Point(1000, 1000);
    }

    @Override
	public boolean isResizable() {
		return true;
	}
	
    private Chart createChart(final Composite parent,
			 String title,
			 String xAxisLabel,
			 String yAxisLabel,
			 String [] labels,
			 double [] actualValues)
		{
		
			if (chart == null) {
				chart = new Chart(parent, SWT.NONE);
			}
			
			chart.getTitle().setText(title);
			chart.getAxisSet().getXAxis(0).getTitle().setText(xAxisLabel);
			chart.getAxisSet().getYAxis(0).getTitle().setText(yAxisLabel);
			IAxisSet axisSet = chart.getAxisSet();
			IAxis xAxis = axisSet.getXAxis(0);
			xAxis.setCategorySeries(labels);
			xAxis.enableCategory(true);
			xAxis.getTick().setTickLabelAngle(45);
			
			IBarSeries series = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "Count");
			series.setBarColor(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
			double[] values = actualValues;
			series.setYSeries(values);
			
			chart.getAxisSet().adjustRange();
			
			return chart;
		
		}

    
	
}
