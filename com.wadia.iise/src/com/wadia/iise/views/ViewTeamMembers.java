package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.Team;
import com.wadia.iise.db.TeamMemberModel;
import com.wadia.iise.helper.TableHelper;

/*
 * Purpose: 
 * View team members...
 */

public class ViewTeamMembers extends TitleAreaDialog{

	private ComboViewer comboProject = null;
	private TableViewer viewer = null;
	
	ArrayList<Project> projects = null;
	
	public ViewTeamMembers(Shell parentShell, ArrayList<Project> projects) {
		super(parentShell);
		this.projects = projects;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Team Members");
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 3; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		Label lblProject = new Label(container1, SWT.NONE);
		lblProject.setText("Project");
		
		comboProject = new ComboViewer(container1,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name;
			    }
			    return super.getText(element);
			}
		});
		
		comboProject.setSelection(new StructuredSelection(projects.get(0)));
		
		comboProject.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				Project project = (Project) selection.getFirstElement();
				viewer.setInput(TeamMemberModel.getAllTeamMembers(project.id));
			}
		});
		
		Button btnNewButton = new Button(container1, SWT.NONE);
		btnNewButton.setText("Add New Team Member");
		btnNewButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection selection = (IStructuredSelection) comboProject.getSelection();
				Project project = (Project) selection.getFirstElement();
				
				ViewAddEditTeamMember dialog = new ViewAddEditTeamMember(new Shell(), null, project.id);
				dialog.create();
				if (dialog.open() != Window.CANCEL) {
					viewer.setInput(TeamMemberModel.getAllTeamMembers(project.id));
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
									   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		viewer.setInput(TeamMemberModel.getAllTeamMembers(projects.get(0).id));
		
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				TableItem[] items = viewer.getTable().getSelection();
				
				if (items.length == -1) {
					return;
				}
				
				TableItem item = items[0];
				Team team = (Team) item.getData();
				
				ViewAddEditTeamMember dialog = new ViewAddEditTeamMember(new Shell(), team, team.project_id);
				dialog.create();
				if (dialog.open() != Window.CANCEL) {
					viewer.setInput(TeamMemberModel.getAllTeamMembers(team.project_id));
				}
					
			}
		});
		
		return area;
	}

	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Name", "RollNo"};
	    int[] bounds = { 300, 300};

	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        Team t = (Team) element;
	        return t.name;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        Team t = (Team) element;
	        return t.rollno;
	      }
	    });
	}
}
