package com.wadia.iise.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.SourceUnitModel;


/*
 * Purpose: 
 * View Complexity details...
 */
public class ViewPowerLawPlot{

	private Long projectId;
	private Timestamp t;
	
	public ViewPowerLawPlot(Long projectId, Timestamp t) {
		this.projectId = projectId;
		this.t = t;
		
		final DefaultCategoryDataset dataset = createDataset();
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        
        JPanel panel = new JPanel();
        panel.setAutoscrolls(true);
        panel.setPreferredSize(new Dimension(400,400));
        
        JScrollPane pane = new JScrollPane(chartPanel);
        
        JFrame frame = new JFrame();
		frame.setContentPane(pane);
		frame.setSize(2000, 2000);
		
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		frame.setVisible(true);
	
        
	}
	
	private DefaultCategoryDataset createDataset() {
        
		ArrayList<SourceUnit> units = SourceUnitModel.getAllSourceUnitsByTimeStamp(projectId, t);
		Collections.sort(units, new Comparator<SourceUnit>() {
			
			@Override
			public int compare(SourceUnit o1, SourceUnit o2) {
				
				int value1 = (int) (o1.numFields + o1.numTokens + o1.numMethods);
				int value2 = (int) (o2.numFields + o2.numTokens + o2.numMethods);
				
				return (value1 - value2);
			}
		});

        final String series1 = "Series";
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        int position_counter = 1;
        
        for (SourceUnit unit: units) {
        	Double count = (double) (unit.numFields + unit.numTokens + unit.numMethods);
        	
        	if (count == 0) {
        		continue;
        	}
        	
        	dataset.addValue((Number)Math.log10(count), series1, Math.log10(position_counter));
        	
        	position_counter = position_counter + 1;
        }
        
        return dataset;        
    }
	
	private JFreeChart createChart(final DefaultCategoryDataset dataset) {
	    
	    // create the chart...
		final JFreeChart chart = ChartFactory.createLineChart(
		            "Power Law Chart",       
		            "Compilation Unit",        
		            "Number of Tokens",        
		            dataset,                   
		            PlotOrientation.VERTICAL,  
		            true,                      
		            true,                      
		            false                      
		        );
	    
		chart.setBackgroundPaint(Color.white);
	
	    chart.getCategoryPlot().getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.DOWN_90); 
	    
	    Font font = new Font("Dialog", Font.PLAIN, 9);
	    chart.getCategoryPlot().getDomainAxis().setTickLabelFont(font);
	    
	    return chart;
	    
	}
}
