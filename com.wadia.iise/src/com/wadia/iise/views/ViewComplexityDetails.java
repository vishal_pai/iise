package com.wadia.iise.views;

import java.sql.Timestamp;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.wadia.iise.dataobjects.MethodComplexity;
import com.wadia.iise.db.SourceMethodModel;
import com.wadia.iise.helper.TableHelper;


/*
 * Purpose: 
 * View Complexity details...
 */
public class ViewComplexityDetails extends TitleAreaDialog{

	private TableViewer viewer = null;
	private Long projectId;
	private Timestamp t;
	
	public ViewComplexityDetails(Shell parentShell, Long projectId, Timestamp t) {
		super(parentShell);
		this.projectId = projectId;
		this.t = t;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Complexity of Methods");
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Label lblProject = new Label(area, SWT.NONE);
		lblProject.setText("Method Analysis Details");
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
									   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		viewer.setInput(SourceMethodModel.getAllSourceMethodsByTimeStamp(projectId, t));
		
		return area;
	}

	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Analyzed Date", "Unit Name", "Method Name", "Complexity" };
	    int[] bounds = { 150, 150, 75, 75};

	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        MethodComplexity m = (MethodComplexity) element;
	        return m.analysisdate.toString();
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  MethodComplexity m = (MethodComplexity) element;
		      return m.unitName;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[2], bounds[2], 2, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  MethodComplexity m = (MethodComplexity) element;
		      return m.methodName;
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[3], bounds[3], 3, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  MethodComplexity m = (MethodComplexity) element;
		      return m.complexity.toString();
	      }
	    });
	}
}
