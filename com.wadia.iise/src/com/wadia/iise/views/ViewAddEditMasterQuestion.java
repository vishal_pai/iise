package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.MasterQuestion;
import com.wadia.iise.db.MasterQuestionModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;


/*
 * Purpose: 
 * CRUD master question
 */
public class ViewAddEditMasterQuestion extends TitleAreaDialog {

	private Text txtQuestion = null;
	
	private String type = "";
	private MasterQuestion masterQuestion = null;
	
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	public class MasterQuestionValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtQuestion,
			        TextFieldLength.TEAM_MEMBER_NAME_MIN_LENGTH,
			        TextFieldLength.TEAM_MEMBER_NAME_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
				
		}
	};
	
	public MasterQuestionValidator dialogValidator = new MasterQuestionValidator();
	
	public ViewAddEditMasterQuestion(Shell parent, 
									 MasterQuestion data, 
									 String type) {
		super(parent);
		
		this.masterQuestion = data;
		this.type = type;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Master Question Details...");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtQuestion = Utility.createFormFields(area,
				         "Question", 
				         "", 
				         TextFieldLength.QUESTION_MIN_LENGTH,
				         TextFieldLength.QUESTION_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         true,
				         false,
				         false);
		
		if (masterQuestion != null) {
			txtQuestion.setText(masterQuestion.question);
		}
		
		return area;
	}
	
	@Override
	protected void okPressed() {
	    String question = txtQuestion.getText().trim();
	    
	    if (masterQuestion == null) {
	    	MasterQuestionModel.createMasterQuestion(question, type);
	    }
	    
		super.okPressed();
	}
}

