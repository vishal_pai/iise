package com.wadia.iise.views;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxisSet;
import org.swtchart.ILineSeries;
import org.swtchart.ISeries.SeriesType;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.TimeUnitLoc;
import com.wadia.iise.db.ProjectModel;


/**
 * 
 * @author ggrec
 *
 */
public class ViewCodeChangesMonitorReport extends Dialog
{
	private Composite container;
	private Chart chart = null;
    private ComboViewer comboProject = null;
	private ArrayList<Project> projects = null;
	
	public ViewCodeChangesMonitorReport(Shell parentShell, ArrayList<Project> projects) {
		super(parentShell);
		this.projects = projects;
	}
	
	
	private void drawChart(Project project) {
		
		ArrayList<TimeUnitLoc> analysisList = null;
		ArrayList<String> labels = new ArrayList<>();
		ArrayList<Double> actualValues = new ArrayList<>();
		
		try {
			analysisList = ProjectModel.getLOCByTime(project.id);
			for (TimeUnitLoc timeUnitLoc: analysisList) {
				labels.add(timeUnitLoc.analysisDateTime.toString());
				actualValues.add(timeUnitLoc.loc.doubleValue());
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        Chart chart1 = createChart(container,
        						   "Code Changes Report",
        						   "Time",
        						   "Loc",
        						   labels.toArray(new String[labels.size()]),
        						   convertDoubles(actualValues));
        
        chart1.setLayoutData(new GridData(1200, 500));
        chart1.redraw();
	}
	
	
	public static double[] convertDoubles(List<Double> doubles)
	{
	    double[] ret = new double[doubles.size()];
	    
	    for(int i = 0; i < ret.length; i++) {
	    	ret[i] = doubles.get(i).doubleValue();
	    }
	    
	    return ret;
	}

	
    @Override
    protected Composite createDialogArea(final Composite parent)
    {
        final Composite dialogArea = (Composite) super.createDialogArea(parent);
        
        container = new Composite(dialogArea, SWT.NULL);
        container.setLayout(new GridLayout(1, true));
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Label lblProject = new Label(container, SWT.NONE);
		lblProject.setText("Project");
		
		comboProject = new ComboViewer(container,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name;
			    }
			    return super.getText(element);
			}
		});
		
		comboProject.setSelection(new StructuredSelection(projects.get(0)));
	    
		comboProject.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				Project project = (Project) selection.getFirstElement();
				drawChart(project);
			}
		});
		
		Project project = projects.get(0);
		
		
		drawChart(project);
          
        return dialogArea;
    } 
    
    @Override
    protected Point getInitialSize() {
      return new Point(1000, 1000);
    }

    @Override
	public boolean isResizable() {
		return true;
	}


    private Chart createChart(final Composite parent,
    								 String title,
    								 String xAxisLabel,
    								 String yAxisLabel,
    								 String [] labels,
    								 double [] actualValues)
    {
    	
    	if (chart == null) {
    		chart = new Chart(parent, SWT.NONE);
    	}
    	
    	chart.getTitle().setText(title);
    	chart.getAxisSet().getXAxis(0).getTitle().setText(xAxisLabel);
    	chart.getAxisSet().getYAxis(0).getTitle().setText(yAxisLabel);
    	IAxisSet axisSet = chart.getAxisSet();
    	IAxis xAxis = axisSet.getXAxis(0);
    	xAxis.setCategorySeries(labels);
    	xAxis.enableCategory(true);
    	xAxis.getTick().setTickLabelAngle(45);
    	ILineSeries series1 = (ILineSeries) chart.getSeriesSet().createSeries(SeriesType.LINE, "LOC Count");
    	double[] values1 = actualValues;
    	series1.setYSeries(values1);
    	
    	chart.getAxisSet().adjustRange();
    	
    	return chart;

    }
}
