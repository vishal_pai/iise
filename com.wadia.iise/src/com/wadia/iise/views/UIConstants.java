package com.wadia.iise.views;

//Strings on UI...
public class UIConstants {
	public static String HARDWARE = "Hardware";
	public static String SOFTWARE = "Software";
	public static String DEVELOPMENT = "Development";
	public static String NON_FUNCTIONAL = "Non Functional";
	
	public static String ECONOMIC = "Economic";
	public static String OPERATIONAL = "Operational";
	public static String TECHNICAL = "Technical";
	
}
