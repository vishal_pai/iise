package com.wadia.iise.views;

import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.wadia.iise.dataobjects.Feasibility;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.db.FeasibilityModel;
import com.wadia.iise.db.GlossaryModel;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.helper.MultiLineTableListener;
import com.wadia.iise.helper.TableHelper;

/*
 * Purpose: 
 * View Delete projects
 */

public class ViewDeleteProjects extends TitleAreaDialog{

	private ComboViewer comboProject = null;
	ArrayList<Project> projects = null;
	
	public ViewDeleteProjects(Shell parentShell, ArrayList<Project> projects) {
		super(parentShell);
		this.projects = projects;
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	@Override
	public void create() {
	    super.create();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 3; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		Label lblProject = new Label(container1, SWT.NONE);
		lblProject.setText("Project");
		
		setTitle("Delete Project");
		
		comboProject = new ComboViewer(container1,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name + " (Start Date: " + project.startDate.toString() + "  End Date: " + project.endDate.toString() + ")";
			    }
			    return super.getText(element);
			}
		});
			
		return area;
	}
	
	@Override
	protected void okPressed() {
		IStructuredSelection selection = (IStructuredSelection) comboProject.getSelection();
		Project project = (Project) selection.getFirstElement();
	    try {
			ProjectModel.deleteProject(project.id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.okPressed();
	}
}
