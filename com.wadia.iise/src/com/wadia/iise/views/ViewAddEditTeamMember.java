package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Team;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.TeamMemberModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;


/*
 * Purpose: 
 * CRUD team members
 */
public class ViewAddEditTeamMember extends TitleAreaDialog {

	private Text txtName = null;
	private Text txtRoll = null;
	
	private Team team = null;
	private Long project_id = null;
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	Button deleteButton = null;

	public class TeamMemberValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtName,
			        TextFieldLength.TEAM_MEMBER_NAME_MIN_LENGTH,
			        TextFieldLength.TEAM_MEMBER_NAME_MAX_LENGTH,
			        false));

			validator.add(new TextBoxValidator(txtRoll,
			        TextFieldLength.TEAM_MEMBER_ROLLNO_MIN_LENGTH,
			        TextFieldLength.TEAM_MEMBER_ROLLNO_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
				
		}
	};
	
	public TeamMemberValidator dialogValidator = new TeamMemberValidator();
	
	public ViewAddEditTeamMember(Shell parent, Team data, Long project_id) {
		super(parent);
		
		this.team = data;
		this.project_id = project_id;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent){ 
		super.createButtonsForButtonBar(parent);
		
		if (team != null) {
			deleteButton = createButton(parent, 2,"Delete",true); 
			
			deleteButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					DBHelper.deleteEntity(DBConstants.TEAM_TABLE, team.id, team.cloudid);
					close();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					
				}
			});
			
			Button okCancel = getButton(IDialogConstants.CANCEL_ID);
			if (okCancel != null) {
				okCancel.setFocus();
			}
		}
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Team Member Details...");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtName = Utility.createFormFields(area,
				         "Name", 
				         "", 
				         TextFieldLength.TEAM_MEMBER_NAME_MIN_LENGTH,
				         TextFieldLength.TEAM_MEMBER_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		txtRoll = Utility.createFormFields(area,
				         "RollNo", 
				         "", 
				         TextFieldLength.TEAM_MEMBER_ROLLNO_MIN_LENGTH,
				         TextFieldLength.TEAM_MEMBER_ROLLNO_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		if (team != null) {
			txtName.setText(team.name);
			txtRoll.setText(team.rollno);
		}
		
		return area;
	}
	
	@Override
	protected void okPressed() {
	    String name = txtName.getText().trim();
	    String roll = txtRoll.getText().trim();
	    
	    boolean result = false;
	    
	    if (team == null) {
	    	result = TeamMemberModel.createTeamMember(project_id, name, roll);
	    }
	    else {
	    	result = TeamMemberModel.updateTeamMember(team.id,name,roll);
	    }
	    
	    if (result == false) {
	    	MessageDialog.openInformation(new Shell(),
										  "IISE",
										  "Could not add the team member. Possible reasons include duplicate roll number.");
	    }
	    else {
	    	super.okPressed();
	    }
	}
}

