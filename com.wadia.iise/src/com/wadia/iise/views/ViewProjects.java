package com.wadia.iise.views;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.helper.TableHelper;

/*
 * Purpose: 
 * View projects...
 */

public class ViewProjects  extends TitleAreaDialog {
	
	private TableViewer viewer = null;
	
	public ViewProjects(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("View Projects...");
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(new GridLayout(1, false));
		
		// define the TableViewer
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL
		      | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		// create the columns 
		createColumns(viewer);

		// make lines and header visible
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		//Below code is used to simulate derby failure on thread interruption...
		//Use it to produce the error and check if system recovers correctly...
		//Whenever exception is hit in *model files we clear the interrupt flag...
		//Thread.currentThread().interrupt();
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(ProjectModel.getAllProjects());
		
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				TableItem[] items = viewer.getTable().getSelection();
				
				if (items.length == -1) {
					return;
				}
				
				TableItem item = items[0];
				Project project = (Project) item.getData();
				
				ViewAddEditProject dialog = new ViewAddEditProject(getShell(), project);
				dialog.create();
				if (dialog.open() == Window.OK) {
					viewer.setInput(ProjectModel.getAllProjects());
				}
				
			}
		});
		
		return area;
	}

	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Name", "Start Date", "End Date"};
	    int[] bounds = { 200, 200, 200};

	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        Project p = (Project) element;
	        return p.name;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        Project p = (Project) element;
	        return p.startDate.toString();
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[2], bounds[2], 2, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        Project p = (Project) element;
	        return p.endDate.toString();
	      }
	    });
	}
	
}
