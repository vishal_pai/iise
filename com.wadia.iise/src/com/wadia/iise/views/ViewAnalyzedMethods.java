package com.wadia.iise.views;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.wadia.iise.dataobjects.SourceMethod;
import com.wadia.iise.db.SourceMethodModel;
import com.wadia.iise.helper.TableHelper;


/*
 * Purpose: 
 * View Analyzed methods...
 */
public class ViewAnalyzedMethods extends TitleAreaDialog{

	private TableViewer viewer = null;
	private Long sourceUnitId;
	
	public ViewAnalyzedMethods(Shell parentShell, Long projectId) {
		super(parentShell);
		this.sourceUnitId = projectId;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Analyzed Methods");
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Label lblProject = new Label(area, SWT.NONE);
		lblProject.setText("Method Analysis Details");
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
									   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		viewer.setInput(SourceMethodModel.getAllSourceMethods(sourceUnitId));
		
		return area;
	}

	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Analyzed Date", "Name", "# Lines", "# Tokens", "# Comments", "# Blanks", "Complexity" };
	    int[] bounds = { 150, 150, 75, 75, 75, 75, 75};

	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        SourceMethod s = (SourceMethod) element;
	        return s.dateTime.toString();
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        SourceMethod s = (SourceMethod) element;
	        return s.name;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[2], bounds[2], 2, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceMethod s = (SourceMethod) element;
		      return s.numLines.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[3], bounds[3], 3, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceMethod s = (SourceMethod) element;
		      return s.numTokens.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[4], bounds[4], 4, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceMethod s = (SourceMethod) element;
		      return s.numComments.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[5], bounds[5], 5, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceMethod s = (SourceMethod) element;
		      return s.numBlanks.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[6], bounds[6], 6, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceMethod s = (SourceMethod) element;
		      return s.complexity.toString();
	      }
	    });
	}
}
