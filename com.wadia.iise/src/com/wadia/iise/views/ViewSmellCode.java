package com.wadia.iise.views;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

import com.wadia.iise.Activator;
import com.wadia.iise.codesmelldetectors.CommentedOutCodeDetector;
import com.wadia.iise.codesmelldetectors.ComplexityDetector;
import com.wadia.iise.codesmelldetectors.DataClassDetector;
import com.wadia.iise.codesmelldetectors.DataClumpDetector;
import com.wadia.iise.codesmelldetectors.DivergentChangeDetector;
import com.wadia.iise.codesmelldetectors.DuplicateCodeDetector;
import com.wadia.iise.codesmelldetectors.FeatureEnvyDetector;
import com.wadia.iise.codesmelldetectors.FieldDetail;
import com.wadia.iise.codesmelldetectors.LargeClassDetector;
import com.wadia.iise.codesmelldetectors.LargeMethodDetector;
import com.wadia.iise.codesmelldetectors.MethodDetail;
import com.wadia.iise.codesmelldetectors.FieldNameLengthDetector;
import com.wadia.iise.codesmelldetectors.ShotGunSurgeryDetector;
import com.wadia.iise.codesmelldetectors.SmellConstants;
import com.wadia.iise.codesmelldetectors.SwitchDetector;
import com.wadia.iise.codesmelldetectors.TooManyParamsDetector;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.CodeSmellVisitor;

/*
 * Purpose: 
 * Smelling code...
 * 
 */
public class ViewSmellCode  extends TitleAreaDialog {
	Label lbl = null;
	private ProgressBar bar = null;
	private static final String JDT_NATURE = "org.eclipse.jdt.core.javanature";
	
	private HashMap<String, Double> smellCount = new HashMap<>();
	private HashMap<String, MethodDetail> allMethods = new HashMap<>();
	private HashMap<String, ArrayList<FieldDetail>> allFields = new HashMap<>();
	private int totalUnits = 0;
	private int totalMethods = 0;
	
	public ViewSmellCode(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Smell Code...");
	}
	
	private void addToMap(String key, Double count) {
		if (smellCount.containsKey(key)) {
			smellCount.put(key,smellCount.get(key) + count);
		}
		else {
			smellCount.put(key,count);
		}
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		lbl = new Label(area, SWT.NONE);
		lbl.setText("Smell source code: ");
		lbl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		bar = new ProgressBar (area, SWT.SMOOTH);
		bar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		return area;
	}
	
	private void analyzeProject(IProject project, Display display) throws IOException, SQLException, CoreException {
	    
		IPackageFragment[] packages = JavaCore.create(project).getPackageFragments();
	    
	    allMethods.clear();
	    allFields.clear();
	    totalUnits = 0;
	    totalMethods = 0;
	    
	    for (IPackageFragment mypackage : packages) {
	    	if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
	    		analyzePackage(mypackage, project, display);
	    	}
	    }
	    
	    //data clumps detection for parameter list...
	    int numClumpIssues = DataClumpDetector.detectAndMark(allMethods, allFields, project, packages, display, lbl, bar);
	    addToMap(SmellConstants.KEY_CLUMPS, (double) numClumpIssues);
	    
	    //finding duplicate code...
	    int numDuplicateIssues = DuplicateCodeDetector.detectAndMark(allMethods, project, packages, display, lbl, bar);
	    addToMap(SmellConstants.KEY_DUPLICATE_METHOD, (double) numDuplicateIssues);
	    
	    //Divergent changes...
	    int numDivergentUnitIssues = DivergentChangeDetector.detectAndMark(project, packages, display, lbl, bar);
	    addToMap(SmellConstants.KEY_DIVERGENT_UNIT, (double) numDivergentUnitIssues);
	    
	    //Shotgun surgery...
	    int numShotgunSurgery = ShotGunSurgeryDetector.detectAndMark(project, packages, display, lbl, bar);
	    addToMap(SmellConstants.KEY_SHOTGUN_SURGERY, (double) numShotgunSurgery);
	    
	    totalMethods = allMethods.size();
	    
	    allMethods.clear();
	    allFields.clear();
	}
	
	private void analyzePackage(IPackageFragment mypackage, 
						   	    IProject project,
						   	    Display display) throws IOException, SQLException, CoreException {
		
		for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
			
			totalUnits++;
			
			display.asyncExec(new Runnable() {
				
				@Override
				public void run() {
					if (lbl.isDisposed() == false) {
						lbl.setText(unit.getPath().toString());
						lbl.getParent().layout();
					}
					
					if (bar.isDisposed() == false) {
						if (bar.getSelection() < 50) {
							bar.setSelection(bar.getSelection() + 1);
						}
					}
					
					IMarker[] markers;
					try {
						markers = unit.getResource().findMarkers("com.wadia.iise.codesmellmarker", 
								true, 
								IResource.DEPTH_ZERO);
						for (IMarker marker: markers) {
							marker.delete();
						}
					} catch (CoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
			
			CompilationUnit parse = parse(unit);
			
			CodeSmellVisitor codeSmellVisitor = new CodeSmellVisitor(unit, parse);
			parse.accept(codeSmellVisitor);
			
			//Field length code smell...
	    	int numFieldLengthIssues = FieldNameLengthDetector.detectAndMark(codeSmellVisitor,display);
	    	addToMap(SmellConstants.KEY_INCORRECT_FIELD, (double) numFieldLengthIssues);
	    	
	    	//Data class smell...
	    	int numDataClassIssues = DataClassDetector.detectAndMark(codeSmellVisitor,display);
	    	addToMap(SmellConstants.KEY_DATA_CLASS, (double) numDataClassIssues);
	    	
	    	//Get class information...
	    	int numLargeClassIssues = LargeClassDetector.detectAndMark(codeSmellVisitor,display);
	    	addToMap(SmellConstants.KEY_LARGE_CLASS, (double) numLargeClassIssues);
	    	
	    	//Too many switch statements...
	    	int numSwitchIssues = SwitchDetector.detectAndMark(codeSmellVisitor,display);
	    	addToMap(SmellConstants.KEY_TOO_MANY_SWITCH, (double) numSwitchIssues);
	    	
	    	//large method smell...
		    int numLargeMethodIssues = LargeMethodDetector.detectAndMark(codeSmellVisitor,display);
		    addToMap(SmellConstants.KEY_LARGE_METHOD, (double) numLargeMethodIssues);
		    
		    //too many parameters smell...
		    int numTooManyParamIssues = TooManyParamsDetector.detectAndMark(codeSmellVisitor,display);
		    addToMap(SmellConstants.KEY_TOO_MANY_PARAMS, (double) numTooManyParamIssues);
		    
	    	//Get comment information...
	    	int numCommentedOutCodeIssues = CommentedOutCodeDetector.detectAndMark(parse, unit, display);
	    	addToMap(SmellConstants.KEY_COMMENTEDOUT_CODE, (double) numCommentedOutCodeIssues);
	    	
	    	//Feature envy...
	    	int numFeatureEnvyIssues = FeatureEnvyDetector.detectAndMark(codeSmellVisitor, display);
	    	addToMap(SmellConstants.KEY_FEATURE_ENVY, (double) numFeatureEnvyIssues);
	    	
	    	//Complexity...
	    	int numComplexityIssues = ComplexityDetector.detectAndMark(codeSmellVisitor, display);
	    	addToMap(SmellConstants.KEY_COMPLEXITY, (double) numComplexityIssues);
	    	
			//Some smell detection require data across all source code...Collecting it here...
	    	for (Map.Entry<String, MethodDetail> entry1: codeSmellVisitor.methodsByUnit.entrySet()) {
	    	    String key1 = entry1.getKey();
	    	    MethodDetail methodDetail = entry1.getValue();
	    	    methodDetail.featureUsage.clear();
	    	    allMethods.put(key1, methodDetail);
	    	}
	    	
	    	for (Map.Entry<String, ArrayList<FieldDetail>> entry1: codeSmellVisitor.fieldsByUnit.entrySet()) {
	    	    String key1 = entry1.getKey();
	    	    ArrayList<FieldDetail> fieldDetails = entry1.getValue();
	    	    allFields.put(key1, fieldDetails);
	    	}
	    	
	    	codeSmellVisitor.parse = null;
	    	codeSmellVisitor.unit = null;
	    }
	 }

	  
	/**
	   * Reads a ICompilationUnit and creates the AST DOM for manipulating the
	   * Java source file
	   * 
	   * @param unit
	   * @return
	*/
	private static CompilationUnit parse(ICompilationUnit unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit);
		parser.setResolveBindings(true);
		return (CompilationUnit) parser.createAST(null); // parse
	}

	@Override
	protected void okPressed() {
		
		this.getButton(IDialogConstants.OK_ID).setEnabled(false);
		this.getButton(IDialogConstants.CANCEL_ID).setEnabled(false);
		
		class MyRunnable implements Runnable {
			
			private Display display;
			
			public MyRunnable(Display display) {
				this.display = display;
			}
			
			@Override
			public void run() {
				IWorkspace workspace = ResourcesPlugin.getWorkspace();
			    IWorkspaceRoot root = workspace.getRoot();
			    IProject[] projects = root.getProjects();
			    
			    for (IProject project : projects) {
			    	try {
			    		if (project.isNatureEnabled(JDT_NATURE)) {
			    			analyzeProject(project, display);
			    		}
			    	} 
			    	catch (CoreException | IOException e) {
			    		e.printStackTrace();
			    		Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
			    	} catch (SQLException e) {
						e.printStackTrace();
						Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
					}
			    }
			    
			    display.asyncExec(new Runnable() {
					
					@Override
					public void run() {
						
						smellCount.put(SmellConstants.KEY_INCORRECT_FIELD,
									   (smellCount.get(SmellConstants.KEY_INCORRECT_FIELD) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_DATA_CLASS,
								   (smellCount.get(SmellConstants.KEY_DATA_CLASS) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_LARGE_CLASS,
								   (smellCount.get(SmellConstants.KEY_LARGE_CLASS) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_TOO_MANY_SWITCH,
								   (smellCount.get(SmellConstants.KEY_TOO_MANY_SWITCH) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_LARGE_METHOD,
								   (smellCount.get(SmellConstants.KEY_LARGE_METHOD) * 100 / totalMethods));
						
						smellCount.put(SmellConstants.KEY_TOO_MANY_PARAMS,
								   (smellCount.get(SmellConstants.KEY_TOO_MANY_PARAMS) * 100 / totalMethods));
						
						smellCount.put(SmellConstants.KEY_COMMENTEDOUT_CODE,
								   (smellCount.get(SmellConstants.KEY_COMMENTEDOUT_CODE) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_FEATURE_ENVY,
								   (smellCount.get(SmellConstants.KEY_FEATURE_ENVY) * 100 / totalMethods));
						
						smellCount.put(SmellConstants.KEY_COMPLEXITY,
								   (smellCount.get(SmellConstants.KEY_COMPLEXITY) * 100 / totalMethods));
						
						smellCount.put(SmellConstants.KEY_CLUMPS,
								   (smellCount.get(SmellConstants.KEY_CLUMPS) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_DUPLICATE_METHOD,
								   (smellCount.get(SmellConstants.KEY_DUPLICATE_METHOD) * 100 / totalMethods));
						
						smellCount.put(SmellConstants.KEY_DIVERGENT_UNIT,
								   (smellCount.get(SmellConstants.KEY_DIVERGENT_UNIT) * 100 / totalUnits));
						
						smellCount.put(SmellConstants.KEY_SHOTGUN_SURGERY,
								   (smellCount.get(SmellConstants.KEY_SHOTGUN_SURGERY) * 100 / totalUnits));
						
						ViewSmellCodeReport dialog = new ViewSmellCodeReport(getShell(), 
																			 smellCount);
						dialog.open();
						
						ViewSmellCode.super.okPressed();
						
					}
				});
			}
		}
		
		MyRunnable runnable = new MyRunnable(Display.getDefault());
		Thread thread = new Thread(runnable);
		thread.start();
	}
}
