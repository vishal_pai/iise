package com.wadia.iise.views;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.helper.TableHelper;

/*
 * Purpose: 
 * View dependency details...
 */

public class ViewModuleDetails extends TitleAreaDialog{
	
	private Long projectId;
	private Timestamp t;
	private TableViewer viewer = null;
	private boolean isCoordinator = false;
	
	public ViewModuleDetails(Shell parentShell, Long projectId, Timestamp t, boolean isCoordinator) {
		super(parentShell);
		this.projectId = projectId;
		this.t = t;
		this.isCoordinator = isCoordinator;
	}

	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	public void create() {
	    super.create();
	    
	    if (isCoordinator == true) {
	    	setTitle("Coordinator Module Information");
	    }
	    else {
	    	setTitle("Utility Module Information");
	    }
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		
		Label lblCoordinator = new Label(area, SWT.NONE);
		lblCoordinator.setText("Ranking");
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
				   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		createCoordinatorColumns(viewer);

		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		ArrayList<SourceUnit> coordinatorList = SourceUnitModel.getAllSourceUnitsByTimeStamp(projectId, t);
		
		if (isCoordinator == true) {
			Collections.sort(coordinatorList, new Comparator<SourceUnit>() {
	
				@Override
				public int compare(SourceUnit o1, SourceUnit o2) {
					return (int)(o2.callerCount - o1.callerCount);
				}
			});
		}
		else {
			Collections.sort(coordinatorList, new Comparator<SourceUnit>() {
				
				@Override
				public int compare(SourceUnit o1, SourceUnit o2) {
					return (int)(o2.calleeCount - o1.calleeCount);
				}
			});
		}
		
		viewer.setInput(coordinatorList);
		
		return area;
	}
	
	private void createCoordinatorColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		
		String[] titles = new String[2];
		titles[0] = "Module";
	    if (isCoordinator == true) {
	    	titles[1] = "# Calls Made";
	    }
	    else {
	    	titles[1] = "# Called In";
	    }
		
		int[] bounds = { 400, 100};

	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        SourceUnit su = (SourceUnit) element;
	        return su.path;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit su = (SourceUnit) element;
		      
	    	  if (isCoordinator == true) {
	    		  return su.callerCount.toString();
	    	  }
	    	  else {
	    		  return su.calleeCount.toString();
	    	  }
	      }
	    });
	}
}
