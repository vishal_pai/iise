package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Glossary;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.GlossaryModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;


/*
 * Purpose: 
 * CRUD glossary
 */
public class ViewAddEditGlossary extends TitleAreaDialog {

	private Text txtName = null;
	private Text txtDescription = null;
	
	private Glossary glossary = null;
	private Long project_id = null;
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	Button deleteButton = null;

	public class GlossaryValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtName,
					TextFieldLength.GLOSSARY_NAME_MIN_LENGTH,
					TextFieldLength.GLOSSARY_NAME_MAX_LENGTH,
					false));
			
			validator.add(new TextBoxValidator(txtDescription,
					TextFieldLength.GLOSSARY_DESCRIPTION_MIN_LENGTH,
					TextFieldLength.GLOSSARY_DESCRIPTION_MAX_LENGTH,
					false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
		}
	};
	
	public GlossaryValidator dialogValidator = new GlossaryValidator();
	
	public ViewAddEditGlossary(Shell parent, Glossary data, Long project_id) {
		super(parent);
		
		this.glossary = data;
		this.project_id = project_id;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent){ 
		super.createButtonsForButtonBar(parent);
		
		if (glossary != null) {
			deleteButton = createButton(parent, 2,"Delete",true); 
			
			deleteButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					DBHelper.deleteEntity(DBConstants.GLOSSARY_TABLE, glossary.id, glossary.cloudid);
					close();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					
				}
			});
			
			Button okCancel = getButton(IDialogConstants.CANCEL_ID);
			if (okCancel != null) {
				okCancel.setFocus();
			}
		}
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Glossary");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtName = Utility.createFormFields(area,
				         "Name", 
				         "", 
				         TextFieldLength.GLOSSARY_NAME_MIN_LENGTH,
				         TextFieldLength.GLOSSARY_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		
		txtDescription = Utility.createFormFields(area,
				         "Description", 
				         "", 
				         TextFieldLength.GLOSSARY_DESCRIPTION_MIN_LENGTH,
				         TextFieldLength.GLOSSARY_DESCRIPTION_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		if(glossary != null) {
			txtName.setText(glossary.name);
			txtDescription.setText(glossary.description);
		}
			
		return area;
	}
	
	@Override
	protected void okPressed() {
	    String name = txtName.getText().trim();
	    String description = txtDescription.getText().trim();
	    
	    if (glossary == null) {
	    	GlossaryModel.createGlossary(project_id, name, description);
	    }
	    else {
	    	GlossaryModel.updateGlossary(glossary.id,name,description);
	    }
	    
		super.okPressed();
	}
}

