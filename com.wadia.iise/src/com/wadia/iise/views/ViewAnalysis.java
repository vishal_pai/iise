package com.wadia.iise.views;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.DBConnection;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.db.SourceFieldModel;
import com.wadia.iise.db.SourceMethodModel;
import com.wadia.iise.db.SourceTokenModel;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.helper.MethodVariableVisitor;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.sourcecodevisitors.MccabeVisitor;
import com.wadia.iise.sourcecodevisitors.MethodVisitor;
import com.wadia.iise.sourcecodevisitors.VariableVisitor;

/*
 * Purpose: 
 * Analysis
 */
public class ViewAnalysis  extends TitleAreaDialog {

	Label lbl = null;
	private ProgressBar bar = null;
	private static final String JDT_NATURE = "org.eclipse.jdt.core.javanature";
	private Long numFields = new Long(0);
	private Long numTokens = new Long(0);
	private List<String> method_tokens = new ArrayList<String>();
	private List<String> unit_fields = new ArrayList<String>();
	
	private HashMap<String, Long> callee = new HashMap<String, Long>();
	private HashMap<String, Long> caller = new HashMap<String, Long>();
	
	public ViewAnalysis(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Analyze source code...");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		lbl = new Label(area, SWT.NONE);
		lbl.setText("Analyze source code: ");
		lbl.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		bar = new ProgressBar (area, SWT.SMOOTH);
		bar.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		return area;
	}
	
	private void analyzeProject(IProject project, 
								Timestamp t,
								Connection conn) throws JavaModelException, IOException, SQLException {
	    IPackageFragment[] packages = JavaCore.create(project).getPackageFragments();
	    
	    int totalCount = 0;
	    for (IPackageFragment mypackage : packages) {
	    	if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
	    		totalCount += 1;
	    	}
	    }
	    
	    float barUpdate = bar.getMaximum() / totalCount;
	    int index = 0;
	    
	    for (IPackageFragment mypackage : packages) {
	    	
	    	if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
	    		
	    		lbl.setText("Analyzing " + mypackage.getElementName());
	    		
	    		analyzePackage(mypackage, project, t, conn);
	    		index = index + 1;
	    		bar.setSelection(index * (int)barUpdate);
	    	}
	    }
	}
	
	private Long calcPhysicalLinesFromFile(String path) throws IOException {
		Long numLines = (long) 0;
		
		BufferedReader br = new BufferedReader(new FileReader(path));
		while (br.readLine() != null) {
			numLines = numLines + 1;
		}
		
		br.close();
		
		return numLines;
	}
	
	private Long calcBlankLinesFromFile(String path) throws IOException {
		String str;
		Long numBlanks = (long) 0;
		
		BufferedReader br = new BufferedReader(new FileReader(path));
		while ((str=br.readLine()) != null) {
			
			str = str.replace("\t", "");
			str = str.replace(" ", "");
			
			if (str.isEmpty()) {
				numBlanks = numBlanks + 1;
			}
		}
		
		br.close();
		
		return numBlanks;
	}
	
	private Long calcNumberOfComments(CompilationUnit parse) {
		//Get number of comments in the class...
		return (long) parse.getCommentList().size();
		
	}
	
	private Long calNumberOfBlanksFromList(ArrayList<String> lines) {
		Long numMethodBlanks = (long) 0;
		for (String line: lines) {
			line = line.replace("\t", "");
			line = line.replace(" ", "");
			if (line.length() == 0) {
				numMethodBlanks++;
			}
		}
		
		return numMethodBlanks;
	}
	
	private void analyzePackage(IPackageFragment mypackage, 
						   IProject project, 
						   Timestamp t,
						   Connection conn) throws JavaModelException, IOException, SQLException {
		
		Project p = ProjectModel.getProjectByName(project.getName());
		
		for (final ICompilationUnit unit : mypackage.getCompilationUnits()) {
	    	
			//Get number of blanks and number of physical lines in the class...
			final String path = unit.getResource().getRawLocation().toString();
			
	    	CompilationUnit parse = parse(unit);
			
			//Get number of data members, number of local variables in the methods
	    	//Also get the names of the data members
			VariableVisitor variableVisitor = new VariableVisitor();
	    	parse.accept(variableVisitor);
	    	
	    	numFields = variableVisitor.getNumberOfFields();
			numTokens = variableVisitor.getNumberOfTokens();
			unit_fields = variableVisitor.getFieldNames();
			
			//Get member function information...
		    MethodVisitor visitor = new MethodVisitor();
		    parse.accept(visitor);
		  
		    Long numMethods = (long) 0;
			
		    if (caller.containsKey(path) == false) {
		    	caller.put(path, (long)0);
		    }
		    
		    for (MethodDeclaration methodDeclaration : visitor.getMethods()) {
			    numMethods = numMethods + 1;
			    
			    if (methodDeclaration.getBody() == null) {
			    	//Example is interfaces simply declare methods not define them...
			    	continue;
			    }
			    
			    methodDeclaration.getBody().accept(new ASTVisitor() {

			    	public boolean visit(MethodInvocation node) {
			    		
			    		IBinding binding = node.resolveMethodBinding();
			    		if (binding == null) {
			    			return true;
			    		}
			    		
			    	    IMethod method = (IMethod)binding.getJavaElement();
			    	    if (method == null) {
			    	    	return true;
			    	    }
			    	    
			    	    ICompilationUnit callerUnit = method.getCompilationUnit();
			    	    
			    	    if (callerUnit == null) {
			    	    	return true;
			    	    }
			    	    
			    	    if (callerUnit.getPath().toString().equals(unit.getPath().toString()) == false) {
				    		caller.put(path, caller.get(path) + 1);
				    		
				    		String callerUnitName = callerUnit.getResource().getRawLocation().toString();
				    		
			    			if (callee.containsKey(callerUnitName) == false) {
			    				callee.put(callerUnitName, (long)1);
			    			}
			    			else {
			    				callee.put(callerUnitName, callee.get(callerUnitName) + 1);
			    			}
				    	}
			    	    
                        return true;
                    }
				});
			}
			
		    Long unitid = SourceUnitModel.createSourceUnit(p.id, 
							 t, 
							 mypackage.getElementName() + "." + unit.getElementName(), 
							 calcPhysicalLinesFromFile(path), 
							 calcNumberOfComments(parse), 
							 calcBlankLinesFromFile(path), 
							 numFields, 
							 numTokens, 
							 numMethods,
							 path,
							 (long)0,
							 (long)0);

			for (String fieldName: unit_fields) {
				SourceFieldModel.createSourceField(unitid, t, fieldName);
			}
			
			for (MethodDeclaration method: visitor.getMethods()) {
				
				IMethod otherRepresentation = (IMethod) method.resolveBinding().getJavaElement();
				
				Long methodComplexity = getComplexity(otherRepresentation, method);
			    
			    ASTParser parser = ASTParser.newParser(AST.JLS4); 
				parser.setSource(otherRepresentation.getSource().toCharArray());
				parser.setResolveBindings(true);
                CompilationUnit cu = (CompilationUnit)parser.createAST(null);
				
				ArrayList<String> lines = Utility.splitLines(otherRepresentation.getSource());
				
				MethodVariableVisitor methodVariableVisitor = new MethodVariableVisitor();
				method.accept(methodVariableVisitor);
				method_tokens = methodVariableVisitor.getMethodTokens();
				
				Long methodid = SourceMethodModel.createSourceMethod(unitid, 
																	 t, 
																	 method.getName().getFullyQualifiedName(), 
																	 (long)lines.size(), 
																	 (long)cu.getCommentList().size(), 
																	 calNumberOfBlanksFromList(lines), 
																	 (long)method_tokens.size(),
																	 methodComplexity);
				for (String token: method_tokens) {
					SourceTokenModel.createSourceToken(methodid, t, token);
				}
			}
		}
	 }

	  
	private Long getComplexity(IMethod otherRepresentation, MethodDeclaration method) throws JavaModelException {
		MccabeVisitor mc = new MccabeVisitor(otherRepresentation.getCompilationUnit().getSource());
		method.accept(mc);
		return mc.getComplexity();
	}

	/**
	   * Reads a ICompilationUnit and creates the AST DOM for manipulating the
	   * Java source file
	   * 
	   * @param unit
	   * @return
	*/
	private static CompilationUnit parse(ICompilationUnit unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(unit);
		parser.setResolveBindings(true);
		return (CompilationUnit) parser.createAST(null); // parse
	}

	@Override
	protected void okPressed() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
	    IWorkspaceRoot root = workspace.getRoot();
	    IProject[] projects = root.getProjects();
	    
	    java.util.Date date= new java.util.Date();
		Timestamp t = new Timestamp(date.getTime());
		
		callee.clear();
		caller.clear();
		
		Connection conn = null;
		
		conn = DBConnection.getConnection();
		
		try {
			conn.setAutoCommit(false);
			
		    for (IProject project : projects) {
		    	try {
		    		if (project.isNatureEnabled(JDT_NATURE)) {
		    			analyzeProject(project, t, conn);
		    		}
		    	} 
		    	catch (CoreException | IOException e) {
		    		e.printStackTrace();
		    		Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		    	} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
				}
		    }
		    
			for (Map.Entry<String, Long> entry : caller.entrySet()) {
				Long count = entry.getValue();
				if (count == 0) {
					continue;
				}
				
				String unitPath = entry.getKey();
			    
			    
			    SourceUnit srcUnit = SourceUnitModel.getSourceUnitByPath(unitPath, t);
			    SourceUnitModel.updateCallerCount(t, srcUnit.id, count);
			}
			
			for (Map.Entry<String, Long> entry : callee.entrySet()) {
			    
			    Long count = entry.getValue();
			    if (count == 0) {
					continue;
				}
			    
			    String unitPath = entry.getKey();
			    
			    SourceUnit srcUnit = SourceUnitModel.getSourceUnitByPath(unitPath, t);
			    SourceUnitModel.updateCalleeCount(t, srcUnit.id, count);
			}
			
			conn.commit();

		}
		catch (SQLException e) {
			
			try {
				conn.rollback();
			}
			catch(Exception e1) {
				e1.printStackTrace();
				Activator.log(Utility.getStackTraceFromException(e1), IStatus.ERROR);
			}
			
			e.printStackTrace();
			DBHelper.clearInterruptFlag(e);
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			callee.clear();
			caller.clear();
		}
		super.okPressed();
	}
}
