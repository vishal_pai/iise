package com.wadia.iise.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.helper.TextBoxKeyListener;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.DateRangeValidator;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;


/*
 * Purpose: 
 * CRUD project
 */
public class ViewAddEditProject extends TitleAreaDialog {

	private Text txtName = null;
	private Text txtProblem = null;
	private Text txtSolution = null;
	private DateTime dtStartDate = null;
	private DateTime dtEndDate = null;
	private ArrayList<String> glossary = new ArrayList<String>();
	private Project project = null;
	
	Button deleteButton = null;
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	public class ProjectDetailsValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtName,
			        TextFieldLength.PROJECT_NAME_MIN_LENGTH,
			        TextFieldLength.PROJECT_NAME_MAX_LENGTH,
			        false));

			validator.add(new TextBoxValidator(txtProblem,
			        TextFieldLength.PROJECT_PROBLEM_MIN_LENGTH,
			        TextFieldLength.PROJECT_PROBLEM_MAX_LENGTH,
			        false));
			
			validator.add(new TextBoxValidator(txtSolution,
			        TextFieldLength.PROJECT_SOLUTION_MIN_LENGTH,
			        TextFieldLength.PROJECT_SOLUTION_MAX_LENGTH,
			        false));
			
			Boolean errorFlag = !Utility.isValid(validator);
			
			if ((dtStartDate != null) && (dtEndDate != null)) {
				Date startDate = Utility.getDateFromSWTDateTimeWidget(dtStartDate);
				Date endDate = Utility.getDateFromSWTDateTimeWidget(dtEndDate);
				
				if (startDate.compareTo(endDate) > 0) {
					errorFlag = true;
				}
			}
			
			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (errorFlag) {
					okButton.setEnabled(false);
				}
				else {
					okButton.setEnabled(true);
				}
				
			}
		}
	};
	
	public ProjectDetailsValidator dialogValidator = new ProjectDetailsValidator();
	
	public ViewAddEditProject(Shell parent, Project data) {
		super(parent);
		
		project = data;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Project Details...");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtName = Utility.createFormFields(area,
				         "Project Name", 
				         "", 
				         TextFieldLength.PROJECT_NAME_MIN_LENGTH,
				         TextFieldLength.PROJECT_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		txtProblem = Utility.createFormFields(area,
				         "Describe the problem you are trying to solve", 
				         "", 
				         TextFieldLength.PROJECT_PROBLEM_MIN_LENGTH,
				         TextFieldLength.PROJECT_PROBLEM_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         true,
				         false,
				         false);
		
		txtSolution = Utility.createFormFields(area,
				         "Describe how your solution solves the problem", 
				         "", 
				         TextFieldLength.PROJECT_SOLUTION_MIN_LENGTH,
				         TextFieldLength.PROJECT_SOLUTION_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         true,
				         false,
				         false);
		
		
		Long pid = (long) 0;
		
		if (project != null) {
			txtName.setEnabled(false);
			txtName.setText(project.name);
			txtProblem.setText(project.problem);
			txtSolution.setText(project.solution);
			pid = project.id;
		}
		
		TextBoxKeyListener listener1 = new TextBoxKeyListener(pid,txtName, glossary);
		txtName.addListener(SWT.KeyUp,listener1);
		
		TextBoxKeyListener listener2 = new TextBoxKeyListener(pid,txtProblem, glossary);
		txtProblem.addListener(SWT.KeyUp,listener2);
		
		TextBoxKeyListener listener3 = new TextBoxKeyListener(pid,txtSolution, glossary);
		txtSolution.addListener(SWT.KeyUp,listener3);
		
		Label lblStartDate = new Label(area, SWT.NONE);
		lblStartDate.setText("Estimated start date of your project");
		
		dtStartDate = new DateTime(area, SWT.DATE);
		
		if (project != null) {
			
			Calendar calStartDate = new GregorianCalendar();
			calStartDate.setTime(project.startDate);
			
			dtStartDate.setDate(calStartDate.get(Calendar.YEAR),
								calStartDate.get(Calendar.MONTH),
								calStartDate.get(Calendar.DAY_OF_MONTH));
		}
		
		Label lblEndDate = new Label(area, SWT.NONE);
		lblEndDate.setText("Estimated end date of your project");
		
		dtEndDate = new DateTime(area, SWT.DATE);
		if (project != null) {
			
			Calendar calEndDate = new GregorianCalendar();
			calEndDate.setTime(project.endDate);
			
			dtEndDate.setDate(calEndDate.get(Calendar.YEAR),
							  calEndDate.get(Calendar.MONTH),
							  calEndDate.get(Calendar.DAY_OF_MONTH));
		}
		
		IObservableValue startDateObservable = WidgetProperties.selection().observe(dtStartDate);
		IObservableValue endDateObservable = WidgetProperties.selection().observe(dtEndDate);

		ControlDecorationSupport.create(new DateRangeValidator(
										startDateObservable, endDateObservable,
										"Start date must be on or before end date",
										dialogValidator),
										SWT.RIGHT | SWT.CENTER);

		return area;
	}
	
	@Override
	protected void okPressed() {
		String name = txtName.getText().trim();
		String problem = txtProblem.getText().trim();
		String solution = txtSolution.getText().trim();
		Date startDate = Utility.getDateFromSWTDateTimeWidget(dtStartDate);
		Date endDate = Utility.getDateFromSWTDateTimeWidget(dtEndDate);
		
		if (project == null) {
			DBHelper.addProjectAndGlossary(name, problem, solution, startDate, endDate, glossary);
		}
		else {
			ProjectModel.updateProject(project.id, name, problem, solution, startDate, endDate);
		}
		
	    super.okPressed();
	}
}
