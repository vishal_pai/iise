package com.wadia.iise.views;

//Synch with cloud URLs
public class URLConstants {
	public static String PROJECT = "/project";
	public static String TEAM = "/team";
	public static String FEASIBILITY = "/feasibility";
	public static String REQUIREMENT = "/requirement";
	public static String GLOSSARY = "/glossary";
	public static String MASTER_QUESTION = "/master";
	public static String DELETE_TRACKER = "/delete";
	public static String FUNCTIONAL_REQUIREMENT = "/functionalrequirement";
	public static String SOURCE_UNIT = "/sourceunit";
	public static String SOURCE_FIELD = "/sourcefield";
	public static String SOURCE_TOKEN = "/sourcetoken";
	public static String SOURCE_METHOD = "/sourcemethod";
	public static String STAKEHOLDER = "/stakeholder";
	
}
