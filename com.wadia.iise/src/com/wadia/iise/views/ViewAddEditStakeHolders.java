package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.StakeHolder;
import com.wadia.iise.dataobjects.StakeHolderType;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.StakeHolderModel;
import com.wadia.iise.db.StakeHolderTypeModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;

/*
 * Purpose: 
 * CRUD stakeholder...
 */

public class ViewAddEditStakeHolders extends TitleAreaDialog {

	private ComboViewer comboQuestion = null;
	private Text txtDetails = null;
	
	private StakeHolder stakeHolder = null;
	private Long project_id = (long) 0;
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	Button deleteButton = null;
	
	public class StakeHolderValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtDetails,
						  TextFieldLength.STAKEHOLDER_NAME_MIN_LENGTH,
						  TextFieldLength.STAKEHOLDER_NAME_MAX_LENGTH,
						  false));
			
			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
		}
	};
	
	public StakeHolderValidator dialogValidator = new StakeHolderValidator();
	
	public ViewAddEditStakeHolders(Shell parent, StakeHolder data, Long project_id) {
		super(parent);
		
		this.stakeHolder = data;
		this.project_id = project_id;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Stakeholders");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		Label lblquestion = new Label(area, SWT.NONE);
		lblquestion.setText("StakeHolder Type");
		
		ArrayList<StakeHolderType> stakeHolderTypes = StakeHolderTypeModel.getAllStakeHolderTypes();
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 2; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		
		comboQuestion = new ComboViewer(container1,SWT.READ_ONLY);
		comboQuestion.setContentProvider(ArrayContentProvider.getInstance());
		comboQuestion.setInput(stakeHolderTypes);
		
		Button buttonAddNewStakeHolderType = new Button(container1, SWT.NONE);
		buttonAddNewStakeHolderType.setText("Add New StakeHolder Type");
		buttonAddNewStakeHolderType.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				ViewAddEditStakeHolderType dialog = new ViewAddEditStakeHolderType(new Shell(), null);
				dialog.create();
				
				if (dialog.open() == Window.OK) {
					ArrayList<StakeHolderType> stakeHolderTypes = new ArrayList<StakeHolderType>();
					stakeHolderTypes = StakeHolderTypeModel.getAllStakeHolderTypes();
					comboQuestion.setInput(stakeHolderTypes);
					comboQuestion.setSelection(new StructuredSelection(stakeHolderTypes.get(0)));
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});
		
		comboQuestion.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof StakeHolderType) {
			    	StakeHolderType shType = (StakeHolderType) element;
			    	return shType.type;
			    }
			    return super.getText(element);
			}
		});
		
		comboQuestion.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				dialogValidator.enableOKButton();
			}
		});
		
		if (stakeHolder == null) {
			comboQuestion.setSelection(new StructuredSelection(stakeHolderTypes.get(0)));
		}
		else {
			for (StakeHolderType shType: stakeHolderTypes) {
				if (shType.type.equals(stakeHolder.type)) {
					comboQuestion.setSelection(new StructuredSelection(shType));
				}
			}
		}
		
		txtDetails = Utility.createFormFields(area,
				         "StakeHolder Name", 
				         "", 
				         TextFieldLength.STAKEHOLDER_NAME_MIN_LENGTH,
				         TextFieldLength.STAKEHOLDER_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		if (stakeHolder != null) {
			txtDetails.setText(stakeHolder.name);
		}
		
		return area;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent){ 
		super.createButtonsForButtonBar(parent);
		
		if (stakeHolder != null) {
	 		deleteButton = createButton(parent, 2,"Delete",true); 
			
			deleteButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					DBHelper.deleteEntity(DBConstants.STAKEHOLDER_TABLE, stakeHolder.id, stakeHolder.cloudid);
					close();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					
				}
			});
			
			Button okCancel = getButton(IDialogConstants.CANCEL_ID);
			if (okCancel != null) {
				okCancel.setFocus();
			}
		}
	}
	
	@Override
	protected void okPressed() {
	    
		IStructuredSelection selection = (IStructuredSelection) comboQuestion.getSelection();
		StakeHolderType stakeHolderType = (StakeHolderType) selection.getFirstElement();
		
	    String name = txtDetails.getText().trim();
	    		
	    		
	    if (stakeHolder == null) {
	    	StakeHolderModel.createStakeHolder(project_id, stakeHolderType.id, name);
	    }
	    else {
	    	StakeHolderModel.updateStakeHolder(stakeHolder.id, stakeHolderType.id, name);
	    	
	    }
	    
		super.okPressed();
	}
}

