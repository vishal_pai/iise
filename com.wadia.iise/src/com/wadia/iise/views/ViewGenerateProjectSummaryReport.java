package com.wadia.iise.views;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Feasibility;
import com.wadia.iise.dataobjects.FunctionalRequirement;
import com.wadia.iise.dataobjects.Glossary;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.Requirement;
import com.wadia.iise.db.FeasibilityModel;
import com.wadia.iise.db.GlossaryModel;
import com.wadia.iise.db.FunctionalRequirementModel;
import com.wadia.iise.db.RequirementModel;

/*
 * Purpose: 
 * Generate reports
 */

public class ViewGenerateProjectSummaryReport extends TitleAreaDialog{
	private ComboViewer comboProject = null;
	private ArrayList<Project> projects = null;
	private String selectedFile = "";
	Label lblPath = null;
	
	private String htmlTemplate = "<!DOCTYPE HTML>"
			+ "<html>"
			+ "<head>"
			+ "<title>$PROJECT_TITLE</title>"
			+ "<style>"
			+ "pre {"
			+ "font-family: None"
			+ "}"
			+ "</style>"
			+ "</head>"
			+ "<body>"
			+ "<div align='center'><h1>$PROJECT_TITLE Summary Report</h1></div>"
			+ "<div><h1>1. Existing Problem</h1>"
			+ "$EXISTING_PROBLEM"
			+ "</div>"
			+ "<div><h1>2. Proposed Solution</h1>"
			+ "$PROPOSED_SOLUTION"
			+ "</div>"
			+ "<div><h1>3. Feasibility Study</h1></div>"
			+ "<div><h2>3.1. Economic</h2>"
			+ "$ECONOMIC"
			+ "</div>"
			+ "<div><h2>3.2. Operational</h2>"
			+ "$OPERATIONAL"
			+ "</div>"
			+ "<div><h2>3.3. Technical</h2>"
			+ "$TECHNICAL"
			+ "</div>"
			+ "<div><h1>4. Requirements</h1></div>"
			+ "<div><h2>4.1. Functional Requirements</h2>"
			+ "$FUNCTIONAL"
			+ "</div>"
			+ "<div><h2>4.2. Non Funcational Requirements</h2>"
			+ "$NON_FUNCTIONAL"
			+ "</div>"
			+ "<div><h2>4.3. Hardware Requirements</h2>"
			+ "$HARDWARE"
			+ "</div>"
			+ "<div><h2>4.4. Software Requirements</h2>"
			+ "$SOFTWARE"
			+ "</div>"
			+ "<div><h2>4.5. Development Requirements</h2>"
			+ "$DEVELOPMENT"
			+ "</div>"
			+ "<div><h1>5. Glossary</h1>"
			+ "$GLOSSARY"
			+ "</div>"
			+ "</body>"
			+ "</html>";
	
	
	public ViewGenerateProjectSummaryReport(Shell parentShell, ArrayList<Project> projects) {
		super(parentShell);
		this.projects = projects;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Generate Project Summary Report");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 2; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		Label lblProject = new Label(container1, SWT.NONE);
		lblProject.setText("Project");
		
		comboProject = new ComboViewer(container1,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name;
			    }
			    return super.getText(element);
			}
		});
		
		comboProject.setSelection(new StructuredSelection(projects.get(0)));
		
		Button btnNewButton = new Button(area, SWT.NONE);
		btnNewButton.setText("Browse");
		btnNewButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dlg = new FileDialog(getShell(),  SWT.OPEN  );
				dlg.setText("Open");
				String path = dlg.open();
				if (path == null) return;
				selectedFile = path;
				lblPath.setText(path);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		lblPath = new Label(area, SWT.NONE);
		lblPath.setText("File Path:");
		lblPath.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		return area;
	}
	
	@Override
	protected void okPressed() {
	    if (selectedFile.length() <= 0)
	    	return;
	    
	    try {
			FileWriter w = new FileWriter(selectedFile);
			
			IStructuredSelection selection = (IStructuredSelection) comboProject.getSelection();
			Project project = (Project) selection.getFirstElement();
			
			ArrayList<Feasibility> feasibility_list = FeasibilityModel.getAllFeasibility(project.id, DBConstants.ECONOMIC_FEASIBILITY);
			String economic = "";
			for (Feasibility f: feasibility_list) {
				economic += f.description;
				economic += "<br/>";
				economic += "<br/>";
			}
			
			feasibility_list = FeasibilityModel.getAllFeasibility(project.id, DBConstants.OPERATIONAL_FEASIBILITY);
			String operational = "";
			for (Feasibility f: feasibility_list) {
				operational += f.description;
				operational += "<br/>";
				operational += "<br/>";
			}
			
			feasibility_list = FeasibilityModel.getAllFeasibility(project.id, DBConstants.TECHNICAL_FEASIBILITY);
			String technical = "";
			for (Feasibility f: feasibility_list) {
				technical += f.description;
				technical += "<br/>";
				technical += "<br/>";
			}
			
			
			ArrayList<FunctionalRequirement> functionalrequirements = FunctionalRequirementModel.getAllProjectRequirements(project.id);
			String functionalRequirement = "";
			for (FunctionalRequirement r: functionalrequirements) {
			functionalRequirement += r.description;
				functionalRequirement += "<br/>";
				functionalRequirement += "<br/>";
			}
			
			ArrayList<Requirement> requirements = RequirementModel.getAllRequirements(project.id, DBConstants.NON_FUNCTIONAL_REQUIREMENTS);
			String nonfunctionalrequirements = "";
			for (Requirement r: requirements) {
				nonfunctionalrequirements += r.details;
				nonfunctionalrequirements += "<br/>";
				nonfunctionalrequirements += "<br/>";
			}
			
			requirements = RequirementModel.getAllRequirements(project.id, DBConstants.HARDWARE_REQUIREMENTS);
			String hardwarerequirements = "";
			for (Requirement r: requirements) {
				hardwarerequirements += r.details;
				hardwarerequirements += "<br/>";
				hardwarerequirements += "<br/>";
			}
			
			requirements = RequirementModel.getAllRequirements(project.id, DBConstants.SOFTWARE_REQUIREMENTS);
			String softwarerequirements = "";
			for (Requirement r: requirements) {
				softwarerequirements += r.details;
				softwarerequirements += "<br/>";
				softwarerequirements += "<br/>";
			}
			
			requirements = RequirementModel.getAllRequirements(project.id, DBConstants.DEVELOPER_REQUIREMENTS);
			String developmentrequirements = "";
			for (Requirement r: requirements) {
				developmentrequirements += r.details;
				developmentrequirements += "<br/>";
				developmentrequirements += "<br/>";
			}
			
			ArrayList<Glossary> glossarylist = GlossaryModel.getAllGlossary(project.id);
			String glossary = "";
			Long count = (long) 1;
			for (Glossary g: glossarylist) {
				glossary += count.toString() + ". ";
				glossary += g.name;
				glossary += "<br/>";
				glossary += g.description;
				glossary += "<br/>";
				glossary += "<br/>";
				count++;
			}
			
			htmlTemplate = htmlTemplate.replace("$PROJECT_TITLE", project.name);
			htmlTemplate = htmlTemplate.replace("$EXISTING_PROBLEM", "<pre>" + project.problem + "</pre>");
			htmlTemplate = htmlTemplate.replace("$PROPOSED_SOLUTION", "<pre>" + project.solution + "</pre>");
			htmlTemplate = htmlTemplate.replace("$ECONOMIC", "<pre>" + economic + "</pre>");
			htmlTemplate = htmlTemplate.replace("$OPERATIONAL", "<pre>" + operational + "</pre>");
			htmlTemplate = htmlTemplate.replace("$TECHNICAL", technical + "</pre>");
			htmlTemplate = htmlTemplate.replace("$FUNCTIONAL","<pre>" +  functionalRequirement + "</pre>");
			htmlTemplate = htmlTemplate.replace("$NON_FUNCTIONAL", "<pre>" + nonfunctionalrequirements + "</pre>");
			htmlTemplate = htmlTemplate.replace("$HARDWARE", "<pre>" + hardwarerequirements + "</pre>");
			htmlTemplate = htmlTemplate.replace("$SOFTWARE", "<pre>" + softwarerequirements + "</pre>");
			htmlTemplate = htmlTemplate.replace("$DEVELOPMENT", "<pre>" + developmentrequirements + "</pre>");
			htmlTemplate = htmlTemplate.replace("$GLOSSARY", "<pre>" + glossary + "</pre>");
			w.write(htmlTemplate);
			
			w.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		super.okPressed();
	}
}
