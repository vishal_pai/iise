package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.StakeHolderType;
import com.wadia.iise.db.StakeHolderTypeModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;


/*
 * Purpose: 
 * CRUD master question
 */
public class ViewAddEditStakeHolderType extends TitleAreaDialog {

	private Text txtType = null;
	
	private StakeHolderType stakeHolderType = null;
	
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	public class StakeHolderTypeValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtType,
			        TextFieldLength.STAKEHOLDER_TYPE_MIN_LENGTH,
			        TextFieldLength.STAKEHOLDER_TYPE_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
				
		}
	};
	
	public StakeHolderTypeValidator dialogValidator = new StakeHolderTypeValidator();
	
	public ViewAddEditStakeHolderType(Shell parent, 
									  StakeHolderType data) {
		super(parent);
		
		this.stakeHolderType = data;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("StakeHolder Type...");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtType = Utility.createFormFields(area,
				         "StakeHolder Type", 
				         "", 
				         TextFieldLength.STAKEHOLDER_TYPE_MIN_LENGTH,
				         TextFieldLength.STAKEHOLDER_TYPE_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		if (stakeHolderType != null) {
			txtType.setText(stakeHolderType.type);
		}
		
		return area;
	}
	
	@Override
	protected void okPressed() {
	    String type = txtType.getText().trim();
	    
	    if (stakeHolderType == null) {
	    	StakeHolderTypeModel.createStakeHolderType(type);
	    }
	    
	    
		super.okPressed();
	}
}

