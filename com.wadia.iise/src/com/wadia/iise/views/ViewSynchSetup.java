package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.SynchSetup;
import com.wadia.iise.db.SynchSetupModel;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;

/*
 * Purpose: 
 * Handles synch to cloud...
 */

public class ViewSynchSetup  extends TitleAreaDialog {

	private Text txtURL = null;
	private String synchURL = "";
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	
	private StringValidationToolkit strValToolkit = null;
	public ViewSynchSetupValidator dialogValidator = new ViewSynchSetupValidator();
	
	public class ViewSynchSetupValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtURL,
			        TextFieldLength.URL_MIN_LENGTH,
			        TextFieldLength.URL_MAX_LENGTH,
			        false));

			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
		}
	};
	
	public ViewSynchSetup(Shell parentShell, SynchSetup setup) {
		super(parentShell);
		
		if (setup != null) {
			synchURL = setup.url;
		}
		else {
			synchURL = "";
		}
	}

	@Override
	public void create() {
	    super.create();
	    setTitle("Setup URL for Synchronization");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		txtURL = Utility.createFormFields(area,
				         "Server / Cloud URL", 
				         synchURL, 
				         TextFieldLength.USER_NAME_MIN_LENGTH,
				         TextFieldLength.USER_NAME_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         false,
				         false,
				         false);
		
		return area;
	}
	
	@Override
	protected void okPressed() {
		//Save base URL into the database...
		String url = txtURL.getText().trim();
		
		SynchSetup setup = SynchSetupModel.getSynchSetup();
		if (setup == null) {
			SynchSetupModel.createProject(url);
		}
		else {
			SynchSetupModel.updateSynchSetup(setup.id, url);
		}
		
		super.okPressed();
	}
}
