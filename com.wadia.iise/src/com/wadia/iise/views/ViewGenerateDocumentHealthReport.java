package com.wadia.iise.views;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxisSet;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries.SeriesType;

import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Feasibility;
import com.wadia.iise.dataobjects.FunctionalRequirement;
import com.wadia.iise.dataobjects.Glossary;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.Requirement;
import com.wadia.iise.db.FeasibilityModel;
import com.wadia.iise.db.FunctionalRequirementModel;
import com.wadia.iise.db.GlossaryModel;
import com.wadia.iise.db.RequirementModel;


/**
 * 
 * @author ggrec
 *
 */
public class ViewGenerateDocumentHealthReport extends Dialog
{
	private int DESCRIPTION_LENGTH = 100;
	private int ECONOMIC_FEASIBILITY_NUMBER = 5;
	private int OPERATIONAL_FEASIBILITY_NUMBER = 5;
	private int TECHNICAL_FEASIBILITY_NUMBER = 5;
	private int FUNCTIONAL_REQUIREMENT_NUMBER = 10;
	private int NON_FUNCTIONAL_REQUIREMENT_NUMBER = 5;
	private int HARDWARE_REQUIREMENT_NUMBER = 10;
	private int SOFTWARE_REQUIREMENT_NUMBER = 10;
	private int DEVELOPMENT_REQUIREMENT_NUMBER = 10;
	private int GLOSSARY_NUMBER = 10;
	private int GLOSSARY_DESCRIPTION = 40;
	
	private Composite container;
	private Chart chart = null;
    private ComboViewer comboProject = null;
	private ArrayList<Project> projects = null;
	
	public ViewGenerateDocumentHealthReport(Shell parentShell, ArrayList<Project> projects) {
		super(parentShell);
		this.projects = projects;
	}
	
	
	private void drawChart(Project project) {
		ArrayList<FunctionalRequirement> functionalrequirements = FunctionalRequirementModel.getAllProjectRequirements(project.id);
		int functionalRequirementsCount = 0;
		for (FunctionalRequirement r: functionalrequirements) {
			if (r.description.length() > DESCRIPTION_LENGTH) {
				functionalRequirementsCount++;	
			}
		}
		
		ArrayList<Requirement> requirements = RequirementModel.getAllRequirements(project.id, DBConstants.NON_FUNCTIONAL_REQUIREMENTS);
		int nonFunctionalRequirementsCount = 0;
		for (Requirement r: requirements) {
			if (r.details.length() > DESCRIPTION_LENGTH) {
				nonFunctionalRequirementsCount++;
			}
		}
		
		requirements = RequirementModel.getAllRequirements(project.id, DBConstants.HARDWARE_REQUIREMENTS);
		int hardwareRequirementsCount = 0;
		for (Requirement r: requirements) {
			if (r.details.length() > DESCRIPTION_LENGTH) {
				hardwareRequirementsCount++;
			}
		}
		
		requirements = RequirementModel.getAllRequirements(project.id, DBConstants.SOFTWARE_REQUIREMENTS);
		int softwareRequirementsCount = 0;
		for (Requirement r: requirements) {
			if (r.details.length() > DESCRIPTION_LENGTH) {
				softwareRequirementsCount++;
			}
		}
		
		requirements = RequirementModel.getAllRequirements(project.id, DBConstants.DEVELOPER_REQUIREMENTS);
		int developerRequirementsCount = 0;
		for (Requirement r: requirements) {
			if (r.details.length() > DESCRIPTION_LENGTH) {
				developerRequirementsCount++;
			}
		}
		
		ArrayList<Glossary> glossarylist = GlossaryModel.getAllGlossary(project.id);
		int glossaryCount = 0;
		
		for (Glossary g: glossarylist) {
			if (g.description.length() > GLOSSARY_DESCRIPTION) {
				glossaryCount++;
			}
		}
			
		ArrayList<Feasibility> economicFeasibility = FeasibilityModel.getAllFeasibility(project.id, DBConstants.ECONOMIC_FEASIBILITY);
		int economicFeasibilityCount = 0;
		for (Feasibility f: economicFeasibility) {
			if (f.description.length() > DESCRIPTION_LENGTH) {
				economicFeasibilityCount++;
			}
		}
		
		ArrayList<Feasibility> operationalFeasibility = FeasibilityModel.getAllFeasibility(project.id, DBConstants.OPERATIONAL_FEASIBILITY);
		int operationalFeasibilityCount = 0;
		for (Feasibility f: operationalFeasibility) {
			if (f.description.length() > DESCRIPTION_LENGTH) {
				operationalFeasibilityCount++;
			}
		}
		
		ArrayList<Feasibility> technicalFeasibility = FeasibilityModel.getAllFeasibility(project.id, DBConstants.TECHNICAL_FEASIBILITY);
		int technicalFeasibilityCount = 0;
		for (Feasibility f: technicalFeasibility) {
			if (f.description.length() > DESCRIPTION_LENGTH) {
				technicalFeasibilityCount++;
			}
		}
		
		int existingProblemProvidedCount = 0;
		if (project.problem.length() >= DESCRIPTION_LENGTH) {
			existingProblemProvidedCount = 1;
		}
		
		int proposedSolutionProvidedCount = 0;
		if (project.solution.length() >= DESCRIPTION_LENGTH) {
			proposedSolutionProvidedCount = 1;
		}
		
		String [] labels = {"Problem",
				            "Proposed \n Solution",
							"Functional \n Requirements",
							"Non Functional \n Requirements",
							"Hardware \n Requirements",
							"Software \n Requirements",
							"Development \n Requirements",
							"Economic \n Feasibility",
							"Operational \n Feasibility",
							"Technical \n Feasibility",
							"Glossary"};
		
		double [] expectedValues = {1,
									1,
									FUNCTIONAL_REQUIREMENT_NUMBER,
									NON_FUNCTIONAL_REQUIREMENT_NUMBER,
									HARDWARE_REQUIREMENT_NUMBER,
									SOFTWARE_REQUIREMENT_NUMBER,
									DEVELOPMENT_REQUIREMENT_NUMBER,
									ECONOMIC_FEASIBILITY_NUMBER,
									OPERATIONAL_FEASIBILITY_NUMBER,
									TECHNICAL_FEASIBILITY_NUMBER,
									GLOSSARY_NUMBER};
		
		double [] actualValues = {existingProblemProvidedCount,
								  proposedSolutionProvidedCount,
								  functionalRequirementsCount,
							      nonFunctionalRequirementsCount,
							      hardwareRequirementsCount,
							      softwareRequirementsCount,
							      developerRequirementsCount,
							      economicFeasibilityCount,
							      operationalFeasibilityCount,
							      technicalFeasibilityCount,
							      glossaryCount};
		
        Chart chart1 = createChart(container,
        						   "Documentation Health Report",
        						   "\n\nExpected v/s Actual",
        						   "Value",
        						   labels,
        						   expectedValues,
        						   actualValues);
        
        chart1.setLayoutData(new GridData(1200, 500));
        chart1.redraw();
	}
	
    @Override
    protected Composite createDialogArea(final Composite parent)
    {
        final Composite dialogArea = (Composite) super.createDialogArea(parent);
        
        container = new Composite(dialogArea, SWT.NULL);
        container.setLayout(new GridLayout(1, true));
        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Label lblProject = new Label(container, SWT.NONE);
		lblProject.setText("Project");
		
		comboProject = new ComboViewer(container,SWT.READ_ONLY);
		comboProject.setContentProvider(ArrayContentProvider.getInstance());
		comboProject.setInput(projects);
		
		comboProject.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof Project) {
			    	Project project = (Project) element;
			    	return project.name;
			    }
			    return super.getText(element);
			}
		});
		
		comboProject.setSelection(new StructuredSelection(projects.get(0)));
	    
		comboProject.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				Project project = (Project) selection.getFirstElement();
				drawChart(project);
			}
		});
		
		Project project = projects.get(0);
		
		
		drawChart(project);
          
        return dialogArea;
    } 
    
    @Override
    protected Point getInitialSize() {
      return new Point(1000, 1000);
    }

    @Override
	public boolean isResizable() {
		return true;
	}


    private Chart createChart(final Composite parent,
    								 String title,
    								 String xAxisLabel,
    								 String yAxisLabel,
    								 String [] labels,
    								 double [] expectedValues,
    								 double [] actualValues)
    {
    	
    	if (chart == null) {
    		chart = new Chart(parent, SWT.NONE);
    	}
    	
    	chart.getTitle().setText(title);
    	chart.getAxisSet().getXAxis(0).getTitle().setText(xAxisLabel);
    	chart.getAxisSet().getYAxis(0).getTitle().setText(yAxisLabel);
    	IAxisSet axisSet = chart.getAxisSet();
    	IAxis xAxis = axisSet.getXAxis(0);
    	xAxis.setCategorySeries(labels);
    	xAxis.enableCategory(true);

    	IBarSeries series1 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "Expected");
    	series1.setBarColor(parent.getDisplay().getSystemColor(SWT.COLOR_GREEN));
    	double[] values1 = expectedValues;
    	series1.setYSeries(values1);
    	
    	IBarSeries series2 = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "Actual");
    	series2.setBarColor(parent.getDisplay().getSystemColor(SWT.COLOR_BLUE));
    	double[] values2 = actualValues;
    	series2.setYSeries(values2);
    	
    	chart.getAxisSet().adjustRange();
    	
    	return chart;

    }
}
