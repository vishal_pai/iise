package com.wadia.iise.views;

import java.sql.Timestamp;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.SourceUnitModel;
import com.wadia.iise.helper.TableHelper;


/*
 * Purpose: 
 * View analyzed code...
 */
public class ViewAnalyzedCode extends TitleAreaDialog{

	private TableViewer viewer = null;
	
	private Long projectId;
	private Timestamp t;
	
	public ViewAnalyzedCode(Shell parentShell, Long projectId, Timestamp t) {
		super(parentShell);
		this.projectId = projectId;
		this.t = t;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Analyzed Code");
	}
	
	@Override
	public boolean isResizable() {
		return true;
	}
	
	
	@Override
	protected Control createDialogArea(Composite parent) {
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 3; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		Label lblProject = new Label(container1, SWT.NONE);
		lblProject.setText("Project");
		
		viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | 
									   SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		
		createColumns(viewer);
		
		final Table table = viewer.getTable();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.heightHint = 200;
		table.setLayoutData(gridData);
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		
		viewer.setInput(SourceUnitModel.getAllSourceUnitsByTimeStamp(projectId, t));
		
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				TableItem[] items = viewer.getTable().getSelection();
				
				if (items.length == -1) {
					return;
				}
				
				TableItem item = items[0];
				SourceUnit sourceUnit = (SourceUnit) item.getData();
				
				ViewAnalyzedMethods dialog = new ViewAnalyzedMethods(new Shell(), 
																	 sourceUnit.id);
				dialog.create();
				dialog.open();
			}
		});
		
		return area;
	}

	private void createColumns(TableViewer viewer2) {
		// TODO Auto-generated method stub
		String[] titles = { "Analyzed Date", "Name", "# Lines", "# Methods", 
						    "# Fields", "# Tokens", "# Comments", "# Blanks", 
						    "As Caller", "As Callee"};
		
	    int[] bounds = { 150, 150, 75, 75, 75, 75, 75, 75, 75, 75};
	    
	    TableViewerColumn col = TableHelper.createTableViewerColumn(titles[0], bounds[0], 0, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        SourceUnit s = (SourceUnit) element;
	        return s.dateTime.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[1], bounds[1], 1, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        SourceUnit s = (SourceUnit) element;
	        return s.path;
	      }
	    });

	    col = TableHelper.createTableViewerColumn(titles[2], bounds[2], 2, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numLines.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[3], bounds[3], 3, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numMethods.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[4], bounds[4], 4, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numFields.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[5], bounds[5], 5, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numTokens.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[6], bounds[6], 6, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numComments.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[7], bounds[7], 7, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.numBlanks.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[8], bounds[8], 8, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.callerCount.toString();
	      }
	    });
	    
	    col = TableHelper.createTableViewerColumn(titles[9], bounds[9], 9, viewer2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  SourceUnit s = (SourceUnit) element;
		      return s.calleeCount.toString();
	      }
	    });
	}
}
