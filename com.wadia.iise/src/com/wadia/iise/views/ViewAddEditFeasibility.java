package com.wadia.iise.views;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Feasibility;
import com.wadia.iise.dataobjects.MasterQuestion;
import com.wadia.iise.db.DBHelper;
import com.wadia.iise.db.FeasibilityModel;
import com.wadia.iise.db.MasterQuestionModel;
import com.wadia.iise.helper.TextBoxKeyListener;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.validations.TextFieldLength;

/*
 * Purpose: 
 * CRUD feasibility
 */

public class ViewAddEditFeasibility extends TitleAreaDialog {

	private ComboViewer comboQuestion = null;
	private Text txtDetails = null;
	
	private Feasibility feasibility = null;
	private Long project_id = (long) 0;
	private ArrayList<String> glossary = new ArrayList<String>();
	private String type = "";
	
	//Validation related stuff...
	private static final int DECORATOR_POSITION = SWT.TOP | SWT.RIGHT;
	private static final int DECORATOR_MARGIN_WIDTH = 1;
	private StringValidationToolkit strValToolkit = null;
	
	Button deleteButton = null;
	
	public class FeasibilityValidator implements IEnableOKButton {
		
		public void enableOKButton() {
			
			ArrayList<TextBoxValidator> validator = new ArrayList<TextBoxValidator>();
			
			validator.add(new TextBoxValidator(txtDetails,
					TextFieldLength.REQUIREMENT_DETAILS_MIN_LENGTH,
					TextFieldLength.REQUIREMENT_DETAILS_MAX_LENGTH,
					false));
			
			Button okButton = getButton(IDialogConstants.OK_ID);
			if (okButton != null) {
				if (Utility.isValid(validator)) {
					okButton.setEnabled(true);
				}
				else {
					okButton.setEnabled(false);
				}
				
			}
		}
	};
	
	public FeasibilityValidator dialogValidator = new FeasibilityValidator();
	
	public ViewAddEditFeasibility(Shell parent, Feasibility data, Long project_id, String type) {
		super(parent);
		
		this.feasibility = data;
		this.project_id = project_id;
		this.type = type;
	}
	
	@Override
	public void create() {
	    super.create();
	    setTitle("Feasibility");
	    
	    Button okButton = getButton(IDialogConstants.OK_ID);
	    okButton.setEnabled(false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		strValToolkit = new StringValidationToolkit(DECORATOR_POSITION,
			    									DECORATOR_MARGIN_WIDTH, 
			    									true);
		
		Composite area = (Composite) super.createDialogArea(parent);
		
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.marginLeft = 20;
		gridLayout.marginRight = 20;
		area.setLayout(gridLayout);
		
		Label lblquestion = new Label(area, SWT.NONE);
		lblquestion.setText("Question");
		
		ArrayList<MasterQuestion> masterQuestions = new ArrayList<MasterQuestion>();
		masterQuestions = MasterQuestionModel.getAllMasterQuestions(type);
		
		Composite container1 = new Composite(area, SWT.NONE); 
		GridLayout gridLayout1 = new GridLayout(); 
		gridLayout1.numColumns = 2; 
		container1.setLayout(gridLayout1); 
		container1.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false)); 
		
		comboQuestion = new ComboViewer(container1,SWT.READ_ONLY);
		comboQuestion.setContentProvider(ArrayContentProvider.getInstance());
		comboQuestion.setInput(masterQuestions);
		
		Button buttonAddNewQuestion = new Button(container1, SWT.NONE);
		buttonAddNewQuestion.setText("Add New Question");
		buttonAddNewQuestion.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection selection = (IStructuredSelection) comboQuestion.getSelection();
				MasterQuestion masterQuestion = (MasterQuestion) selection.getFirstElement();
				ViewAddEditMasterQuestion dialog = new ViewAddEditMasterQuestion(new Shell(), null, masterQuestion.type);
				dialog.create();
				
				if (dialog.open() == Window.OK) {
					ArrayList<MasterQuestion> masterQuestions = new ArrayList<MasterQuestion>();
					masterQuestions = MasterQuestionModel.getAllMasterQuestions(type);
					comboQuestion.setInput(masterQuestions);
					comboQuestion.setSelection(new StructuredSelection(masterQuestions.get(0)));
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		comboQuestion.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
			    if (element instanceof MasterQuestion) {
			    	MasterQuestion h = (MasterQuestion) element;
			    	return h.question;
			    }
			    return super.getText(element);
			}
		});
		
		if (feasibility == null) {
			comboQuestion.setSelection(new StructuredSelection(masterQuestions.get(0)));
		}
		else {
			for (MasterQuestion q: masterQuestions) {
				if (q.question.equals(feasibility.question)) {
					comboQuestion.setSelection(new StructuredSelection(q));
				}
			}
		}
		
		txtDetails = Utility.createFormFields(area,
				         "Answer", 
				         "", 
				         TextFieldLength.REQUIREMENT_DETAILS_MIN_LENGTH,
				         TextFieldLength.REQUIREMENT_DETAILS_MAX_LENGTH,
				         dialogValidator,
				         strValToolkit,
				         true,
				         false,
				         false);
		
		if (feasibility != null) {
			txtDetails.setText(feasibility.description);
		}
		
		TextBoxKeyListener listener1 = new TextBoxKeyListener(project_id,txtDetails, glossary);
		txtDetails.addListener(SWT.KeyUp,listener1);
		
		return area;
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent){ 
		super.createButtonsForButtonBar(parent);
		
		if (feasibility != null) {
	 		deleteButton = createButton(parent, 2,"Delete",true); 
			
			deleteButton.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent e) {
					DBHelper.deleteEntity(DBConstants.FEASIBILITY_TABLE, feasibility.id, feasibility.cloudid);
					close();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					
				}
			});
			
			Button okCancel = getButton(IDialogConstants.CANCEL_ID);
			if (okCancel != null) {
				okCancel.setFocus();
			}
		}
	}
	
	@Override
	protected void okPressed() {
	    
		IStructuredSelection selection = (IStructuredSelection) comboQuestion.getSelection();
		MasterQuestion masterQuestion = (MasterQuestion) selection.getFirstElement();
		
	    String details = txtDetails.getText().trim();
	    		
	    		
	    if (feasibility == null) {
	    	FeasibilityModel.createFeasibility(project_id,masterQuestion.id, details, this.type);
	    }
	    else {
	    	FeasibilityModel.updateFeasibility(feasibility.id, masterQuestion.id, details);
	    }
	    
		super.okPressed();
	}
}

