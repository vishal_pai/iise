package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.DriverManager;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Represents connection to the database...
 */

public class DBConnection {
	private static String connectionURL = "jdbc:derby:sedb;create=true"; 
	private static Connection conn = null;
	private static int VALIDITY_TIME_OUT = 5;
	
	public static Connection getConnection() {
		try {
			
			if ((conn != null) && (conn.isClosed() == false) && (conn.isValid(VALIDITY_TIME_OUT))) {
				return conn;
			}
			
			if (conn != null) {
				try {
					conn.close();
				}
				catch(Exception e) {
					e.printStackTrace();
					Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
				}
			}
			
			DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
			conn = DriverManager.getConnection(connectionURL); 
			return conn;
		}
		catch (Exception e) {
			conn = null;
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return null;
	}
	
	public static void close() {
		try {
			if (conn != null) {
				conn.close();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		conn = null;
	}
}
