package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.StakeHolder;
import com.wadia.iise.dataobjects.StakeHolderType;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Interface to do CRUD on Stakeholder related information in db.
 */

public class StakeHolderModel  extends BaseModel {
	
	private static StakeHolder getStakeHolder(ResultSet rs) {
		StakeHolder stakeHolder = new StakeHolder();
		
		try {
			stakeHolder.id = rs.getLong("ID");
			stakeHolder.name = rs.getString("Name");
			stakeHolder.project_id = rs.getLong("PROJECT_ID");
			stakeHolder.type_id = rs.getLong("TYPE_ID");
			stakeHolder.cloudid = rs.getString("CLOUD_ID");
			if (stakeHolder.cloudid == null) {
				stakeHolder.cloudid = "";
			}
			
			stakeHolder.synched = rs.getLong("Synched");
			
			Project project = ProjectModel.getProjectById(stakeHolder.project_id);
			stakeHolder.project_cloudid = project.cloudid;
			
			StakeHolderType stakeHolderType = StakeHolderTypeModel.getStakeHolderTypeById(stakeHolder.type_id);
			stakeHolder.type = stakeHolderType.type;
			
			return stakeHolder;
		}
		catch(SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		catch(Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return null;
	}
	
	public static Boolean createStakeHolder(Long project_id,
											Long type_id,
											String name) {

		String insertQuery = "Insert into STAKEHOLDER (PROJECT_ID,TYPE_ID," + 
							 "Name) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setLong(1, project_id);
			prepStatement.setLong(2, type_id);
			prepStatement.setString(3, name);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<StakeHolder> getAllStakeHolders(Long projectID) {

		ArrayList<StakeHolder> stakeHolders = new ArrayList<StakeHolder>();
		
		try {
			String selectQuery = "select * from STAKEHOLDER where PROJECT_ID=?";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			prepStatement.setLong(1, projectID);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				StakeHolder stakeHolder = getStakeHolder(rs);
				
				stakeHolders.add(stakeHolder);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return stakeHolders;
	}
	
	public static ArrayList<StakeHolder> getAllNotSynchedStakeHolders() {

		ArrayList<StakeHolder> stakeHolders = new ArrayList<StakeHolder>();
		
		try {
			String selectQuery = "select * from STAKEHOLDER where Synched is NULL";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				StakeHolder feasibility = getStakeHolder(rs);
				
				stakeHolders.add(feasibility);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return stakeHolders;
	
	}

	public static void updateStakeHolder(Long id, 
									     Long type_id,
									     String name) {

		String strUpdateQuery = "update STAKEHOLDER set Name=?,TYPE_ID=?,Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, name);
			prepStatement.setLong(2, type_id);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			e.printStackTrace();
		}
	}
}
