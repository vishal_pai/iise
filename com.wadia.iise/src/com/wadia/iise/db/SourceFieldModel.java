package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.SourceField;
import com.wadia.iise.dataobjects.SourceUnit;

/*
 * Purpose: 
 * Interface to do CRUD on member variable related information in db.
 */

public class SourceFieldModel  extends BaseModel {
	
	private static SourceField getSourceField(ResultSet rs) {
	    try {
	    	SourceField sourceField = new SourceField();
	    	sourceField.dateTime = rs.getTimestamp("AnalysisDate");
			sourceField.name = rs.getString("Name");
			sourceField.unit_id = rs.getLong("UNIT_ID");
			sourceField.id = rs.getLong("ID");
			sourceField.cloudid = rs.getString("CLOUD_ID");
			if (sourceField.cloudid == null) {
				sourceField.cloudid = "";
			}
			sourceField.synched = rs.getLong("Synched");
			
			SourceUnit sourceUnit = SourceUnitModel.getSourceUnitById(sourceField.unit_id);
			
			sourceField.unit_cloudid = sourceUnit.cloudid;
			
			return sourceField;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Long createSourceField(Long unit_id,
										 Timestamp dateTime,
										 String name) {

		String insertQuery = "Insert into SOURCE_FIELD(AnalysisDate, Name, " + 
							 "UNIT_ID) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, unit_id);
			
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long)0;
	}

	public static ArrayList<SourceField> getAllSourceField(Long unitID) {

		ArrayList<SourceField> sourceFields = new ArrayList<SourceField>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_FIELD where UNIT_ID=?");
			prepStatement.setLong(1, unitID);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceField sourceField = getSourceField(rs);
				sourceFields.add(sourceField);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceFields;
	}

	public static ArrayList<SourceField> getAllNotSynchedSourceField() {

		ArrayList<SourceField> sourceFields = new ArrayList<SourceField>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_FIELD where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceField sourceField = getSourceField(rs);
				sourceFields.add(sourceField);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceFields;
	}

	
	public static void updateSourceField(Long id, 
										 Timestamp dateTime,
										 String name) {

		String strUpdateQuery = "update SOURCE_FIELD set AnalysisDate=?,name=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}