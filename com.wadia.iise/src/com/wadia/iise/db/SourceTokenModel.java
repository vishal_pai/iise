package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.SourceMethod;
import com.wadia.iise.dataobjects.SourceToken;

/*
 * Purpose: 
 * Interface to do CRUD on local variable related information in db.
 */

public class SourceTokenModel  extends BaseModel {
	private static SourceToken getSourceToken(ResultSet rs) {
	    try {
	    	SourceToken sourceToken = new SourceToken();
	    	sourceToken.dateTime = rs.getTimestamp("AnalysisDate");
			sourceToken.name = rs.getString("Name");
			sourceToken.method_id = rs.getLong("METHOD_ID");
			sourceToken.id = rs.getLong("ID");
			sourceToken.cloudid = rs.getString("CLOUD_ID");
			if (sourceToken.cloudid == null) {
				sourceToken.cloudid = "";
			}
			sourceToken.synched = rs.getLong("Synched");
			
			SourceMethod sourceMethod = SourceMethodModel.getSourceMethodById(sourceToken.method_id);
			sourceToken.method_cloudid = sourceMethod.cloudid;
			
			return sourceToken;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Long createSourceToken(Long method_id,
										 Timestamp dateTime,
										 String name) {

		String insertQuery = "Insert into SOURCE_TOKEN(AnalysisDate, Name, " + 
							 "METHOD_ID) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, method_id);
			
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long)0;
	}

	public static ArrayList<SourceToken> getAllSourceToken(Long methodID) {

		ArrayList<SourceToken> sourceTokens = new ArrayList<SourceToken>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_TOKEN where METHOD_ID=?");
			prepStatement.setLong(1, methodID);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceToken sourceToken = getSourceToken(rs);
				
				sourceTokens.add(sourceToken);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceTokens;
	}

	public static ArrayList<SourceToken> getAllNotSynchedSourceToken() {

		ArrayList<SourceToken> sourceTokens = new ArrayList<SourceToken>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_TOKEN where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceToken sourceToken = getSourceToken(rs);
				sourceTokens.add(sourceToken);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceTokens;
	}

	public static void updateSourceToken(Long id, 
										 Timestamp dateTime,
										 String name) {

		String strUpdateQuery = "update SOURCE_TOKEN set AnalysisDate=?, name=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}