package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.FunctionalRequirement;
import com.wadia.iise.dataobjects.Project;

/*
 * Purpose: 
 * Interface to do CRUD on functional requirements related information in db.
 */

public class FunctionalRequirementModel  extends BaseModel {
	
	private static FunctionalRequirement getFunctionalRequirement(ResultSet rs) {
	    try {
	    	String name = rs.getString("Name");
			String description = rs.getString("Description");
			
			FunctionalRequirement projectRequirement = new FunctionalRequirement();
			projectRequirement.id =  rs.getLong("ID");
			projectRequirement.project_id = rs.getLong("PROJECT_ID");
			projectRequirement.name = name;
			projectRequirement.description = description;
			projectRequirement.cloudid = rs.getString("CLOUD_ID");
			if (projectRequirement.cloudid == null) {
				projectRequirement.cloudid = "";
			}
			projectRequirement.synched = rs.getLong("Synched");
			
			Project project = ProjectModel.getProjectById(projectRequirement.project_id);
			
			projectRequirement.project_cloudid = project.cloudid;
			
			return projectRequirement;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Boolean createProjectRequirement(Long project_id,
												   String name,
												   String description) {

		String insertQuery = "Insert into PROJECT_REQUIREMENT(Name,Description,PROJECT_ID) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, description);
			prepStatement.setLong(3, project_id);
			
			prepStatement.execute();
		
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<FunctionalRequirement> getAllProjectRequirements(Long projectID) {

		ArrayList<FunctionalRequirement> projectRequirements = new ArrayList<FunctionalRequirement>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT_REQUIREMENT where PROJECT_ID=?");
			prepStatement.setLong(1, projectID);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				FunctionalRequirement projectRequirement = getFunctionalRequirement(rs);
				
				projectRequirements.add(projectRequirement);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return projectRequirements;
	}

	public static ArrayList<FunctionalRequirement> getAllNotSynchedProjectRequirements() {

		ArrayList<FunctionalRequirement> projectRequirements = new ArrayList<FunctionalRequirement>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT_REQUIREMENT where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				FunctionalRequirement projectRequirement = getFunctionalRequirement(rs);
				projectRequirements.add(projectRequirement);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return projectRequirements;
	}

	public static void updateProjectRequirement(Long id, 
												String name, 
												String description) {

		String strUpdateQuery = "update PROJECT_REQUIREMENT set Name=?, Description=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, description);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}
