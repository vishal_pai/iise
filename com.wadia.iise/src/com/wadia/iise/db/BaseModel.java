package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/*
 * Purpose: 
 * Base model from which other models derive...
 */

public class BaseModel {
	
	/*
	 * Once entity is synched successfully to cloud, 
	 * mark the row in the table to synched and also save the cloud id
	 * 
	 */
	public static Boolean synchToCloudSuccessful(String tableName, 
												 Long id, 
												 String cloudid) {
		String strUpdateQuery = "update " + tableName + " set Synched=?, CLOUD_ID=? where ID=?";
				
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setLong(1, 1);
			prepStatement.setString(2, cloudid);
			prepStatement.setLong(3, id);
			prepStatement.execute();
			
			return true;
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return false;
	}
	
	/*
	 * Delete entity from the table...
	 */
	public static void deleteEntity(String tableName, Long id) {
		try {
			String deleteQuery = "delete from " + tableName + " where id=?";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(deleteQuery);
			prepStatement.setLong(1, id);
			prepStatement.execute();
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}
