package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.Feasibility;
import com.wadia.iise.dataobjects.MasterQuestion;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Interface to do CRUD on feasibility related information in db.
 */

public class FeasibilityModel  extends BaseModel {
	
	private static Feasibility getFeasibility(ResultSet rs) {
		Feasibility feasibility = new Feasibility();
		
		try {
			feasibility.id = rs.getLong("ID");
			feasibility.project_id = rs.getLong("PROJECT_ID");
			feasibility.question_id = rs.getLong("QUESTION_ID");
			feasibility.description = rs.getString("Details");
			feasibility.type = rs.getString("Type");
			feasibility.cloudid = rs.getString("CLOUD_ID");
			if (feasibility.cloudid == null) {
				feasibility.cloudid = "";
			}
			
			feasibility.synched = rs.getLong("Synched");
			
			Project project = ProjectModel.getProjectById(feasibility.project_id);
			feasibility.project_cloudid = project.cloudid;
			
			MasterQuestion masterQuestion = MasterQuestionModel.getMasterQuestionById(feasibility.question_id);
			feasibility.question = masterQuestion.question;
			
			return feasibility;
		}
		catch(SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		catch(Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return null;
	}
	
	public static Boolean createFeasibility(Long project_id,
											Long question_id,
											String details,
											String type) {

		String insertQuery = "Insert into FEASIBILITY (PROJECT_ID,QUESTION_ID," + 
							 "Details, Type) values(?,?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setLong(1, project_id);
			prepStatement.setLong(2, question_id);
			prepStatement.setString(3, details);
			prepStatement.setString(4, type);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<Feasibility> getAllFeasibility(Long projectID,
														    String type) {

		ArrayList<Feasibility> listFeasibility = new ArrayList<Feasibility>();
		
		try {
			String selectQuery = "select * from FEASIBILITY where PROJECT_ID=? and type=?";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			prepStatement.setLong(1, projectID);
			prepStatement.setString(2, type);
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Feasibility feasibility = getFeasibility(rs);
				
				listFeasibility.add(feasibility);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return listFeasibility;
	}
	
	public static ArrayList<Feasibility> getAllNotSynchedFeasibility() {

	ArrayList<Feasibility> listFeasibility = new ArrayList<Feasibility>();
	
	try {
			String selectQuery = "select * from FEASIBILITY where Synched is NULL";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Feasibility feasibility = getFeasibility(rs);
				
				listFeasibility.add(feasibility);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return listFeasibility;
	}

	public static void updateFeasibility(Long id, 
									     Long question_id,
									     String details) {

		String strUpdateQuery = "update FEASIBILITY set question_id=?,details=?,Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setLong(1, question_id);
			prepStatement.setString(2, details);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}
