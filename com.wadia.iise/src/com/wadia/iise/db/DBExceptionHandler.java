package com.wadia.iise.db;

import java.sql.SQLException;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.helper.Utility;

public class DBExceptionHandler {
	public static void handleException(SQLException e) {
		DBHelper.clearInterruptFlag(e);
		e.printStackTrace();
		Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
	}
}
