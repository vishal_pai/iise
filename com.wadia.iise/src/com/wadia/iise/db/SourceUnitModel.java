package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.SourceUnit;

/*
 * Purpose: 
 * Interface to do CRUD on class related information in db.
 */

public class SourceUnitModel extends BaseModel {
	
	private static SourceUnit getSourceUnit(ResultSet rs) {
	    try {
	    	SourceUnit sourceUnit = new SourceUnit();
	    	sourceUnit.dateTime = rs.getTimestamp("AnalysisDate");
			sourceUnit.name = rs.getString("Name");
			sourceUnit.path = rs.getString("Path");
			sourceUnit.numLines = rs.getLong("NumLines");
			sourceUnit.numComments = rs.getLong("NumComments");
			sourceUnit.numBlanks = rs.getLong("NumBlanks");
			sourceUnit.numFields = rs.getLong("NumFields");
			sourceUnit.numTokens = rs.getLong("NumTokens");
			sourceUnit.numMethods = rs.getLong("NumMethods");
			sourceUnit.callerCount = rs.getLong("CallerCount");
			sourceUnit.calleeCount = rs.getLong("CalleeCount");
			sourceUnit.project_id = rs.getLong("PROJECT_ID");
			sourceUnit.cloudid = rs.getString("CLOUD_ID");
			if (sourceUnit.cloudid == null) {
				sourceUnit.cloudid = "";
			}
			sourceUnit.synched = rs.getLong("Synched");
			
			sourceUnit.id = rs.getLong("ID");
			
			Project project = ProjectModel.getProjectById(sourceUnit.project_id);
			
			sourceUnit.project_cloudid = project.cloudid;
			
			return sourceUnit;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Long createSourceUnit(Long project_id,
										   Timestamp dateTime,
										   String name,
										   Long numLines,
										   Long numComments,
										   Long numBlanks,
										   Long numFields,
										   Long numTokens,
										   Long numMethods,
										   String path,
										   Long callerCount,
										   Long calleeCount) {

		String insertQuery = "Insert into SOURCE_UNIT(AnalysisDate, Name, NumLines, NumComments, " +
							 "NumBlanks, NumFields, NumTokens, NumMethods, Path, CallerCount, CalleeCount," + 
							 "PROJECT_ID) values(?,?,?,?,?,?,?,?,?,?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, numLines);
			prepStatement.setLong(4, numComments);
			prepStatement.setLong(5, numBlanks);
			prepStatement.setLong(6, numFields);
			prepStatement.setLong(7, numTokens);
			prepStatement.setLong(8, numMethods);
			prepStatement.setString(9, path);
			prepStatement.setLong(10, callerCount);
			prepStatement.setLong(11, calleeCount);
			prepStatement.setLong(12, project_id);
			
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long)0;
	}

	public static ArrayList<SourceUnit> getAllSourceUnits(Long projectID) {

		ArrayList<SourceUnit> sourceUnits = new ArrayList<SourceUnit>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_UNIT where PROJECT_ID=? order by ANALYSISDATE");
			prepStatement.setLong(1, projectID);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceUnit sourceUnit = getSourceUnit(rs);
				
				sourceUnits.add(sourceUnit);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceUnits;
	}

	public static ArrayList<SourceUnit> getAllSourceUnitsByTimeStamp(Long projectID, Timestamp t) {

		ArrayList<SourceUnit> sourceUnits = new ArrayList<SourceUnit>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_UNIT where PROJECT_ID=? and AnalysisDate=?");
			prepStatement.setLong(1, projectID);
			prepStatement.setTimestamp(2,t);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceUnit sourceUnit = getSourceUnit(rs);
				
				sourceUnits.add(sourceUnit);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceUnits;
	}
	
	public static ArrayList<SourceUnit> getAllNotSynchedSourceUnits() {

		ArrayList<SourceUnit> sourceUnits = new ArrayList<SourceUnit>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_UNIT where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceUnit sourceUnit = getSourceUnit(rs);
				
				sourceUnits.add(sourceUnit);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceUnits;
	}
	
	public static void updateCallerCount(Timestamp t, Long id, Long count) {
		String strUpdateQuery = "update SOURCE_UNIT set CallerCount=? where ID=? and AnalysisDate=?";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setLong(1, count);
			prepStatement.setLong(2, id);
			prepStatement.setTimestamp(3, t);
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
	
	public static void updateCalleeCount(Timestamp t, Long id, Long count) {
		String strUpdateQuery = "update SOURCE_UNIT set CalleeCount=? where ID=? and AnalysisDate=?";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setLong(1, count);
			prepStatement.setLong(2, id);
			prepStatement.setTimestamp(3, t);
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
	
	public static void updateSourceUnit(Long id, 
										Timestamp dateTime,
										String name,
										Long numLines,
										Long numComments,
										Long numBlanks,
										Long numFields,
										Long numTokens,
										Long numMethods,
										String path,
										Long callerCount,
										Long calleeCount) {

		String strUpdateQuery = "update SOURCE_UNIT set AnalysisDate=?,name=?, NumLines=?" +
							    ", NumComments=?, NnumBlanks=?, NumFields=?, NumTokens=?" + 
							    ", NumMethods=?, Path=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, numLines);
			prepStatement.setLong(4, numComments);
			prepStatement.setLong(5, numBlanks);
			prepStatement.setLong(6, numFields);
			prepStatement.setLong(7, numTokens);
			prepStatement.setLong(8, numMethods);
			prepStatement.setString(9, path);
			prepStatement.setLong(10, callerCount);
			prepStatement.setLong(11, calleeCount);
			prepStatement.setLong(12, id);
			
			prepStatement.execute();

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}

	
	public static SourceUnit getSourceUnitById(Long id) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_UNIT where ID=?");
		prepStatement.setLong(1, id);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		SourceUnit sourceUnit = getSourceUnit(rs);
		
		return sourceUnit;
	}
	
	public static SourceUnit getSourceUnitByPath(String path, Timestamp t) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_UNIT where Path=? and AnalysisDate=?");
		prepStatement.setString(1, path);
		prepStatement.setTimestamp(2, t);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		SourceUnit sourceUnit = getSourceUnit(rs);
		
		return sourceUnit;
	}
}