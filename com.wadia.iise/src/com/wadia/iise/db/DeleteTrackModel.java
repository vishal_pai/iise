package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.DeleteTrack;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Interface to do CRUD on tracking information. 
 * Delete information needs to be tracked as it needs to be synched to the cloud...
 */

public class DeleteTrackModel extends BaseModel {
	
	private static DeleteTrack getDeleteTracker(ResultSet rs) {
		try {
			
			DeleteTrack deleteTrack = new DeleteTrack();
			deleteTrack.type = rs.getString("TYPE");
			deleteTrack.cloudid = rs.getString("CLOUD_ID");
			deleteTrack.id = rs.getLong("ID");
			
			if (deleteTrack.cloudid == null) {
				deleteTrack.cloudid = "";
			}
			
			return deleteTrack;
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		catch (Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return null;
	}
	
	public static Boolean createDeleteTracker(String type,
											  String cloudid) {

		String insertQuery = "Insert into DELETE_TRACKER (TYPE,CLOUD_ID) values(?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setString(1, type);
			prepStatement.setString(2, cloudid);
			
			prepStatement.execute();

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<DeleteTrack> getAllDeleteTracker() {

		ArrayList<DeleteTrack> deleteTracker = new ArrayList<DeleteTrack>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from DELETE_TRACKER");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				DeleteTrack deleteTrack = getDeleteTracker(rs);
				deleteTracker.add(deleteTrack);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return deleteTracker;
	}
	
	public static ArrayList<DeleteTrack> getAllNotSynchedDeleteTracker() {

		ArrayList<DeleteTrack> deleteTracker = new ArrayList<DeleteTrack>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from DELETE_TRACKER");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				DeleteTrack deleteTrack = getDeleteTracker(rs);
				deleteTracker.add(deleteTrack);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return deleteTracker;
	}  
}
