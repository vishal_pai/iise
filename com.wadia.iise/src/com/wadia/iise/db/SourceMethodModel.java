
package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.MethodComplexity;
import com.wadia.iise.dataobjects.SourceMethod;
import com.wadia.iise.dataobjects.SourceUnit;

/*
 * Purpose: 
 * Interface to do CRUD on member method related information in db.
 */

public class SourceMethodModel  extends BaseModel {
	private static SourceMethod getSourceMethod(ResultSet rs) {
	    try {
	    	SourceMethod sourceMethod = new SourceMethod();
	    	sourceMethod.dateTime = rs.getTimestamp("AnalysisDate");
			sourceMethod.name = rs.getString("Name");
			sourceMethod.numLines = rs.getLong("NumLines");
			sourceMethod.numComments = rs.getLong("NumComments");
			sourceMethod.numBlanks = rs.getLong("NumBlanks");
			sourceMethod.numTokens = rs.getLong("NumTokens");
			sourceMethod.complexity = rs.getLong("Complexity");
			sourceMethod.unit_id = rs.getLong("UNIT_ID");
			sourceMethod.cloudid = rs.getString("CLOUD_ID");
			if (sourceMethod.cloudid == null) {
				sourceMethod.cloudid = "";
			}
			
			sourceMethod.synched = rs.getLong("Synched");
			sourceMethod.id = rs.getLong("ID");
			
			SourceUnit sourceUnit = SourceUnitModel.getSourceUnitById(sourceMethod.unit_id);
			sourceMethod.unit_cloudid = sourceUnit.cloudid;
			
			return sourceMethod;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Long createSourceMethod(Long unit_id,
										  Timestamp dateTime,
										  String name,
										  Long numLines,
										  Long numComments,
										  Long numBlanks,
										  Long numTokens,
										  Long complexity) {

		String insertQuery = "Insert into SOURCE_METHOD(AnalysisDate, Name, NumLines, NumComments, " +
							 "NumBlanks, NumTokens, Complexity, " + 
							 "UNIT_ID) values(?,?,?,?,?,?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setTimestamp(1, dateTime);
			prepStatement.setString(2, name);
			prepStatement.setLong(3, numLines);
			prepStatement.setLong(4, numComments);
			prepStatement.setLong(5, numBlanks);
			prepStatement.setLong(6, numTokens);
			prepStatement.setLong(7, complexity);
			prepStatement.setLong(8, unit_id);
			
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long)0;
	}

	public static ArrayList<SourceMethod> getAllSourceMethods(Long unitID) {

		ArrayList<SourceMethod> sourceMethods = new ArrayList<SourceMethod>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_METHOD where UNIT_ID=?");
			prepStatement.setLong(1, unitID);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceMethod sourceMethod = getSourceMethod(rs);
				
				sourceMethods.add(sourceMethod);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceMethods;
	}
	
	public static ArrayList<SourceMethod> getAllSourceMethods() {

		ArrayList<SourceMethod> sourceMethods = new ArrayList<SourceMethod>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_METHOD");
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceMethod sourceMethod = getSourceMethod(rs);
				
				sourceMethods.add(sourceMethod);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceMethods;
	}

	public static ArrayList<MethodComplexity> getAllSourceMethodsByTimeStamp(Long projectId, Timestamp t) {

		ArrayList<MethodComplexity> sourceMethods = new ArrayList<MethodComplexity>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select SOURCE_METHOD.ID as Id, SOURCE_METHOD.UNIT_ID as UID, " + 
					" SOURCE_METHOD.AnalysisDate as AnalysisDate, SOURCE_METHOD.Name as Name, " +
					" SOURCE_METHOD.Complexity as Complexity, SOURCE_UNIT.Path as UnitName " +
					" from PROJECT join SOURCE_UNIT on PROJECT.ID = SOURCE_UNIT.PROJECT_ID" + 
					" join SOURCE_METHOD on SOURCE_UNIT.ID = SOURCE_METHOD.UNIT_ID" + 
					" where PROJECT_ID=? and SOURCE_METHOD.AnalysisDate=? order by SOURCE_METHOD.Complexity desc");
			
			prepStatement.setLong(1, projectId);
			prepStatement.setTimestamp(2, t);
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				MethodComplexity method = new MethodComplexity();
				method.analysisdate = t;
				method.id = rs.getLong("Id");
				method.methodName = rs.getString("Name");
				method.unitName = rs.getString("UnitName");
				method.complexity = rs.getLong("Complexity");
				method.unitid = rs.getLong("UID");
				sourceMethods.add(method);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceMethods;
	}

	public static ArrayList<SourceMethod> getAllNotSynchedSourceMethods() {

		ArrayList<SourceMethod> sourceMethods = new ArrayList<SourceMethod>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_METHOD where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			while (rs.next()) {
				SourceMethod sourceMethod = getSourceMethod(rs);
				
				sourceMethods.add(sourceMethod);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return sourceMethods;
	}

	public static SourceMethod getSourceMethodById(Long id) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from SOURCE_METHOD where ID=?");
		prepStatement.setLong(1, id);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		SourceMethod sourceMethod = getSourceMethod(rs);
		
		return sourceMethod;
	}
}