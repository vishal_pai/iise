package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.StakeHolderType;
import com.wadia.iise.helper.Utility;

public class StakeHolderTypeModel extends BaseModel {
	
	private static StakeHolderType getStakeHolderType(ResultSet rs) {
		StakeHolderType stakeHolderType = new StakeHolderType();
		
		try {
			stakeHolderType.id = rs.getLong("ID");
			stakeHolderType.type = rs.getString("Type");
			stakeHolderType.cloudid = rs.getString("CLOUD_ID");
			if (stakeHolderType.cloudid == null) {
				stakeHolderType.cloudid = "";
			}
			
			stakeHolderType.synched = rs.getLong("Synched");
			
			return stakeHolderType;
		}
		catch(SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		catch(Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return null;
	}
	
	public static Boolean createStakeHolderType(String type) {

		String insertQuery = "Insert into MASTER_STAKEHOLDER (Type) values(?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setString(1, type);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<StakeHolderType> getAllStakeHolderTypes() {

		ArrayList<StakeHolderType> stakeHolderTypes = new ArrayList<StakeHolderType>();
		
		try {
			String selectQuery = "select * from MASTER_STAKEHOLDER";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				StakeHolderType stakeHolderType = getStakeHolderType(rs);
				
				stakeHolderTypes.add(stakeHolderType);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return stakeHolderTypes;
	}
	
	public static StakeHolderType getStakeHolderTypeById(long id) {

		StakeHolderType stakeHolderType = null;
		
		try {
			String selectQuery = "select * from MASTER_STAKEHOLDER where ID=?";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			prepStatement.setLong(1, id);
			ResultSet rs = prepStatement.executeQuery();
			
			rs.next();
			stakeHolderType = getStakeHolderType(rs);
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return stakeHolderType;
	}
	
	public static ArrayList<StakeHolderType> getAllNotSynchedStakeHolderTypes() {

		ArrayList<StakeHolderType> stakeHolderTypes = new ArrayList<StakeHolderType>();
		
		try {
			String selectQuery = "select * from MASTER_STAKEHOLDER where Synched is NULL";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				StakeHolderType stakeHolderType = getStakeHolderType(rs);
				
				stakeHolderTypes.add(stakeHolderType);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return stakeHolderTypes;
	}
}
