package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Helper functions to make db easier to deal with...
 */

public class DBHelper {
	//If a thread is interrupted then derby db detects it and fails...
	//This function is a workaround to clear the interrupted flag...
	//If this function is not called derby keeps failing with 08000 error with below exception...
	//java.sql.SQLNonTransientConnectionException: Connection closed by unknown interrupt.
	public static Boolean clearInterruptFlag(SQLException e) {
		
		if (e.getSQLState().contains("8000")) {
			Thread.interrupted();
		}
		
		Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		return true;
	}
	
	//Deletes entity from the table
	//Also creates delete tracker entry so that entity can be deleted from cloud when synch is done...
	public static void deleteEntity(String tableName, Long id, String cloudId) {
		Connection conn = null;
		
		try {
			conn = DBConnection.getConnection();
			
			conn.setAutoCommit(false);
			
			BaseModel.deleteEntity(tableName, id);
			
			if ((cloudId != null) && (cloudId.length() > 0)) {
				DeleteTrackModel.createDeleteTracker(tableName, cloudId);
			}
			
			conn.commit();
			
		}
		catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				DBExceptionHandler.handleException(e);
			}
			
			DBExceptionHandler.handleException(e);
		}
		finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				DBExceptionHandler.handleException(e);
			}
		}
	}
	
	//Add project + glossary information.
	//In create project screen project is not yet created. 
	//But user can press ctrl + g to create glossary. 
	//Glossary created cannot be saved until project is created...
	//Hence this function...
	public static void addProjectAndGlossary(String name, 
											 String problem, 
											 String solution, 
											 Date startDate, 
											 Date endDate,
											 ArrayList<String> glossary) {
		Connection conn = null;
		
		try {
			conn = DBConnection.getConnection();
			
			conn.setAutoCommit(false);
			
			Long id = ProjectModel.createProject(name, problem, solution, startDate, endDate);
			for (String glossaryname : glossary) {
				GlossaryModel.createGlossary(id, glossaryname, "To be entered");
			}
			
			conn.commit();
			
		}
		catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				DBExceptionHandler.handleException(e);
			}
			DBExceptionHandler.handleException(e);
		}
		finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				DBExceptionHandler.handleException(e);
			}
		}
	}
}
