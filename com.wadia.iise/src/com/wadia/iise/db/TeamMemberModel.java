package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.Team;

/*
 * Purpose: 
 * Interface to do CRUD on team related information in db.
 */

public class TeamMemberModel extends BaseModel  {
	
	private static Team getTeam(ResultSet rs) {
		try {
			
			Team team = new Team();
			team.id = rs.getLong("ID");
			team.project_id = rs.getLong("PROJECT_ID");
			team.name = rs.getString("Name");
			team.rollno = rs.getString("RollNo");
			team.synched = rs.getLong("Synched");
			team.cloudid = rs.getString("CLOUD_ID");
			if (team.cloudid == null) {
				team.cloudid = "";
			}
			
			Project project = ProjectModel.getProjectById(team.project_id);
			
			team.project_cloudid = project.cloudid;
			
			return team;
		}
		catch (SQLException e) {
			e.printStackTrace();
			DBHelper.clearInterruptFlag(e);
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	public static Boolean createTeamMember(Long project_id,
										   String name,
										   String rollno) {

		String insertQuery = "Insert into TEAM(Name,RollNo,PROJECT_ID) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, rollno);
			prepStatement.setLong(3, project_id);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<Team> getAllTeamMembers(Long projectID) {

		ArrayList<Team> teams = new ArrayList<Team>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from TEAM where PROJECT_ID=?");
			prepStatement.setLong(1, projectID);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Team team = getTeam(rs);
				
				teams.add(team);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return teams;
	}

	public static ArrayList<Team> getNotSynchedTeamMembers() {

		ArrayList<Team> teams = new ArrayList<Team>();
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from TEAM where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Team team = getTeam(rs);
				
				teams.add(team);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return teams;
	}
	
	public static boolean updateTeamMember(Long id, 
										String name, 
										String rollno) {

		String strUpdateQuery = "update team set Name=?,RollNo=?,Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, rollno);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();
			
			return true;
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}
	}
}
