package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.wadia.iise.dataobjects.SynchSetup;

public class SynchSetupModel {
	
	private static SynchSetup getSynchSetup(ResultSet rs) {
		
		try {
			SynchSetup setup = new SynchSetup();
			
			setup.id = rs.getLong("ID");
			setup.url = rs.getString("URL");
			
			return setup;
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return null;
	}
	
	public static Long createProject(String URL) {
		
		String insertQuery = "Insert into SYNCH_SETUP (URL) values(?)";
		
		
		try {
			
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setString(1, URL);
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long) 0;
	}
	
	public static SynchSetup getSynchSetup() {
		
		try {
			
			Connection conn = DBConnection.getConnection();
			
			PreparedStatement prepStatement = conn.prepareStatement("select * from SYNCH_SETUP");
			
			ResultSet rs = prepStatement.executeQuery();
			rs.next();
				
			SynchSetup setup = getSynchSetup(rs);
			
			return setup;
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return null;
	}

	public static void updateSynchSetup(Long id, String url) {
		
		String strUpdateQuery = "update SYNCH_SETUP set URL=? where ID=?";
								
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, url);
			prepStatement.setLong(2, id);
			prepStatement.execute();
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}
