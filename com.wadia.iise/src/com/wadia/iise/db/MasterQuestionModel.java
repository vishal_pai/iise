package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.MasterQuestion;

/*
 * Purpose: 
 * Interface to do CRUD on masterquestion related information in db.
 */

public class MasterQuestionModel   extends BaseModel {
	
	private static MasterQuestion getMasterQuestion(ResultSet rs) {
		try {
			MasterQuestion masterQuestion = new MasterQuestion();
			masterQuestion.id = rs.getLong("ID");
			masterQuestion.question = rs.getString("Question");
			masterQuestion.type = rs.getString("type");
			masterQuestion.cloudid = rs.getString("CLOUD_ID");
			if (masterQuestion.cloudid == null) {
				masterQuestion.cloudid = "";
			}
			masterQuestion.synched = rs.getLong("Synched");
			return masterQuestion;
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return null;
	}
	
	public static Long createMasterQuestion(String question,
											String type) {
		
		String insertQuery = "Insert into MASTER_QUESTION(Question,Type) values(?,?)";
		
		try {
			
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setString(1, question);
			prepStatement.setString(2, type);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long) 0;
	}
	
	public static ArrayList<MasterQuestion> getAllMasterQuestions(String type) {
		
		ArrayList<MasterQuestion> masterQuestions = new ArrayList<MasterQuestion>();
		try {
			
			Connection conn = DBConnection.getConnection();
			
			String query = "select * from MASTER_QUESTION where type=?";
			
			PreparedStatement prepStatement = conn.prepareStatement(query);
			prepStatement.setString(1, type);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				
				MasterQuestion masterQuestion = getMasterQuestion(rs);
				
				masterQuestions.add(masterQuestion);
			}
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return masterQuestions;
	}
	
public static ArrayList<MasterQuestion> getAllNotSynchedMasterQuestions() {
		
		ArrayList<MasterQuestion> masterQuestions = new ArrayList<MasterQuestion>();
		try {
			
			Connection conn = DBConnection.getConnection();
			
			String query = "select * from MASTER_QUESTION where Synched is NULL";
			
			PreparedStatement prepStatement = conn.prepareStatement(query);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				
				MasterQuestion masterQuestion = getMasterQuestion(rs);
				
				masterQuestions.add(masterQuestion);
			}
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return masterQuestions;
	}

	public static MasterQuestion getMasterQuestionById(Long id) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from MASTER_QUESTION where ID=?");
		prepStatement.setLong(1, id);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		MasterQuestion masterQuestion = getMasterQuestion(rs);
		
		return masterQuestion;
	}
}
