package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.MasterQuestion;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.Requirement;

/*
 * Purpose: 
 * Interface to do CRUD on requirement related information in db.
 */

public class RequirementModel  extends BaseModel {
	
	private static Requirement getRequirement(ResultSet rs) {
	    try {
	    	Requirement requirement = new Requirement();
			requirement.id = rs.getLong("ID");
			requirement.project_id = rs.getLong("PROJECT_ID");
			requirement.question_id = rs.getLong("QUESTION_ID");
			requirement.details = rs.getString("Details");
			requirement.type = rs.getString("Type");
			requirement.cloudid = rs.getString("CLOUD_ID");
			if (requirement.cloudid == null) {
				requirement.cloudid = "";
			}
			requirement.synched = rs.getLong("Synched");
			Project project = ProjectModel.getProjectById(requirement.project_id);
			
			MasterQuestion masterQuestion = MasterQuestionModel.getMasterQuestionById(requirement.question_id);
			requirement.question = masterQuestion.question;
			
			requirement.project_cloudid = project.cloudid;
			return requirement;
	    }
	    catch (SQLException e) {
	    	DBExceptionHandler.handleException(e);
	    }
	    
	    return null;
	}
	
	public static Boolean createRequirements(Long project_id,
											 Long question_id,
											 String details,
											 String type) {

		String insertQuery = "Insert into REQUIREMENT(PROJECT_ID,QUESTION_ID," + 
							 "Details, Type) values(?,?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setLong(1, project_id);
			prepStatement.setLong(2, question_id);
			prepStatement.setString(3, details);
			prepStatement.setString(4, type);
			
			prepStatement.execute();
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<Requirement> getAllRequirements(Long projectID,
														    String type) {

		ArrayList<Requirement> requirements = new ArrayList<Requirement>();
		
		try {
			String selectQuery = "select * from REQUIREMENT where PROJECT_ID=? and type=?";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			prepStatement.setLong(1, projectID);
			prepStatement.setString(2, type);
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Requirement requirement = getRequirement(rs);
				
				requirements.add(requirement);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return requirements;
	}


	public static ArrayList<Requirement> getAllNotSynchedRequirements() {

		ArrayList<Requirement> requirements = new ArrayList<Requirement>();
		
		try {
			String selectQuery = "select * from REQUIREMENT where Synched is NULL";
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(selectQuery);
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Requirement requirement = getRequirement(rs);
				requirements.add(requirement);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return requirements;
	}


	public static void updateRequirement(Long id, 
									     Long question_id,
										String details) {

		String strUpdateQuery = "update REQUIREMENT set question_id=?, details=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setLong(1, question_id);
			prepStatement.setString(2, details);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}
