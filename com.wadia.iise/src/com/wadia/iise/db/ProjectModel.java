package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.dataobjects.ProjectAnalysis;
import com.wadia.iise.dataobjects.TimeUnitLoc;

/*
 * Purpose: 
 * Interface to do CRUD on project related information in db.
 */

public class ProjectModel  extends BaseModel {
	
	private static Project getProject(ResultSet rs) {
		
		try {
			String name = rs.getString("Name");
			String problem = rs.getString("ExisitingProblem");
			String solution = rs.getString("ExistingProblemSolution");
			Date startDate = rs.getDate("StartDate");
			Date endDate = rs.getDate("EndDate");
			String cloudid = rs.getString("CLOUD_ID");
			
			if (cloudid == null) {
				cloudid = "";
			}
			
			Long synched = rs.getLong("Synched");
			
			Project project = new Project();
			project.id = rs.getLong("ID");
			project.name = name;
			project.problem = problem;
			project.solution = solution;
			project.startDate = startDate;
			project.endDate = endDate;
			project.cloudid = cloudid;
			project.synched = synched;
			
			return project;
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return null;
	}
	
	public static Long createProject(String name,
										String problem,
										String solution,
										Date startDate,
										Date endDate) {
		
		String insertQuery = "Insert into PROJECT(Name,ExisitingProblem,ExistingProblemSolution,StartDate,EndDate) values(?,?,?,?,?)";
		
		
		try {
			
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			prepStatement.setString(1, name);
			prepStatement.setString(2, problem);
			prepStatement.setString(3, solution);
			prepStatement.setDate(4, new java.sql.Date(startDate.getTime()));
			prepStatement.setDate(5, new java.sql.Date(endDate.getTime()));
			
			prepStatement.execute();
			
			ResultSet rs = prepStatement.getGeneratedKeys();
			if (rs != null && rs.next()) {
			    return rs.getLong(1);
			}
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return (long) 0;
	}
	
	public static ArrayList<Project> getAllProjects() {
		
		ArrayList<Project> projects = new ArrayList<Project>();
		try {
			
			Connection conn = DBConnection.getConnection();
			
			PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT");
			
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				
				Project project = getProject(rs);

				projects.add(project);
			}
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return projects;
	}

	public static ArrayList<Project> getNotSynchedProjects() {
		
		ArrayList<Project> projects = new ArrayList<Project>();
		try {
			
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Project project = getProject(rs);

				projects.add(project);
			}
			
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
		
		return projects;
	}

	public static void updateProject(Long id, String name, String problem,
			String solution, Date startDate, Date endDate) {
		
		String strUpdateQuery = "update project set Name=?, ExisitingProblem=?, ExistingProblemSolution=?," +
						        "StartDate=?, EndDate=?, Synched=NULL where ID=?";
								
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, problem);
			prepStatement.setString(3, solution);
			prepStatement.setDate(4, new java.sql.Date(startDate.getTime()));
			prepStatement.setDate(5, new java.sql.Date(endDate.getTime()));
			prepStatement.setLong(6, id);
			prepStatement.execute();
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
	
	public static Project getProjectByName(String name) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT where name=?");
		prepStatement.setString(1, name);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		Project project = getProject(rs);
		
		return project;
	}
	
	public static Project getProjectById(Long id) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select * from PROJECT where ID=?");
		prepStatement.setLong(1, id);
		
		ResultSet rs = prepStatement.executeQuery();
		
		rs.next();
		
		Project project = getProject(rs);
		
		return project;
	}
	
	public static ArrayList<ProjectAnalysis> getProjectAnalysis(Long projectId) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select DISTINCT PROJECT.ID, PROJECT.Name, SOURCE_UNIT.AnalysisDate from PROJECT join SOURCE_UNIT on PROJECT.ID = SOURCE_UNIT.PROJECT_ID where PROJECT.ID = ? order by AnalysisDate desc");
		prepStatement.setLong(1, projectId);
		ResultSet rs = prepStatement.executeQuery();
		
		ArrayList<ProjectAnalysis> analysisList = new ArrayList<ProjectAnalysis>();
		
		while (rs.next()) {
			ProjectAnalysis pa = new ProjectAnalysis();
			pa.name = rs.getString("Name");
			pa.analysisDateTime = rs.getTimestamp("AnalysisDate");
			pa.project_id = rs.getLong("ID");

			analysisList.add(pa);
		}
		
		return analysisList;
		
	}
	
	public static ArrayList<TimeUnitLoc> getLOCByTime(Long projectId) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select AnalysisDate, SUM(NumLines) as TOTALLOC from SOURCE_UNIT where PROJECT_ID=? GROUP BY AnalysisDate");
		prepStatement.setLong(1, projectId);
		ResultSet rs = prepStatement.executeQuery();
		
		ArrayList<TimeUnitLoc> analysisList = new ArrayList<TimeUnitLoc>();
		
		while (rs.next()) {
			TimeUnitLoc timeUnitLoc = new TimeUnitLoc();
			
			timeUnitLoc.analysisDateTime = rs.getTimestamp("AnalysisDate");
			timeUnitLoc.loc = rs.getLong("TOTALLOC");

			analysisList.add(timeUnitLoc);
		}
		
		return analysisList;

	}
	
	public static ArrayList<Timestamp> getAllTimeStamps(Long projectId) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("select DISTINCT AnalysisDate from SOURCE_UNIT where PROJECT_ID=? ORDER BY AnalysisDate");
		prepStatement.setLong(1, projectId);
		ResultSet rs = prepStatement.executeQuery();
		
		ArrayList<Timestamp> analysisList = new ArrayList<Timestamp>();
		
		while (rs.next()) {
			analysisList.add(rs.getTimestamp("AnalysisDate"));
		}
		
		return analysisList;
	}

	public static void deleteProject(Long projectId) throws SQLException {
		Connection conn = DBConnection.getConnection();
		PreparedStatement prepStatement = conn.prepareStatement("delete from PROJECT where ID=?");
		prepStatement.setLong(1, projectId);
		prepStatement.execute();
		
	}
}
