package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.eclipse.core.runtime.IStatus;

import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.helper.Utility;

/*
 * Purpose: 
 * Init the database...
 */

public class DDLScripts {
	
	private static String CREATE_VERSION_TABLE = "CREATE TABLE " + DBConstants.VERSION_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Version INTEGER NOT NULL," +
			"primary key (ID)" +
		")";
	
	
	private static String CREATE_SYNCH_SETUP_TABLE = "CREATE TABLE " + DBConstants.SYNCH_SETUP_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"URL varchar(300) NOT NULL," +
			"primary key (ID)" +
		")";
	
	
	private static String CREATE_MASTER_QUESTION_TABLE = "CREATE TABLE " + DBConstants.MASTER_QUESTION_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Question varchar(500) NOT NULL," +
			"Type varchar(100) NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," +
			"primary key (ID)" +
		")";
	
	private static String CREATE_DELETE_TABLE = "CREATE TABLE " + DBConstants.DELETE_TRACKER_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
			"TYPE varchar(100)," +
			"CLOUD_ID varchar(200)" +
		")";

	
	private static String CREATE_MASTER_STAKEHOLDER_TABLE = "CREATE TABLE " + DBConstants.MASTER_STAKEHOLDER_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Type varchar(100) NOT NULL UNIQUE," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String CREATE_STAKEHOLDERS_SETUP_TABLE = "CREATE TABLE " + DBConstants.STAKEHOLDER_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Name varchar(300) NOT NULL," +
			"TYPE_ID INTEGER NOT NULL," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER1_STAKEHOLDER_TABLE = "ALTER TABLE STAKEHOLDER ADD CONSTRAINT STAKEHOLDER_MASTERSTAKEHOLDER_FK FOREIGN KEY (TYPE_ID) REFERENCES MASTER_STAKEHOLDER(ID)";
	private static String ALTER2_STAKEHOLDER_TABLE = "ALTER TABLE STAKEHOLDER ADD CONSTRAINT STAKEHOLDER_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID)  ON DELETE CASCADE";
	
	
	private static String CREATE_PROJECT_TABLE = "CREATE TABLE " + DBConstants.PROJECT_TABLE + " (" +
													"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
													"Name varchar(100) NOT NULL," +
													"ExisitingProblem varchar(5000) NOT NULL," +
													"ExistingProblemSolution varchar(5000) NOT NULL," +
													"StartDate date NOT NULL," +
													"EndDate date NOT NULL," + 
													"Synched INTEGER," + 
													"CLOUD_ID varchar(200)," + 
													"primary key (ID)" +
												")";
	
	private static String CREATE_TEAM_TABLE = "CREATE TABLE " + DBConstants.TEAM_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Name varchar(100) NOT NULL," +
			"RollNo varchar(20) NOT NULL UNIQUE," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_TEAM_TABLE = "ALTER TABLE TEAM ADD CONSTRAINT TEAM_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID) ON DELETE CASCADE";
	
	private static String CREATE_PROJECT_REQUIREMENT_TABLE = "CREATE TABLE " + DBConstants.PROJECT_REQUIREMENT_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Name varchar(500) NOT NULL," +
			"Description varchar(5000) NOT NULL," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_PROJECT_REQUIREMENT_TABLE = "ALTER TABLE PROJECT_REQUIREMENT ADD CONSTRAINT PROJECT_REQUIREMENT_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID) ON DELETE CASCADE";
	
	private static String CREATE_REQUIREMENT_TABLE = "CREATE TABLE " + DBConstants.REQUIREMENT_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"QUESTION_ID INTEGER NOT NULL," +
			"Details varchar(5000) NOT NULL," +
			"Type varchar(100) NOT NULL," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER1_REQUIREMENT_TABLE = "ALTER TABLE REQUIREMENT ADD CONSTRAINT REQUIREMENT_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID)  ON DELETE CASCADE";
	private static String ALTER2_REQUIREMENT_TABLE = "ALTER TABLE REQUIREMENT ADD CONSTRAINT REQUIREMENT_MASTER_QUESTION_FK FOREIGN KEY (QUESTION_ID) REFERENCES MASTER_QUESTION(ID)";
	
	
	private static String CREATE_FEASIBILITY_TABLE = "CREATE TABLE " + DBConstants.FEASIBILITY_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"QUESTION_ID INTEGER NOT NULL," +
			"Details varchar(5000) NOT NULL," +
			"Type varchar(100) NOT NULL," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER1_FEASIBILITY_TABLE = "ALTER TABLE FEASIBILITY ADD CONSTRAINT FEASIBILITY_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID) ON DELETE CASCADE";
	private static String ALTER2_FEASIBILITY_TABLE = "ALTER TABLE FEASIBILITY ADD CONSTRAINT FEASIBILITY_MASTER_QUESTION_FK FOREIGN KEY (QUESTION_ID) REFERENCES MASTER_QUESTION(ID)";
	
	
	private static String GLOSSARY_TABLE = "CREATE TABLE " + DBConstants.GLOSSARY_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"Name varchar(100) NOT NULL," +
			"Description varchar(4990) NOT NULL," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_GLOSSARY_TABLE = "ALTER TABLE GLOSSARY ADD CONSTRAINT GLOSSARY_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID) ON DELETE CASCADE";
	
	private static String CREATE_SOURCE_UNIT_TABLE = "CREATE TABLE " + DBConstants.SOURCE_UNIT_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"AnalysisDate TIMESTAMP NOT NULL," +
			"Name varchar(500) NOT NULL," +
			"NumLines INTEGER NOT NULL," +
			"NumComments INTEGER NOT NULL," +
			"NumBlanks INTEGER NOT NULL," +
			"NumMethods INTEGER NOT NULL," +
			"NumTokens INTEGER NOT NULL," +
			"NumFields INTEGER NOT NULL," +
			"Path varchar(1000) NOT NULL," +
			"CallerCount INTEGER," +
			"CalleeCount INTEGER," +
			"PROJECT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_SOURCE_UNIT_TABLE = "ALTER TABLE SOURCE_UNIT ADD CONSTRAINT COMPILE_UNIT_PROJECT_FK FOREIGN KEY (PROJECT_ID) REFERENCES PROJECT(ID) ON DELETE CASCADE";
	
	private static String CREATE_SOURCE_METHOD_TABLE = "CREATE TABLE " + DBConstants.SOURCE_METHOD_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"AnalysisDate TIMESTAMP NOT NULL," +
			"Name varchar(500) NOT NULL," +
			"NumLines INTEGER NOT NULL," +
			"NumComments INTEGER NOT NULL," +
			"NumBlanks INTEGER NOT NULL," +
			"NumTokens INTEGER NOT NULL," +
			"UNIT_ID INTEGER NOT NULL," +
			"Complexity INTEGER," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_SOURCE_METHOD_TABLE = "ALTER TABLE SOURCE_METHOD ADD CONSTRAINT METHOD_COMPILE_UNIT_FK FOREIGN KEY (UNIT_ID) REFERENCES SOURCE_UNIT(ID) ON DELETE CASCADE";
	
	private static String CREATE_SOURCE_FIELD_TABLE = "CREATE TABLE " + DBConstants.SOURCE_FIELD_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"AnalysisDate TIMESTAMP NOT NULL," +
			"Name varchar(500) NOT NULL," +
			"UNIT_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_SOURCE_FIELD_TABLE = "ALTER TABLE SOURCE_FIELD ADD CONSTRAINT FIELD_COMPILE_UNIT_FK FOREIGN KEY (UNIT_ID) REFERENCES SOURCE_UNIT(ID) ON DELETE CASCADE";
	
	private static String CREATE_SOURCE_TOKEN_TABLE = "CREATE TABLE " + DBConstants.SOURCE_TOKEN_TABLE + " (" +
			"ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," + 
			"AnalysisDate TIMESTAMP NOT NULL," +
			"Name varchar(500) NOT NULL," +
			"METHOD_ID INTEGER NOT NULL," +
			"Synched INTEGER," +
			"CLOUD_ID varchar(200)," + 
			"primary key (ID)" +
		")";
	
	private static String ALTER_SOURCE_TOKEN_TABLE = "ALTER TABLE SOURCE_TOKEN ADD CONSTRAINT TOKEN_METHOD_FK FOREIGN KEY (METHOD_ID) REFERENCES SOURCE_METHOD(ID) ON DELETE CASCADE";
	
	
	
	private static String DDL_SCRIPT [] = new String[] {CREATE_VERSION_TABLE,
														CREATE_SYNCH_SETUP_TABLE,
														CREATE_MASTER_QUESTION_TABLE,
														CREATE_PROJECT_TABLE, 
														CREATE_MASTER_STAKEHOLDER_TABLE,
														CREATE_STAKEHOLDERS_SETUP_TABLE,
														ALTER1_STAKEHOLDER_TABLE,
														ALTER2_STAKEHOLDER_TABLE,
														CREATE_TEAM_TABLE,
														ALTER_TEAM_TABLE,
														CREATE_PROJECT_REQUIREMENT_TABLE,
														ALTER_PROJECT_REQUIREMENT_TABLE,
														CREATE_REQUIREMENT_TABLE,
														ALTER1_REQUIREMENT_TABLE,
														ALTER2_REQUIREMENT_TABLE,
														CREATE_FEASIBILITY_TABLE,
														ALTER1_FEASIBILITY_TABLE,
														ALTER2_FEASIBILITY_TABLE,
														GLOSSARY_TABLE,
														ALTER_GLOSSARY_TABLE,
														CREATE_SOURCE_UNIT_TABLE,
														ALTER_SOURCE_UNIT_TABLE,
														CREATE_SOURCE_METHOD_TABLE,
														ALTER_SOURCE_METHOD_TABLE,
														CREATE_SOURCE_FIELD_TABLE,
														ALTER_SOURCE_FIELD_TABLE,
														CREATE_SOURCE_TOKEN_TABLE,
														ALTER_SOURCE_TOKEN_TABLE,
														CREATE_DELETE_TABLE};
	
	private static String REQUIREMENT_MASTER_DATA [][] = new String [][] {
		{"Provide hardware specification for application server",DBConstants.HARDWARE_REQUIREMENTS},
		{"Provide count of application servers needed",DBConstants.HARDWARE_REQUIREMENTS},
		{"Provide hardware specification for database server",DBConstants.HARDWARE_REQUIREMENTS},
		{"Provide details of other hardware needed",DBConstants.HARDWARE_REQUIREMENTS},
		{"Provide details on the database that will be used",DBConstants.SOFTWARE_REQUIREMENTS},
		{"Provide details other tools on which the project is dependent",DBConstants.SOFTWARE_REQUIREMENTS},
		{"Provide details on the programming languages that will be used",DBConstants.DEVELOPER_REQUIREMENTS},
		{"Provide hardware specification of development machines",DBConstants.DEVELOPER_REQUIREMENTS},
		{"Provide count of development machines",DBConstants.DEVELOPER_REQUIREMENTS},
		{"Provide IDEs that will be used",DBConstants.DEVELOPER_REQUIREMENTS},
		{"Provide details of other tools needed for development",DBConstants.DEVELOPER_REQUIREMENTS},
		{"Provide details on report rendering time limit", DBConstants.NON_FUNCTIONAL_REQUIREMENTS},
		{"Provide details on number of transactions supported by the system", DBConstants.NON_FUNCTIONAL_REQUIREMENTS},
		{"Number of concurrent users supported by the system", DBConstants.NON_FUNCTIONAL_REQUIREMENTS},
	};
	
	private static String FEASIBILITY_MASTER_DATA [][] = new String [][] {
		{"What existing resources can be used for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"What is the cost of using existing resources for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"What resources are required to be updated for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"What is the cost of upgrading resources for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"What new resources need to be procured for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"What is the cost of procuring new resources for this project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"Does your team have enough skills to work on the project?",DBConstants.TECHNICAL_FEASIBILITY},
		{"Why are customers supporting the solution?",DBConstants.OPERATIONAL_FEASIBILITY},
		{"Why are end users supporting the solution?",DBConstants.OPERATIONAL_FEASIBILITY},
		{"What are the features being added that will need user training?",DBConstants.OPERATIONAL_FEASIBILITY},
		{"Are all the requirements high priority requirements?",DBConstants.OPERATIONAL_FEASIBILITY},
		{"What is the cost of investigation phase?",DBConstants.ECONOMIC_FEASIBILITY},
		{"What is the cost of hardware?",DBConstants.ECONOMIC_FEASIBILITY},
		{"What is the cost of software?",DBConstants.ECONOMIC_FEASIBILITY},
		{"What is the cost of the team?",DBConstants.ECONOMIC_FEASIBILITY},
		{"How will organization get long term benefits from this project?",DBConstants.ECONOMIC_FEASIBILITY},
	};
	
	private static String MASTER_STAKEHOLDER_DATA [][] = new String [][] {
		{"Customer"},
		{"EndUser"}
	};
	
	private static boolean doesTableExist(String sTablename){
	    
		try {
			Connection conn = DBConnection.getConnection();
			
			if(conn == null)
		    {
		    	return false;
		    }
	        
		    DatabaseMetaData dbmd = conn.getMetaData();
	        ResultSet rs = (ResultSet) dbmd.getTables(null, null, sTablename.toUpperCase(),null);
	        
	        if (rs.next()) {
	        	return true;
	        }
       
		}
		catch (Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
        return false;
	}

	private static Boolean fillMasterData() {
		try {
			Connection conn = DBConnection.getConnection();
			
			String insertQuery = "insert into MASTER_QUESTION (Question, Type) values (?,?)";
			
			for(int index = 0; index < REQUIREMENT_MASTER_DATA.length; index++) {
				PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
				prepStatement.setString(1, REQUIREMENT_MASTER_DATA[index][0]);
				prepStatement.setString(2, REQUIREMENT_MASTER_DATA[index][1]);
				
				prepStatement.execute();
			}
			
			insertQuery = "insert into MASTER_QUESTION (Question, Type) values (?,?)";
			
			for(int index = 0; index < FEASIBILITY_MASTER_DATA.length; index++) {
				PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
				prepStatement.setString(1, FEASIBILITY_MASTER_DATA[index][0]);
				prepStatement.setString(2, FEASIBILITY_MASTER_DATA[index][1]);
				
				prepStatement.execute();
			}
			
			insertQuery = "insert into MASTER_STAKEHOLDER (Type) values (?)";
			
			for(int index = 0; index < MASTER_STAKEHOLDER_DATA.length; index++) {
				PreparedStatement prepStatement = conn.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
				prepStatement.setString(1,MASTER_STAKEHOLDER_DATA[index][0] );
				prepStatement.execute();
			}
			
			insertQuery = "insert into VERSION (Version) values (?)";
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setLong(1, 1);
			prepStatement.execute();
			
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return false;
	}
	
	public static Boolean createTables() {
		try {
			
			Connection conn = DBConnection.getConnection();
			
			if (doesTableExist(DBConstants.VERSION_TABLE) == false) {
				for (int index = 0; index < DDL_SCRIPT.length; index++) {
					conn.createStatement().execute(DDL_SCRIPT[index]);
				}
				
				fillMasterData();
			}
			else {
				PreparedStatement prepStatement = conn.prepareStatement("select * from VERSION");
				ResultSet rs = prepStatement.executeQuery();
				rs.next();
				
				long version = rs.getLong("Version");
			}
			
			DBConnection.close();
			
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
			return false;
		}
	}
}
