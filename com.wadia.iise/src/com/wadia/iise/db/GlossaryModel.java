package com.wadia.iise.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.wadia.iise.dataobjects.Glossary;
import com.wadia.iise.dataobjects.Project;

/*
 * Purpose: 
 * Interface to do CRUD on glossary related information in db.
 */

public class GlossaryModel  extends BaseModel {
	
	private static Glossary getGlossary(ResultSet rs) {
		try {
			Glossary glossary = new Glossary();
			glossary.name = rs.getString("Name");
			glossary.description = rs.getString("Description");
			glossary.project_id = rs.getLong("PROJECT_ID");
			glossary.id = rs.getLong("ID");
			glossary.cloudid = rs.getString("CLOUD_ID");
			if (glossary.cloudid == null) {
				glossary.cloudid = "";
			}
			
			glossary.synched = rs.getLong("Synched");
			
			Project project = ProjectModel.getProjectById(glossary.project_id);
			
			glossary.project_cloudid = project.cloudid;
			
			return glossary;
		}
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
				
		return null;
	}
	
	public static Boolean createGlossary(Long project_id,
									     String name,
										 String description) {

		String insertQuery = "Insert into GLOSSARY(Name,Description," + 
							 "PROJECT_ID) values(?,?,?)";
		
		try {
			Connection conn = DBConnection.getConnection();
			
			PreparedStatement prepStatement = conn.prepareStatement(insertQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, description);
			prepStatement.setLong(3, project_id);
			
			prepStatement.execute();
		
		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
			return false;
		}

		return true;
	}

	public static ArrayList<Glossary> getAllGlossary(Long projectID) {

		ArrayList<Glossary> listglossary = new ArrayList<Glossary>();
		
		try {
			Connection conn = DBConnection.getConnection();
			
			PreparedStatement prepStatement = conn.prepareStatement("select * from GLOSSARY where PROJECT_ID=?");
			prepStatement.setLong(1, projectID);
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Glossary glossary = getGlossary(rs);
				listglossary.add(glossary);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return listglossary;
	}
	
	public static ArrayList<Glossary> getAllNotSynchedGlossary() {

		ArrayList<Glossary> listglossary = new ArrayList<Glossary>();
		
		try {
			Connection conn = DBConnection.getConnection();
			
			PreparedStatement prepStatement = conn.prepareStatement("select * from GLOSSARY where Synched is NULL");
			
			ResultSet rs = prepStatement.executeQuery();
			
			while (rs.next()) {
				Glossary glossary = getGlossary(rs);
				listglossary.add(glossary);
			}
		} 
		catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	
		return listglossary;
	}

	
	public static void updateGlossary(Long id, 
									  String name,
									  String description) {

		String strUpdateQuery = "update GLOSSARY set Name=?, Description=?, Synched=NULL where ID=?";
			
		try {
			Connection conn = DBConnection.getConnection();
			PreparedStatement prepStatement = conn.prepareStatement(strUpdateQuery);
			prepStatement.setString(1, name);
			prepStatement.setString(2, description);
			prepStatement.setLong(3, id);
			
			prepStatement.execute();

		} catch (SQLException e) {
			DBExceptionHandler.handleException(e);
		}
	}
}

