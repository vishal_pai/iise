package com.wadia.iise.helper;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.richclientgui.toolbox.validation.string.StringValidationToolkit;
import com.wadia.iise.Activator;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.db.ProjectModel;
import com.wadia.iise.validations.RequiredTextFieldValidator;
import com.wadia.iise.validations.TextBoxValidator;
import com.wadia.iise.views.IEnableOKButton;

/*
 * Purpose: 
 * Other helper methods...
 */

public class Utility {
	
	public static void createMarker(final IProject project,
									final String path,
									final String message,
								    Display display) throws JavaModelException {
		
		display.syncExec(new Runnable() {
			
			@Override
			public void run() {
				try {
					
					IFile file1 = project.getWorkspace().getRoot().getFile(new Path(path));
								  
					IMarker marker1;
					
					if (file1 == null || file1.exists() == false) {
						return;
					}
					
					marker1 = file1.createMarker("com.wadia.iise.codesmellmarker");
					marker1.setAttribute(IMarker.LINE_NUMBER, 1);
					marker1.setAttribute(IMarker.MESSAGE, message);
					marker1.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void createMarker(final IProject project,
									final IPackageFragment[] packages,
									final String path1,
									final int lineNumber1,
								    final String path2,
								    final int lineNumber2,
								    final String message,
								    Display display) throws JavaModelException {
		
		display.syncExec(new Runnable() {
			
			@Override
			public void run() {
				try {
					
					IFile file1 = project.getWorkspace().getRoot().getFile(new Path(path1));
					IFile file2 = project.getWorkspace().getRoot().getFile(new Path(path2));
								  
					IMarker marker1;
					IMarker marker2;
					
					if (file1 == null || file1.exists() == false) {
						return;
					}
					
					marker1 = file1.createMarker("com.wadia.iise.codesmellmarker");
					marker1.setAttribute(IMarker.LINE_NUMBER, lineNumber1);
					marker1.setAttribute(IMarker.MESSAGE, message);
					marker1.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
					
					if (file2 == null || file2.exists() == false) {
						return;
					}
					
					marker2 = file2.createMarker("com.wadia.iise.codesmellmarker");
					marker2.setAttribute(IMarker.LINE_NUMBER, lineNumber2);
					marker2.setAttribute(IMarker.MESSAGE, message);
					marker2.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
					
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	}
	
	
	
	public static void createMarker(final ICompilationUnit unit, 
				 			        final String message, 
								    final int lineNumber,
								    Display display) {
		display.syncExec(new Runnable() {
			
			@Override
			public void run() {
				IMarker marker;
				try {
					marker = unit.getResource().createMarker("com.wadia.iise.codesmellmarker");
					marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
					marker.setAttribute(IMarker.MESSAGE, message);
					marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
	}
	
	//Gets project that are active in eclipse...
	//So that project filter in the ui contains only the projects that are active right now...
	public static ArrayList<Project> getProjectsActiveInEditor() {
		
		ArrayList<Project> projects = ProjectModel.getAllProjects();
		ArrayList<Project> filteredProjects = new ArrayList<Project>();
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
	    IWorkspaceRoot root = workspace.getRoot();
	    IProject[] workspaceProjects = root.getProjects();
	    
	    for (Project project: projects) {
	    	for (IProject workspaceProject : workspaceProjects) {
	    		if (project.name.equals(workspaceProject.getName())) {
	    			filteredProjects.add(project);
	    		}
	    	}
	    }
	    
	    return filteredProjects;
	}
	
	//Validating text box controls...
	public static Boolean isValid(ArrayList<TextBoxValidator> controlsToValidate) {
		for (TextBoxValidator validator: controlsToValidate) {
			if (validator.isValid() == false) {
				return false;
			}
		}
		
		return true;
	}
	
	//Validating the contents of text box
	public static boolean isNumeric(String str)  
	{  
		try  
		{  
		    Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
		    return false;  
		}  
		
		return true;  
	}
	
	public static Date getDateFromSWTDateTimeWidget(DateTime widget) {
		
		Calendar instance = Calendar.getInstance();
		instance.set(Calendar.DAY_OF_MONTH, widget.getDay());
		instance.set(Calendar.MONTH, widget.getMonth());
		instance.set(Calendar.YEAR, widget.getYear());
		
		return instance.getTime();
	}
	
	public static Text createFormFields(Composite area,
			  String labelText,
			  String initialText,
			  int minLength,
			  int maxLength,
			  IEnableOKButton dialogValidator,
			  StringValidationToolkit strValToolkit,
			  Boolean multiline,
			  Boolean isNumeric,
			  Boolean isPassword) {
		
		Label lblControl = new Label(area, SWT.NONE);
		lblControl.setText(labelText);
		
		Text txtControl = null;
		
		int options = SWT.BORDER;
		
		if (multiline == true) {
			options = options | SWT.MULTI | SWT.WRAP;
		}
		
		if (isPassword == true) {
			options = options | SWT.PASSWORD;
		}
		
		txtControl = new Text(area, options);
		
		GridData gd_txtApplicationServerCount = new GridData(SWT.FILL, SWT.FILL, true, false);
		
		if (multiline == true) {
			gd_txtApplicationServerCount.heightHint = 150;
		}
		
		txtControl.setLayoutData(gd_txtApplicationServerCount);
		
		strValToolkit.createField(txtControl, 
					  new RequiredTextFieldValidator(dialogValidator,
							  						 minLength,
							  						 maxLength,
							  						 isNumeric), 
					  true, 
					  initialText);
		
		return txtControl;
	}
	
	public static String getStackTraceFromException(Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
	
	public static ArrayList<String> splitLines(String txt) {
		BufferedReader rdr = new BufferedReader(new StringReader(txt));
		ArrayList<String> lines = new ArrayList<String>();
		
		try {
			for (String line = rdr.readLine(); line != null; line = rdr.readLine()) {
			    lines.add(line);
			}
			rdr.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			Activator.log(Utility.getStackTraceFromException(e), IStatus.ERROR);
		}
		
		return lines;
	}
}
