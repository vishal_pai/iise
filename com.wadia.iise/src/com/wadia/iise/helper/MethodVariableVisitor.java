package com.wadia.iise.helper;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class MethodVariableVisitor extends ASTVisitor{
	
	private ArrayList<String> method_tokens = new ArrayList<>();
	
	public boolean visit(VariableDeclarationFragment node) {
		method_tokens.add(node.getName().getFullyQualifiedName());
		return false;
	}
	
	public ArrayList<String> getMethodTokens() {
		return method_tokens;
	}
}
