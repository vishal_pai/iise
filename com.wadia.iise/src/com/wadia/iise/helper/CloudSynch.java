package com.wadia.iise.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Scanner;

public class CloudSynch {
	
	//Make http post call...
	public static String postData(String inputURL,
								  Map<String, String> nameValuePairs) throws IOException {
		URL url = new URL(inputURL);
		URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
	
		PrintWriter out = new PrintWriter(connection.getOutputStream());
	
		boolean first = true;
		for (Map.Entry<String, String> pair : nameValuePairs.entrySet())
		{	
			if (first) 
				first = false;
			else 
				out.print('&');
			String name = pair.getKey();
			String value = pair.getValue();
			out.print(name);
			out.print('=');
			out.print(URLEncoder.encode(value, "UTF-8"));
		}
	
		out.close();
	
		Scanner in;
		StringBuilder response = new StringBuilder();
		
		try
		{
			in = new Scanner(connection.getInputStream());
		}
		catch (IOException e)
		{
			if (!(connection instanceof HttpURLConnection)) throw e;
			InputStream err = ((HttpURLConnection) connection).getErrorStream();
			if (err == null) throw e;
			in = new Scanner(err);
		}
	
		while (in.hasNextLine())
		{
			response.append(in.nextLine());
		}
	
	    in.close();
	    return response.toString();
	}	
}
