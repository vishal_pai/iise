package com.wadia.iise.helper;

import java.awt.Color;
import java.awt.Dimension;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.apporiented.algorithm.clustering.AverageLinkageStrategy;
import com.apporiented.algorithm.clustering.Cluster;
import com.apporiented.algorithm.clustering.ClusteringAlgorithm;
import com.apporiented.algorithm.clustering.DefaultClusteringAlgorithm;
import com.apporiented.algorithm.clustering.visualization.DendrogramPanel;
import com.wadia.iise.dataobjects.MethodComplexity;
import com.wadia.iise.dataobjects.SourceField;
import com.wadia.iise.dataobjects.SourceMethod;
import com.wadia.iise.dataobjects.SourceToken;
import com.wadia.iise.dataobjects.SourceUnit;
import com.wadia.iise.db.SourceFieldModel;
import com.wadia.iise.db.SourceMethodModel;
import com.wadia.iise.db.SourceTokenModel;
import com.wadia.iise.db.SourceUnitModel;

/*
 * Purpose: 
 * View to create dendrogram...
 */

public class DendrogramViewer {
	private Long id;
	private Timestamp t;
	
	public DendrogramViewer(Long id, Timestamp t) {
		this.id = id;
		this.t = t;
	}
	
	public List<String> union(List<String> list1, List<String> list2) {
        Set<String> set = new HashSet<String>();

        set.addAll(list1);
        set.addAll(list2);

        return new ArrayList<String>(set);
    }

    public List<String> intersection(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<String>();

        for (String t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }
    
	public void showChart() {
		ArrayList<SourceUnit> units = SourceUnitModel.getAllSourceUnitsByTimeStamp(id, t);
		ArrayList<MethodComplexity> allMethods = SourceMethodModel.getAllSourceMethodsByTimeStamp(id, t);
		
		HashMap<Long, ArrayList<MethodComplexity>> methodsBySourceUnit = new HashMap<>();
		for (MethodComplexity method: allMethods) {
			ArrayList<MethodComplexity> methodNames = null;
			
			if (methodsBySourceUnit.containsKey(method.unitid)) {
				methodNames = methodsBySourceUnit.get(method.unitid);
			}
			else {
				methodNames = new ArrayList<>();
			}
			
			methodNames.add(method);
			methodsBySourceUnit.put(method.unitid,methodNames);
		}
		
		
		int size = units.size();
		
		HashMap<Long, ArrayList<String>> map = new HashMap<Long, ArrayList<String>>();
		
		String[] names = new String[size];
		
		for (int index = 0; index < size; index++) {
			names[index] = units.get(index).name;
			
			SourceUnit unit = units.get(index);
			ArrayList<String> tokenList = new ArrayList<String>();
			ArrayList<MethodComplexity> methods = methodsBySourceUnit.get(unit.id);
			
			if (methods != null)
			{	
				for (MethodComplexity method: methods) {
					
					tokenList.add(method.methodName);
					
					ArrayList<SourceToken> tokens = SourceTokenModel.getAllSourceToken(method.id);
					for (SourceToken token: tokens) {
						tokenList.add(token.name);
					}
				}
			}
			
			ArrayList<SourceField> fields = SourceFieldModel.getAllSourceField(unit.id);
			for (SourceField f: fields) {
				tokenList.add(f.name);
			}
			
			map.put(unit.id, tokenList);
		}
		
		double[][] distances = new double[size][size];
		for (int row = 0; row < size; row++) {
			
			SourceUnit unit1 = units.get(row);
			
			for (int col = 0; col < size; col++) {
				if (row == col) {
					distances[row][col] = 0;
				}
				else {
					SourceUnit unit2 = units.get(col);
					
					double intersection = intersection(map.get(unit1.id), map.get(unit2.id)).size();
					double union = union(map.get(unit1.id), map.get(unit2.id)).size();
					
					if (intersection == 0 || union == 0) {
						distances[row][col] = 2;
					}
					else {
						distances[row][col] = intersection / union;
					}
				}
			}
		}
				
		ClusteringAlgorithm alg = new DefaultClusteringAlgorithm();
		Cluster cluster = alg.performClustering(distances, names, new AverageLinkageStrategy());
		
		DendrogramPanel dp = new DendrogramPanel();
		dp.setModel(cluster);
		dp.setBackground(Color.WHITE);
        dp.setLineColor(Color.BLACK);
        dp.setScaleValueDecimals(0);
        dp.setScaleValueInterval(1);
        dp.setShowDistances(true);
        dp.setPreferredSize(new Dimension(size * 20,size * 20));
		
        JScrollPane pane = new JScrollPane(dp);
        dp.setAutoscrolls(true);
        
		JFrame frame = new JFrame();
		frame.setContentPane(pane);
		frame.setSize(400, 400);
		
		pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
		
        frame.setVisible(true);
	}
}
