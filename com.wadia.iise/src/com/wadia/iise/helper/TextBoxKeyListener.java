package com.wadia.iise.helper;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.wadia.iise.db.GlossaryModel;

/*
 * Purpose: 
 * On pressing ctrl + g in any dialog glossary should be automatically created...
 */

public class TextBoxKeyListener implements Listener {
	private Text control;
	private ArrayList<String> glossary;
	private Long id;
	
	public TextBoxKeyListener(Long id, Text control, ArrayList<String> glossary) {
		// TODO Auto-generated constructor stub
		this.control = control;
		this.glossary = glossary;
		this.id = id;
	}
	
	@Override
	public void handleEvent(Event event) {
		// TODO Auto-generated method stub
		if ((event.stateMask == SWT.CTRL) && (event.keyCode == 'g')) {
			
			String txt = control.getSelectionText();
			if (txt.length() > 0) {
				if (id > 0) {
					GlossaryModel.createGlossary(id, txt, "To be entered");
				}
				else {
					if (glossary.contains(txt) == false) {
						glossary.add(txt);
					}
				}
			}
		}
	}

}
