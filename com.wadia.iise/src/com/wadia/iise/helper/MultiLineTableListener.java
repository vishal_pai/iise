package com.wadia.iise.helper;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TableItem;

/*
 * Purpose: 
 * Official code to make table viewer in swt multiline. By default SWT tables are single line...
 */

public class MultiLineTableListener implements Listener {
    public MultiLineTableListener() {
	}
	
	public void handleEvent(Event event) {
        switch (event.type) {
	        case SWT.MeasureItem: {
	          TableItem item = (TableItem) event.item;
	          String text = getText(item, event.index);
	          Point size = event.gc.textExtent(text);
	          event.width = size.x;
	          event.height = Math.max(event.height, size.y);
	          break;
	        }
	        case SWT.PaintItem: {
	          TableItem item = (TableItem) event.item;
	          String text = getText(item, event.index);
	          Point size = event.gc.textExtent(text);
	          int offset2 = event.index == 0 ? Math.max(0, (event.height - size.y) / 2) : 0;
	          event.gc.drawText(text, event.x, event.y + offset2, true);
	          break;
	        }
	        case SWT.EraseItem: {
	          event.detail &= ~SWT.FOREGROUND;
	          break;
	        }
        }
    }
    
    String getText(TableItem item, int column) {
    	
        String text = item.getText(column);
        return text;
      }
}

