package com.wadia.iise.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.views.ViewFunctionalRequirements;

/*
 * Purpose: 
 * Handles request by user to view functional requirements.
 * Launches the dialog that shows the functional requirements...      
 */

public class ViewFunctionalRequirementsHandler  extends AbstractHandler{
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ArrayList<Project> projects = Utility.getProjectsActiveInEditor();
		
		if (projects.size() > 0) {
			ViewFunctionalRequirements dialog = new ViewFunctionalRequirements(window.getShell(), 
																			   projects,
																			   "Requirements");
			dialog.create();
			dialog.open();
		}
		else {
			
			MessageDialog.openInformation(
					window.getShell(),
					"IISE",
					"Please enter a project in the system before using this feature.");
		}
		
		return null;
	}
}
