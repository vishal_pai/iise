package com.wadia.iise.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.views.UIConstants;
import com.wadia.iise.views.ViewRequirements;

/*
 * Purpose: 
 * Handles request by user to view requirements.
 * Launches the dialog that shows the requirements...      
 */
public class ViewRequirementsHandler extends AbstractHandler{
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		String param = "";
		try {
			param = event.getCommand().getName();
		} catch (NotDefinedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ArrayList<Project> projects = Utility.getProjectsActiveInEditor();
		
		if (projects.size() > 0) {
			
			ViewRequirements dialog = null;
			
			//Based on the sub menu item selected appropriate dialog is shown...
			if (param.toLowerCase().contains("hardware")) {
				dialog = new ViewRequirements(window.getShell(), projects, DBConstants.HARDWARE_REQUIREMENTS, UIConstants.HARDWARE);
			}
			else if (param.toLowerCase().contains("software")) {
				dialog = new ViewRequirements(window.getShell(), projects, DBConstants.SOFTWARE_REQUIREMENTS, UIConstants.SOFTWARE);
			}
			else if (param.toLowerCase().contains("nonfunctional")) {
				dialog = new ViewRequirements(window.getShell(), projects, DBConstants.NON_FUNCTIONAL_REQUIREMENTS, UIConstants.NON_FUNCTIONAL);
			}
			else {
				dialog = new ViewRequirements(window.getShell(), projects, DBConstants.DEVELOPER_REQUIREMENTS, UIConstants.DEVELOPMENT);
			}
			
			dialog.create();
			dialog.open();
		}
		else {
			
			MessageDialog.openInformation(
					window.getShell(),
					"IISE",
					"Please enter a project in the system before using this feature.");
		}
		
		return null;
	}
}
