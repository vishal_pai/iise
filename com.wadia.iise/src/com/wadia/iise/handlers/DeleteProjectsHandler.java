package com.wadia.iise.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.DBConstants;
import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.views.UIConstants;
import com.wadia.iise.views.ViewDeleteProjects;
import com.wadia.iise.views.ViewFeasibility;

/*
 * Purpose: 
 * Handles request by user to view feasibility related information (economic, operational, technical).
 * Launches the appropriate feasibility dialog...      
 */

public class DeleteProjectsHandler  extends AbstractHandler{
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		String param = "";
		try {
			param = event.getCommand().getName();
		} catch (NotDefinedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ArrayList<Project> projects = Utility.getProjectsActiveInEditor();
		
		if (projects.size() > 0) {
			
			ViewDeleteProjects dialog = new ViewDeleteProjects(window.getShell(), projects);
			dialog.create();
			dialog.open();
		}
		else {
			
			MessageDialog.openInformation(
					window.getShell(),
					"IISE",
					"Please enter a project in the system before using this feature.");
		}
		
		return null;
	}
}
