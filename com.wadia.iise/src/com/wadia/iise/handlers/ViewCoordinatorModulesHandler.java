package com.wadia.iise.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.Project;
import com.wadia.iise.helper.Utility;
import com.wadia.iise.views.ViewProjectAnalysis;

/*
 * Purpose: 
 * Handles request by user to view coordinator and utility modules.
 * Launches the dialog that shows the coordinator and utility modules.   
 */

public class ViewCoordinatorModulesHandler extends AbstractHandler{

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		ArrayList<Project> projects = Utility.getProjectsActiveInEditor();
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		if (projects.size() > 0) {
		
			ViewProjectAnalysis dialog = new ViewProjectAnalysis(window.getShell(), projects, "Coordinator");
			dialog.create();
			dialog.open();
			return null;
		}
		else {
			MessageDialog.openInformation(
					window.getShell(),
					"IISE",
					"Please enter a project in the system before using this feature.");
		}
		
		return null;
	}

}
