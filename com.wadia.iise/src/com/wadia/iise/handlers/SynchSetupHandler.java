package com.wadia.iise.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.SynchSetup;
import com.wadia.iise.db.SynchSetupModel;
import com.wadia.iise.views.ViewSynchSetup;

public class SynchSetupHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		SynchSetup setup = SynchSetupModel.getSynchSetup();
		ViewSynchSetup dialog = new ViewSynchSetup(window.getShell(),setup);
		dialog.create();
		dialog.open();
		return null;
	}

}
