package com.wadia.iise.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.dataobjects.SynchSetup;
import com.wadia.iise.db.SynchSetupModel;
import com.wadia.iise.views.ViewSynchronizeData;

/*
 * Purpose: 
 * Handles request by user to synchronize local data with cloud.
 * Launches the synch with cloud view that takes care of synch.       
 */
public class SynchHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		SynchSetup setup = SynchSetupModel.getSynchSetup();
		if (setup != null) {
			ViewSynchronizeData dialog = new ViewSynchronizeData(window.getShell(),setup);
			dialog.create();
			dialog.open();
		}
		else {
			MessageDialog.openInformation(
					window.getShell(),
					"IISE",
					"Please setup the synch url.");
		}
		
		return null;
	}
}
