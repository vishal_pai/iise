package com.wadia.iise.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.wadia.iise.views.ViewProjects;

/*
 * Purpose: 
 * Handles request by user to view project information.
 * Launches the dialog that shows the projects...      
 */
public class ViewProjectsHandler extends AbstractHandler {

	public ViewProjectsHandler() {
		
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ViewProjects dialog = new ViewProjects(window.getShell());
		dialog.create();
		
		dialog.open();
		
		return null;
	}

}
