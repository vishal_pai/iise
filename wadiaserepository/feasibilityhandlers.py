import webapp2
import model
import json
import logging
import basehandler
import utilities

class FeasibilityHandler(basehandler.BaseHandler):
    def post(self):
    
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
            
        feasibilitys_json = json.loads(data)
        
        ids = ""
        
        feasibility = None
        feasibility_list = []
        for feasibility_json in feasibilitys_json:
            if feasibility_json['cloudid'] == '':
                #It could happen that during a previous synch data is put into the database but did not reach the client...
                #Hence this is a safety check...
                feasibility = model.Feasibility.query(model.Feasibility.createdby == email,
                                                      model.Feasibility.user_db_id == str(feasibility_json['id'])).get()
                if feasibility is None:
                    feasibility = model.Feasibility()
                    feasibility.createdby = email
                    feasibility.can_be_purged = 0
            else:
                feasibility = model.Feasibility.get_by_id(int(feasibility_json['cloudid']))
            
            project = model.Project.get_by_id(int(feasibility_json['project_cloudid']))
            
            feasibility.user_db_id = str(feasibility_json['id'])
            feasibility.question = feasibility_json['question']
            feasibility.description = feasibility_json['description']
            feasibility.type = feasibility_json['type']
            feasibility.projectkey = project.key
            feasibility.modifiedby = email
            
            feasibility_list.append(feasibility)
            
        self.response.write(utilities.save_multiple_objects(feasibility_list))
