from google.appengine.ext import ndb
import logging

def is_authorized_user(request, model):
    email = request.POST.get('username')
    password = request.POST.get('password')
    
    user = model.User.query(model.User.name == email, model.User.password == password).get()
    if user is None:
        return False
    
    return True
    
def save_multiple_objects(objects):
    keys = ndb.put_multi(objects)
    ids = ""
    index = 0
    for index in range(0,len(objects)):
        if ids <> "":
            ids += ","
        
        ids += str(objects[index].user_db_id)
        ids += ','
        ids += str(keys[index].id())
    
    logging.error(ids)
    return ids
    
def get_numberlines_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_lines,unit.num_tokens + unit.num_fields + unit.num_methods]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_lines
            dict_aggregates[unit.analysisdate][1] += unit.num_tokens + unit.num_fields + unit.num_methods
    return dict_aggregates    
    
    
def get_tokens_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_lines,unit.num_tokens + unit.num_fields + unit.num_methods]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_lines
            dict_aggregates[unit.analysisdate][1] += unit.num_tokens + unit.num_fields + unit.num_methods
    return dict_aggregates
    
    
def get_comments_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_comments]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_comments
    return dict_aggregates