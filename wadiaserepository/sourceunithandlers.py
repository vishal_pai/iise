import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class SourceUnitHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        units_json = json.loads(data)
        ids = ""
        unit = None
        unit_list = []
        for unit_json in units_json:
            if unit_json['cloudid'] == '':
                unit = model.SourceUnit.query(model.SourceUnit.user_db_id == str(unit_json['id']),
                                              model.SourceUnit.createdby == email,
                                              model.SourceUnit.analysisdate == unit_json['dateTime']).get()
                if unit is None:
                    unit = model.SourceUnit()
                    unit.createdby = email
                    unit.can_be_purged = 0
            else:
                unit = model.SourceUnit.get_by_id(int(unit_json['cloudid']))
            
            project = model.Project.get_by_id(int(unit_json['project_cloudid']))
            
            unit.user_db_id = str(unit_json['id'])
            unit.analysisdate = unit_json['dateTime']
            unit.name = unit_json['name']
            unit.path = unit_json['path']
            unit.num_lines = unit_json['numLines']
            unit.num_comments = unit_json['numComments']
            unit.num_blanks = unit_json['numBlanks']
            unit.num_methods = unit_json['numMethods']
            unit.num_fields = unit_json['numFields']
            unit.num_tokens = unit_json['numTokens']
            unit.caller_count = unit_json['callerCount']
            unit.callee_count = unit_json['calleeCount']
            
            unit.projectkey = project.key
            unit.modifiedby = email
            
            unit_list.append(unit)
            
        self.response.write(utilities.save_multiple_objects(unit_list))
