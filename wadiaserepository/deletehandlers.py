import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import constants
import basehandler
import utilities

class DeleteHandler(basehandler.BaseHandler):
    def post(self):
        
        if utilities.is_authorized_user(request, model) == False:
            return ""
            
        deletes_json = json.loads(data)
        
        ids = ""
        
        for entity_json in deletes_json:
        
            if ids <> "":
                ids += ","
        
            ids += str(entity_json['id'])
            ids += ','
            ids += str(entity_json['cloudid'])
            
            entity_to_be_deleted = None
            
            if entity_json['type'] == constants.TEAM_TABLE:
                entity_to_be_deleted = model.Team.get_by_id(int(entity_json['cloudid']))
            elif entity_json['type'] == constants.PROJECT_REQUIREMENT_TABLE:
                entity_to_be_deleted = model.FunctionalRequirement.get_by_id(int(entity_json['cloudid']))
            elif entity_json['type'] == constants.REQUIREMENT_TABLE:
                entity_to_be_deleted = model.Requirement.get_by_id(int(entity_json['cloudid']))
            elif entity_json['type'] == constants.FEASIBILITY_TABLE:
                entity_to_be_deleted = model.Feasibility.get_by_id(int(entity_json['cloudid']))
            elif entity_json['type'] == constants.GLOSSARY_TABLE:
                entity_to_be_deleted = model.Glossary.get_by_id(int(entity_json['cloudid']))
            
            try
                entity_to_be_deleted.key.delete()
            except:
                pass
        
        self.response.write(ids)
