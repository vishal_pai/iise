import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class GlossaryHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        logging.error(data)
        
        glossarys_json = json.loads(data)
        
        ids = ""
        glossary = None
        glossary_list = []
        for glossary_json in glossarys_json:
            if glossary_json['cloudid'] == '':
                glossary = model.Glossary.query(model.Glossary.createdby == email,
                                                model.Glossary.user_db_id == str(glossary_json['id'])).get()
                if glossary is None:
                    glossary = model.Glossary()
                    glossary.createdby = email
                    glossary.can_be_purged = 0
            else:
                glossary = model.Glossary.get_by_id(int(glossary_json['cloudid']))
            
            project = model.Project.get_by_id(int(glossary_json['project_cloudid']))
            
            glossary.user_db_id = str(glossary_json['id'])
            glossary.name = glossary_json['name']
            glossary.description = glossary_json['description']
            glossary.projectkey = project.key
            glossary.modifiedby = email
            
            glossary_list.append(glossary)
            
        self.response.write(utilities.save_multiple_objects(glossary_list))
