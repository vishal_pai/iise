import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class SourceFieldHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        fields_json = json.loads(data)
        field = None
        ids = ""
        field_list = []
        for field_json in fields_json:
            if field_json['cloudid'] == '':
                field = model.SourceField.query(model.SourceField.createdby == email,
                                                model.SourceField.analysisdate == field_json['dateTime'],
                                                model.SourceField.user_db_id == str(field_json['id'])).get()
                if field is None:
                    field = model.SourceField()
                    field.createdby = email
                    field.can_be_purged = 0
            else:
                field = model.SourceField.get_by_id(int(field_json['cloudid']))
            
            unit = model.SourceUnit.get_by_id(int(field_json['unit_cloudid']))
            
            field.user_db_id = str(field_json['id'])
            field.analysisdate = field_json['dateTime']
            field.name = field_json['name']
            field.unitkey = unit.key
            field.modifiedby = email
            
            field_list.append(field)
            
        self.response.write(utilities.save_multiple_objects(field_list))