import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class RequirementHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
            
        logging.error(data)
        
        requirements_json = json.loads(data)
        
        ids = ""
        requirement = None
        requirement_list = []
        for requirement_json in requirements_json:
            if requirement_json['cloudid'] == '':
                requirement = model.Requirement.query(model.Requirement.user_db_id == str(requirement_json['id']),
                                                      model.Requirement.createdby == email).get()
                
                if requirement is None:
                    requirement = model.Requirement()
                    requirement.createdby = email
                    requirement.can_be_purged = 0
            else:
                requirement = model.Requirement.get_by_id(int(requirement_json['cloudid']))
            
            project = model.Project.get_by_id(int(requirement_json['project_cloudid']))
            
            requirement.user_db_id = str(requirement_json['id'])
            requirement.question = requirement_json['question']
            requirement.details = requirement_json['details']
            requirement.type = requirement_json['type']
            requirement.projectkey = project.key
            requirement.modifiedby = email
            
            requirement_list.append(requirement)
            
        self.response.write(utilities.save_multiple_objects(requirement_list))