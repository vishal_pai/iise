from google.appengine.ext import ndb

class Audit(ndb.Model):
    user_db_id = ndb.StringProperty()
    can_be_purged = ndb.IntegerProperty()
    createdby = ndb.StringProperty()
    creationdate = ndb.DateTimeProperty(auto_now_add=True)
    modifiedby = ndb.StringProperty()
    modifieddate = ndb.DateTimeProperty(auto_now=True)

class User(ndb.Model):
    name = ndb.StringProperty()
    password = ndb.StringProperty()
    type = ndb.StringProperty()

class Project(Audit):
    name = ndb.StringProperty()
    problem = ndb.StringProperty()
    solution = ndb.StringProperty()
    start = ndb.StringProperty()
    end = ndb.StringProperty()

class Team(Audit):
    name = ndb.StringProperty()
    rollno = ndb.StringProperty()
    projectkey = ndb.KeyProperty()

class StakeHolder(Audit):
    name = ndb.StringProperty()
    type = ndb.StringProperty()
    projectkey = ndb.KeyProperty()

class FunctionalRequirement(Audit):
    name = ndb.StringProperty()
    description = ndb.StringProperty()
    projectkey = ndb.KeyProperty()    
    
class Feasibility(Audit):
    question = ndb.StringProperty()
    description = ndb.StringProperty()
    type = ndb.StringProperty()
    projectkey = ndb.KeyProperty()    

class Requirement(Audit):
    question = ndb.StringProperty()
    details = ndb.StringProperty()
    type = ndb.StringProperty()
    projectkey = ndb.KeyProperty()    
    
class Glossary(Audit):
    name = ndb.StringProperty()
    description = ndb.StringProperty()
    projectkey = ndb.KeyProperty()        

class MasterQuestion(Audit):
    question = ndb.StringProperty()
    type = ndb.StringProperty()
    
class SourceUnit(Audit):
    analysisdate = ndb.StringProperty()
    name = ndb.StringProperty()
    path = ndb.StringProperty()
    num_lines = ndb.IntegerProperty()
    num_comments = ndb.IntegerProperty()
    num_blanks = ndb.IntegerProperty()
    num_methods = ndb.IntegerProperty()
    num_fields = ndb.IntegerProperty()
    num_tokens = ndb.IntegerProperty()
    caller_count = ndb.IntegerProperty()
    callee_count = ndb.IntegerProperty()
    projectkey = ndb.KeyProperty()  

class SourceField(Audit):
    analysisdate = ndb.StringProperty()
    name = ndb.StringProperty()
    unitkey = ndb.KeyProperty()    

class SourceMethod(Audit):
    analysisdate = ndb.StringProperty()
    name = ndb.StringProperty()
    num_lines = ndb.IntegerProperty()
    num_comments = ndb.IntegerProperty()
    num_blanks = ndb.IntegerProperty()
    num_tokens = ndb.IntegerProperty()
    complexity = ndb.IntegerProperty()
    unitkey = ndb.KeyProperty()    

class SourceToken(Audit):
    analysisdate = ndb.StringProperty()
    name = ndb.StringProperty()
    methodkey = ndb.KeyProperty()    