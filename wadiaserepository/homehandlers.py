import model
import basehandler
import rolehelper
import logging
from datetime import datetime
import utilities
class MainHandler(basehandler.BaseHandler):
    def get(self):
        if rolehelper.is_loggedin(self.session):
            self.redirect('/landing')
        else:
            self.render_template('home.html')

class RegistrationHandler(basehandler.BaseHandler):
    def get(self):
        self.render_template('register.html')
    
    def post(self):
        email = self.request.POST.get('email')
        password = self.request.POST.get('password')
        
        user = model.User.query(model.User.name == email).get()
        if user is None:
            user = model.User(name=email, password=password, type='student')
            user.put()
            self.redirect('/')
        else:
            self.render_template('error.html', errormessage='User name already taken.')

class LoginHandler(basehandler.BaseHandler):
    def get(self):
        self.render_template('login.html')
    
    def post(self):  
        email = self.request.POST.get('email')
        password = self.request.POST.get('password')
        user = model.User.query(model.User.name == email, 
                                model.User.password == password, 
                                model.User.type == 'admin').get()
        if user is None:
            self.render_template('error.html', errormessage='User unauthorized or Invalid user name / password')
        else:
            self.session['loggedin'] = 1
            self.redirect('/landing')

class LogoutHandler(basehandler.BaseHandler):
    def get(self):
        self.session.clear()
        self.redirect('/')

class LineChartHandler(basehandler.BaseHandler):
    def get(self):
        if rolehelper.is_loggedin(self.session):
            dict_aggregates = {}
            
            chart_type = self.request.GET.get('type')
            project_id = int(self.request.GET.get('project_id'))
            project = model.Project.get_by_id(project_id)
            
            units = model.SourceUnit.query(model.SourceUnit.projectkey == project.key)
            
            if chart_type == 'number_of_lines':
                dict_aggregates = utilities.get_numberlines_dict(units)
            elif chart_type == 'number_of_tokens':
                dict_aggregates = utilities.get_tokens_dict(units)
            elif chart_type == 'number_of_comments':
                dict_aggregates = utilities.get_comments_dict(units)
            else:
                return
            
            flag = False
            strdata = '{"cols":[{"type":"datetime"},{"type":"number"}],"rows":['
            for key in dict_aggregates:
                
                a = datetime.strptime(key,'%Y-%m-%d %H:%M:%S.%f')
                
                if flag == True:
                    strdata += ','
                else:
                    flag = True
                    
                strdata += '{"c": [{ "v": "Date('
                strdata += str(a.year)
                strdata += ','
                strdata += str(a.month - 1) #Google visualization uses 0 as Jan
                strdata += ','
                strdata += str(a.day)
                strdata += ','
                strdata += str(a.hour)
                strdata += ','
                strdata += str(a.minute)
                strdata += ','
                strdata += str(a.second)
                strdata += ')"},{"v":'
                strdata += str(dict_aggregates[key][0])
                strdata += '} ]}'
                
            strdata += ']}'
            
            logging.error(strdata)
            self.response.write(strdata)

class LandingHandler(basehandler.BaseHandler):
    def get(self):
        if rolehelper.is_loggedin(self.session):
            projects = model.Project.query()
                    
            self.render_template('landing.html',projects=projects)
        else:
            self.render_template('error.html', errormessage='User unauthorized')
