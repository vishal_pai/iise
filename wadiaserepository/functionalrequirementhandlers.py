import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class FunctionalRequirementHandler(basehandler.BaseHandler):
    def post(self):
        
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
            
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        func_reqs_json = json.loads(data)
        ids = ""
        
        func_req = None
        func_req_list = []
        for func_req_json in func_reqs_json:
            if func_req_json['cloudid'] == '':
                func_req = model.FunctionalRequirement.query(model.FunctionalRequirement.createdby == email,
                                                             model.FunctionalRequirement.user_db_id == str(func_req_json['id'])).get()
                if func_req is None:
                    func_req = model.FunctionalRequirement()
                    func_req.createdby = email
                    func_req.can_be_purged = 0
            else:
                func_req = model.FunctionalRequirement.get_by_id(int(func_req_json['cloudid']))
            
            project = model.Project.get_by_id(int(func_req_json['project_cloudid']))
            
            func_req.user_db_id = str(func_req_json['id'])
            func_req.name = func_req_json['name']
            func_req.description = func_req_json['description']
            func_req.projectkey = project.key
            func_req.modifiedby = email
            
            func_req_list.append(func_req)
            
        self.response.write(utilities.save_multiple_objects(func_req_list))