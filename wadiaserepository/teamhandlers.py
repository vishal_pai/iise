import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class TeamHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        logging.error(data)
        
        teams_json = json.loads(data)
        
        ids = ""
        team = None
        team_list = []
        for team_json in teams_json:
            if team_json['cloudid'] == '':
                team = model.Team.query(model.Team.createdby == email, 
                                        model.Team.user_db_id == str(team_json['id'])).get()
                if team is None:
                    team = model.Team()
                    team.createdby = email
                    team.can_be_purged = 0
            else:
                team = model.Team.get_by_id(int(team_json['cloudid']))
            
            project = model.Project.get_by_id(int(team_json['project_cloudid']))
            
            team.user_db_id = str(team_json['id'])
            team.name = team_json['name']
            team.rollno = team_json['rollno']
            team.projectkey = project.key
            team.modifiedby = email
            
            team_list.append(team)
        
        self.response.write(utilities.save_multiple_objects(team_list))