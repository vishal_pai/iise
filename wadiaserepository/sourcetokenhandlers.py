import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class SourceTokenHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        tokens_json = json.loads(data)
        
        ids = ""
        
        token = None
        token_list = []
        for token_json in tokens_json:
            
            if token_json['cloudid'] == '':
                token = model.SourceToken.query(model.SourceToken.createdby == email,
                                                model.SourceToken.analysisdate == token_json['dateTime'],
                                                model.SourceToken.user_db_id == str(token_json['id'])).get()
                if token is None:
                    token = model.SourceToken()
                    token.createdby = email
                    token.can_be_purged = 0
            else:
                token = model.SourceToken.get_by_id(int(token_json['cloudid']))
            
            method = model.SourceMethod.get_by_id(int(token_json['method_cloudid']))
            
            token.user_db_id = str(token_json['id'])
            token.analysisdate = token_json['dateTime']
            token.name = token_json['name']
            token.methodkey = method.key
            token.modifiedby = email
            
            token_list.append(token)
        
        self.response.write(utilities.save_multiple_objects(token_list))