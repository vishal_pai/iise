import webapp2
import model
import json
import logging
import basehandler
import utilities

class StakeHolderHandler(basehandler.BaseHandler):
    def post(self):
    
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
            
        stakeholders_json = json.loads(data)
        
        ids = ""
        
        stakeholder = None
        stakeholder_list = []
        for stakeholder_json in stakeholders_json:
            if stakeholder_json['cloudid'] == '':
                #It could happen that during a previous synch data is put into the database but did not reach the client...
                #Hence this is a safety check...
                stakeholder = model.StakeHolder.query(model.StakeHolder.createdby == email,
                                                      model.StakeHolder.user_db_id == str(stakeholder_json['id'])).get()
                if stakeholder is None:
                    stakeholder = model.StakeHolder()
                    stakeholder.createdby = email
                    stakeholder.can_be_purged = 0
            else:
                stakeholder = model.StakeHolder.get_by_id(int(stakeholder_json['cloudid']))
            
            project = model.Project.get_by_id(int(stakeholder_json['project_cloudid']))
            
            stakeholder.user_db_id = str(stakeholder_json['id'])
            stakeholder.name = stakeholder_json['name']
            stakeholder.type = stakeholder_json['type']
            stakeholder.projectkey = project.key
            stakeholder.modifiedby = email
            
            stakeholder_list.append(stakeholder)
            
        self.response.write(utilities.save_multiple_objects(stakeholder_list))
