import webapp2

config = {}
config['webapp2_extras.sessions'] = {
    'secret_key': 'iiseesupersecretkey!@#$%',
}

app = webapp2.WSGIApplication([
    webapp2.Route('/', handler='homehandlers.MainHandler'),
    webapp2.Route('/register', handler='homehandlers.RegistrationHandler'),
    webapp2.Route('/login', handler='homehandlers.LoginHandler'),
    webapp2.Route('/logout', handler='homehandlers.LogoutHandler'),
    webapp2.Route('/landing', handler='homehandlers.LandingHandler'),
    webapp2.Route('/linechart', handler='homehandlers.LineChartHandler'),
    webapp2.Route('/project', handler='projecthandlers.ProjectHandler'),
    webapp2.Route('/team', handler='teamhandlers.TeamHandler'),
    webapp2.Route('/feasibility', handler='feasibilityhandlers.FeasibilityHandler'),
    webapp2.Route('/functionalrequirement', handler='functionalrequirementhandlers.FunctionalRequirementHandler'),
    webapp2.Route('/requirement', handler='requirementhandlers.RequirementHandler'),
    webapp2.Route('/glossary', handler='glossaryhandlers.GlossaryHandler'),
    webapp2.Route('/master', handler='masterquestionhandlers.MasterQuestionHandler'),
    webapp2.Route('/sourceunit', handler='sourceunithandlers.SourceUnitHandler'),
    webapp2.Route('/sourcefield', handler='sourcefieldhandlers.SourceFieldHandler'),
    webapp2.Route('/sourcemethod', handler='sourcemethodhandlers.SourceMethodHandler'),
    webapp2.Route('/sourcetoken', handler='sourcetokenhandlers.SourceTokenHandler'),
    webapp2.Route('/delete', handler='deletehandlers.DeleteHandler'),
    webapp2.Route('/stakeholder', handler='stakeholderhandlers.StakeHolderHandler')
], debug=True, config=config)
