import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class ProjectHandler(basehandler.BaseHandler):
    def post(self):
        
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        projects_json = json.loads(data)
        ids = ""
        
        project = None
        project_list = []
        for project_json in projects_json:
            if project_json['cloudid'] == '':
                #It could happen that during sync the project_json was put into database but response did not reach client...
                project = model.Project.query(model.Project.user_db_id == str(project_json['id']),
                                              model.Project.createdby == email).get()
                
                if project is None:
                    project = model.Project()
                    project.createdby = email
                    project.can_be_purged = 0
            else:
                project = model.Project.get_by_id(int(project_json['cloudid']))
            
            project.user_db_id = str(project_json['id'])
            project.name = project_json['name']
            project.problem = project_json['problem']
            project.solution = project_json['solution']
            project.start = project_json['startDate']
            project.end = project_json['endDate']
            project.modifiedby = email
            
            project_list.append(project)
            
        self.response.write(utilities.save_multiple_objects(project_list))