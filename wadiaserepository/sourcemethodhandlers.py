import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class SourceMethodHandler(basehandler.BaseHandler):
    def post(self):
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        methods_json = json.loads(data)
        
        ids = ""
        method = None
        method_list = []
        for method_json in methods_json:
            if method_json['cloudid'] == '':
                method = model.SourceMethod.query(model.SourceMethod.createdby == email,
                                                  model.SourceMethod.analysisdate == method_json['dateTime'],
                                                  model.SourceMethod.user_db_id == str(method_json['id'])).get()
                if method is None:
                    method = model.SourceMethod()
                    method.createdby = email
                    method.can_be_purged = 0
            else:
                method = model.SourceMethod.get_by_id(int(method_json['cloudid']))
            
            unit = model.SourceUnit.get_by_id(int(method_json['unit_cloudid']))
            
            method.user_db_id = str(method_json['id'])
            method.analysisdate = method_json['dateTime']
            method.name = method_json['name']
            method.num_lines = method_json['numLines']
            method.num_comments = method_json['numComments']
            method.num_blanks = method_json['numBlanks']
            method.num_tokens = method_json['numTokens']
            method.complexity = method_json['complexity']
            method.unitkey = unit.key
            method.modifiedby = email
            
            method_list.append(method)
            
        self.response.write(utilities.save_multiple_objects(method_list))