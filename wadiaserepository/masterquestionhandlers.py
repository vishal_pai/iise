import webapp2
import model
import json
from google.appengine.ext import ndb
import logging
import basehandler
import utilities

class MasterQuestionHandler(basehandler.BaseHandler):
    def post(self):
        
        if utilities.is_authorized_user(self.request, model) == False:
            return ""
        
        data = self.request.POST.get('data')
        email = self.request.POST.get('username')
        
        logging.error(data)
        
        questions_json = json.loads(data)
        question = None
        ids = ""
        question_list = []
        for question_json in questions_json:
            if question_json['cloudid'] == '':
                question = model.MasterQuestion.query(model.MasterQuestion.user_db_id == str(question_json['id']),
                                                      model.MasterQuestion.createdby == email).get()
                
                if question is None:
                    question = model.MasterQuestion()
                    question.createdby = email
                    question.can_be_purged = 0
                
            else:
                question = model.MasterQueston.get_by_id(int(question_json['cloudid']))
            
            question.user_db_id = str(question_json['id'])
            question.question = question_json['question']
            question.type = question_json['type']
            question.modifiedby = email
            
            question_list.append(question)
            
        self.response.write(utilities.save_multiple_objects(question_list))
