import json
from datetime import datetime
import utilities

def create_edit_sourcetokens(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    sourcetokens_json = json.loads(data)
    ids = ""
    
    sourcetoken = None
    sourcetokens = []
    for sourcetoken_json in sourcetokens_json:
        if sourcetoken_json['cloudid'] == '':
            #It could happen that during sync the sourcetoken was put into database but response did not reach client...
            sourcetoken = model.SourceToken.query.filter_by(user_db_id=str(sourcetoken_json['id']),
                                                            createdby=email, 
                                                            analysisdate=sourcetoken_json['dateTime']).first()
            
            if sourcetoken is None:
                sourcetoken = model.SourceToken()
                sourcetoken.createdby = email
                sourcetoken.creationdate = dt
                sourcetoken.synched = 0
                sourcetoken.cloud_id = ""
        else:
            sourcetoken = model.MasterQuestion.query.filter_by(id=int(sourcetoken_json['cloudid'])).first()
        
        unit = model.SourceMethod.query.filter_by(id=int(sourcetoken_json['method_cloudid'])).first()
        
        sourcetoken.user_db_id = sourcetoken_json['id']
        sourcetoken.analysisdate = sourcetoken_json['dateTime']
        sourcetoken.name = sourcetoken_json['name']
        sourcetoken.modifiedby = email
        sourcetoken.modifieddate = dt
        sourcetoken.unit_id = unit.id
        
        sourcetokens.append(sourcetoken)
    return utilities.save_multiple_objects(model, sourcetokens)