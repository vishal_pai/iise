import json
from datetime import datetime
import utilities

def create_edit_masterquestions(request, model):
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    masterquestions_json = json.loads(data)
    ids = ""
    
    masterquestion = None
    masterquestions = []
    for masterquestion_json in masterquestions_json:
        if masterquestion_json['cloudid'] == '':
            #It could happen that during sync the masterquestion was put into database but response did not reach client...
            masterquestion = model.MasterQuestion.query.filter_by(user_db_id=str(masterquestion_json['id']),
                                                                  createdby=email).first()
            
            if masterquestion is None:
                masterquestion = model.MasterQuestion()
                masterquestion.createdby = email
                masterquestion.creationdate = dt
                masterquestion.synched = 0
                masterquestion.cloud_id = ""
        else:
            masterquestion = model.MasterQuestion.query.filter_by(id=int(masterquestion_json['cloudid'])).first()
        
        masterquestion.user_db_id = masterquestion_json['id']
        masterquestion.question = masterquestion_json['question']
        masterquestion.type = masterquestion_json['type']
        masterquestion.modifiedby = email
        masterquestion.modifieddate = dt
        
        masterquestions.append(masterquestion)
    return utilities.save_multiple_objects(model, masterquestions)