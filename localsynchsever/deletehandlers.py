import json
from datetime import datetime
import utilities
import constants

def deletedata(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    
    dt = datetime.now()
    
    list_delete = json.loads(data)
    ids = ""
    
    for entity in list_delete:
    
        if ids <> "":
            ids += ","
    
        ids += str(entity['id'])
        ids += ','
        ids += str(entity['cloudid'])
        
        e = None
        
        if entity['type'] == constants.TEAM_TABLE:
            e = model.Team.query.filter_by(id=int(entity['cloudid'])).first()
        elif entity['type'] == constants.PROJECT_REQUIREMENT_TABLE:
            e = model.FunctionalRequirement.query.filter_by(id=int(entity['cloudid'])).first()
        elif entity['type'] == constants.REQUIREMENT_TABLE:
            e = model.Requirement.query.filter_by(id=int(entity['cloudid'])).first()
        elif entity['type'] == constants.FEASIBILITY_TABLE:
            e = model.Feasibility.query.filter_by(id=int(entity['cloudid'])).first()
        elif entity['type'] == constants.GLOSSARY_TABLE:
            e = model.Glossary.query.filter_by(id=int(entity['cloudid'])).first()
        
        try:
            model.db.session.delete(e)
            model.db.session.commit()
        except:
            pass
            
    return ids
