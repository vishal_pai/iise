import json
from datetime import datetime
import utilities

def create_edit_functionalrequirements(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    functionalrequirements_json = json.loads(data)
    ids = ""
    
    functionalrequirement = None
    funcreqs = []
    for functionalrequirement_json in functionalrequirements_json:
        if functionalrequirement_json['cloudid'] == '':
            #It could happen that during sync the functionalrequirement was put into database but response did not reach client...
            functionalrequirement = model.FunctionalRequirement.query.filter_by(user_db_id=str(functionalrequirement_json['id']),
                                              createdby=email).first()
            
            if functionalrequirement is None:
                functionalrequirement = model.FunctionalRequirement()
                functionalrequirement.createdby = email
                functionalrequirement.creationdate = dt
                functionalrequirement.synched = 0
                functionalrequirement.cloud_id = ""
        else:
            functionalrequirement = model.FunctionalRequirement.query.filter_by(id=int(functionalrequirement_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(functionalrequirement_json['project_cloudid'])).first()
        
        functionalrequirement.user_db_id = functionalrequirement_json['id']
        functionalrequirement.name = functionalrequirement_json['name']
        functionalrequirement.description = functionalrequirement_json['description']
        functionalrequirement.modifiedby = email
        functionalrequirement.modifieddate = dt
        functionalrequirement.project_id = project.id
        
        funcreqs.append(functionalrequirement)
    
    return utilities.save_multiple_objects(model,funcreqs)