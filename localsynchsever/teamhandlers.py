import json
from datetime import datetime
import utilities

def create_edit_teammember(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    teams_json = json.loads(data)
    ids = ""
    
    team = None
    teams = []
    
    for team_json in teams_json:
        if team_json['cloudid'] == '':
            #It could happen that during sync the team was put into database but response did not reach client...
            team = model.Team.query.filter_by(user_db_id=str(team_json['id']),
                                              createdby=email).first()
            
            if team is None:
                team = model.Team()
                team.createdby = email
                team.creationdate = dt
                team.synched = 0
                team.cloud_id = ""
        else:
            team = model.Team.query.filter_by(id=int(team_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(team_json['project_cloudid'])).first()
        
        team.user_db_id = team_json['id']
        team.name = team_json['name']
        team.rollno = team_json['rollno']
        team.modifiedby = email
        team.modifieddate = dt
        team.project_id = project.id
        
        teams.append(team)
    return utilities.save_multiple_objects(model, teams)