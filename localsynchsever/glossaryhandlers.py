import json
from datetime import datetime
import utilities

def create_edit_glossary(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    glossarys_json = json.loads(data)
    ids = ""
    
    glossary = None
    glossarys = []
    for glossary_json in glossarys_json:
        if glossary_json['cloudid'] == '':
            #It could happen that during sync the glossary was put into database but response did not reach client...
            glossary = model.Glossary.query.filter_by(user_db_id=str(glossary_json['id']),
                                                      createdby=email).first()
            
            if glossary is None:
                glossary = model.Glossary()
                glossary.createdby = email
                glossary.creationdate = dt
                glossary.synched = 0
                glossary.cloud_id = ""
        else:
            glossary = model.Glossary.query.filter_by(id=int(glossary_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(glossary_json['project_cloudid'])).first()
        
        glossary.user_db_id = glossary_json['id']
        glossary.name = glossary_json['name']
        glossary.description = glossary_json['description']
        glossary.modifiedby = email
        glossary.modifieddate = dt
        glossary.project_id = project.id
        
        glossarys.append(glossary)
    return utilities.save_multiple_objects(model, glossarys)