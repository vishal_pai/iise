import json
from datetime import datetime
import utilities

def create_edit_feasibility(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    feasibilitys_json = json.loads(data)
    ids = ""
    
    feasibility = None
    feasiblities = []
    for feasibility_json in feasibilitys_json:
        if feasibility_json['cloudid'] == '':
            #It could happen that during sync the feasibility was put into database but response did not reach client...
            feasibility = model.Feasibility.query.filter_by(user_db_id=str(feasibility_json['id']),
                                                            createdby=email).first()
            
            if feasibility is None:
                feasibility = model.Feasibility()
                feasibility.createdby = email
                feasibility.creationdate = dt
                feasibility.synched = 0
                feasibility.cloud_id = ""
        else:
            feasibility = model.Feasibility.query.filter_by(id=int(feasibility_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(feasibility_json['project_cloudid'])).first()
        
        feasibility.user_db_id = feasibility_json['id']
        feasibility.question = feasibility_json['question']
        feasibility.description = feasibility_json['description']
        feasibility.type = feasibility_json['type']
        feasibility.modifiedby = email
        feasibility.modifieddate = dt
        feasibility.project_id = project.id
        
        feasiblities.append(feasibility)
        
    return utilities.save_multiple_objects(model, feasiblities)