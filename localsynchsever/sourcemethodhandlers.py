import json
from datetime import datetime
import utilities

def create_edit_sourcemethods(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    sourcemethods_json = json.loads(data)
    ids = ""
    
    sourcemethod = None
    sourcemethods = []
    for sourcemethod_json in sourcemethods_json:
        if sourcemethod_json['cloudid'] == '':
            #It could happen that during sync the sourceunit was put into database but response did not reach client...
            sourcemethod = model.SourceMethod.query.filter_by(user_db_id=str(sourcemethod_json['id']),
                                                              createdby=email, 
                                                              analysisdate=sourcemethod_json['dateTime']).first()
            
            if sourcemethod is None:
                sourcemethod = model.SourceMethod()
                sourcemethod.createdby = email
                sourcemethod.creationdate = dt
                sourcemethod.synched = 0
                sourcemethod.cloud_id = ""
        else:
            sourcemethod = model.MasterQuestion.query.filter_by(id=int(sourcemethod_json['cloudid'])).first()
        
        unit = model.SourceUnit.query.filter_by(id=int(sourcemethod_json['unit_cloudid'])).first()
        
        sourcemethod.user_db_id = sourcemethod_json['id']
        sourcemethod.analysisdate = sourcemethod_json['dateTime']
        sourcemethod.name = sourcemethod_json['name']
        sourcemethod.num_lines = sourcemethod_json['numLines']
        sourcemethod.num_comments = sourcemethod_json['numComments']
        sourcemethod.num_blanks = sourcemethod_json['numBlanks']
        sourcemethod.num_tokens = sourcemethod_json['numTokens']
        sourcemethod.complexity = sourcemethod_json['complexity']
        sourcemethod.modifiedby = email
        sourcemethod.modifieddate = dt
        sourcemethod.unit_id = unit.id
        
        sourcemethods.append(sourcemethod)
        
    return utilities.save_multiple_objects(model, sourcemethods)