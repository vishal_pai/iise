import json
from datetime import datetime
import utilities

def create_edit_sourceunits(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']    
    
    dt = datetime.now()
    
    sourceunits_json = json.loads(data)
    ids = ""
    
    sourceunit = None
    units = []
    for sourceunit_json in sourceunits_json:
        if sourceunit_json['cloudid'] == '':
            #It could happen that during sync the sourceunit was put into database but response did not reach client...
            sourceunit = model.SourceUnit.query.filter_by(user_db_id=str(sourceunit_json['id']),
                                                          createdby=email, 
                                                          analysisdate=sourceunit_json['dateTime']).first()
            
            if sourceunit is None:
                sourceunit = model.SourceUnit()
                sourceunit.createdby = email
                sourceunit.creationdate = dt
                sourceunit.synched = 0
                sourceunit.cloud_id = ""
        else:
            sourceunit = model.MasterQuestion.query.filter_by(id=int(sourceunit_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(sourceunit_json['project_cloudid'])).first()
        
        sourceunit.user_db_id = sourceunit_json['id']
        sourceunit.analysisdate = sourceunit_json['dateTime']
        sourceunit.name = sourceunit_json['name']
        sourceunit.path = sourceunit_json['path']
        sourceunit.num_lines = sourceunit_json['numLines']
        sourceunit.num_comments = sourceunit_json['numComments']
        sourceunit.num_blanks = sourceunit_json['numBlanks']
        sourceunit.num_methods = sourceunit_json['numMethods']
        sourceunit.num_fields = sourceunit_json['numFields']
        sourceunit.num_tokens = sourceunit_json['numTokens']
        sourceunit.caller_count = sourceunit_json['callerCount']
        sourceunit.callee_count = sourceunit_json['calleeCount']
        sourceunit.modifiedby = email
        sourceunit.modifieddate = dt
        sourceunit.project_id = project.id
        
        units.append(sourceunit)
    return utilities.save_multiple_objects(model, units)