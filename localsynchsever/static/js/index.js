function onSurveyChange(surveyid) {

    if (surveyid == 0) {
        $('#idscores').html('');
        return;
    }
    
    var responseText = $.ajax({
                                async: false,
                                url: '/vendorscoresummary',
                                type: 'GET',
                                data: { 'surveyid': surveyid}
                            }).responseText;
    
    $('#idscores').html(responseText);
}