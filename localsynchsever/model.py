from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import generate_password_hash,check_password_hash

db=SQLAlchemy()

class Audit():
    synched = db.Column(db.Integer)
    user_db_id = db.Column(db.String(100))
    cloud_id = db.Column(db.String(100))
    createdby = db.Column(db.String(200))
    creationdate = db.Column(db.DateTime)
    modifiedby = db.Column(db.String(200))
    modifieddate = db.Column(db.DateTime)
    
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cloud_id = db.Column(db.String(100))
    email = db.Column(db.String(100),unique=True)
    password =  db.Column(db.String(200))
    type = db.Column(db.String(20))
    synched = db.Column(db.Integer)
    user_db_id = db.Column(db.String(100))
    
    def __init__(self,email,password,type):
        self.email = email
        self.password = password
        self.type = type
        
class Project(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    problem = db.Column(db.String(5000))
    solution = db.Column(db.String(5000))
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    team_members = db.relationship('Team',backref='project',lazy='dynamic')
    funcrequirements = db.relationship('FunctionalRequirement',backref='project',lazy='dynamic')
    feasibility_list = db.relationship('Feasibility',backref='project',lazy='dynamic')
    glossary_list = db.relationship('Glossary',backref='project',lazy='dynamic')
    requirements = db.relationship('Requirement',backref='project',lazy='dynamic')
    sourceunits = db.relationship('SourceUnit',backref='project',lazy='dynamic')
    
class Team(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    rollno = db.Column(db.String(20))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))

class StakeHolder(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    type = db.Column(db.String(100))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))

class FunctionalRequirement(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    description = db.Column(db.String(5000))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))
    
class Feasibility(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(500))
    description = db.Column(db.String(5000))
    type = db.Column(db.String(100))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))   

class Requirement(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(500))
    details = db.Column(db.String(5000))
    type = db.Column(db.String(100))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))  
    
class Glossary(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500))
    description = db.Column(db.String(5000))
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))        

class MasterQuestion(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(500))
    type = db.Column(db.String(100))
    
class SourceUnit(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    analysisdate = db.Column(db.DateTime)
    name = db.Column(db.String(500))
    path = db.Column(db.String(1000))
    num_lines = db.Column(db.Integer)
    num_comments = db.Column(db.Integer)
    num_blanks = db.Column(db.Integer)
    num_methods = db.Column(db.Integer)
    num_fields = db.Column(db.Integer)
    num_tokens = db.Column(db.Integer)
    caller_count = db.Column(db.Integer)
    callee_count = db.Column(db.Integer)
    project_id = db.Column(db.Integer,db.ForeignKey('project.id'))    
    source_fields = db.relationship('SourceField',backref='sourceunit',lazy='dynamic')
    source_methods = db.relationship('SourceMethod',backref='sourceunit',lazy='dynamic')
    
class SourceField(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    analysisdate = db.Column(db.DateTime)
    name = db.Column(db.String(500))
    unit_id = db.Column(db.Integer,db.ForeignKey('source_unit.id'))      

class SourceMethod(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    analysisdate = db.Column(db.DateTime)
    name = db.Column(db.String(500))
    num_lines = db.Column(db.Integer)
    num_comments = db.Column(db.Integer)
    num_blanks = db.Column(db.Integer)
    num_tokens = db.Column(db.Integer)
    complexity = db.Column(db.Integer)
    unit_id = db.Column(db.Integer,db.ForeignKey('source_unit.id'))    
    method_tokens = db.relationship('SourceToken',backref='sourcemethod',lazy='dynamic')
    
class SourceToken(db.Model, Audit):
    id = db.Column(db.Integer, primary_key=True)
    analysisdate = db.Column(db.DateTime)
    name = db.Column(db.String(500))
    method_id = db.Column(db.Integer,db.ForeignKey('source_method.id'))       
    