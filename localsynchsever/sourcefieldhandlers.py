import json
from datetime import datetime
import utilities

def create_edit_sourcefields(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    sourcefields_json = json.loads(data)
    ids = ""
    
    sourcefield = None
    sourcefields = []
    for sourcefield_json in sourcefields_json:
        if sourcefield_json['cloudid'] == '':
            #It could happen that during sync the sourceunit was put into database but response did not reach client...
            sourcefield = model.SourceField.query.filter_by(user_db_id=str(sourcefield_json['id']),
                                                            createdby=email, 
                                                            analysisdate=sourcefield_json['dateTime']).first()
            
            if sourcefield is None:
                sourcefield = model.SourceField()
                sourcefield.createdby = email
                sourcefield.creationdate = dt
                sourcefield.synched = 0
                sourcefield.cloud_id = ""
        else:
            sourcefield = model.MasterQuestion.query.filter_by(id=int(sourcefield_json['cloudid'])).first()
        
        unit = model.SourceUnit.query.filter_by(id=int(sourcefield_json['unit_cloudid'])).first()
        
        sourcefield.user_db_id = sourcefield_json['id']
        sourcefield.analysisdate = sourcefield_json['dateTime']
        sourcefield.name = sourcefield_json['name']
        sourcefield.modifiedby = email
        sourcefield.modifieddate = dt
        sourcefield.unit_id = unit.id
        
        sourcefields.append(sourcefield)
    return utilities.save_multiple_objects(model, sourcefields)