import json
from datetime import datetime
import utilities

def create_edit_requirements(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    requirements_json = json.loads(data)
    ids = ""
    
    requirement = None
    requirements = []
    for requirement_json in requirements_json:
        if requirement_json['cloudid'] == '':
            #It could happen that during sync the requirement was put into database but response did not reach client...
            requirement = model.Requirement.query.filter_by(user_db_id=str(requirement_json['id']),
                                                            createdby=email).first()
            
            if requirement is None:
                requirement = model.Requirement()
                requirement.createdby = email
                requirement.creationdate = dt
                requirement.synched = 0
                requirement.cloud_id = ""
        else:
            requirement = model.Requirement.query.filter_by(id=int(requirement_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(requirement_json['project_cloudid'])).first()
        
        requirement.user_db_id = requirement_json['id']
        requirement.question = requirement_json['question']
        requirement.details = requirement_json['details']
        requirement.type = requirement_json['type']
        requirement.modifiedby = email
        requirement.modifieddate = dt
        requirement.project_id = project.id
        
        requirements.append(requirement)
    return utilities.save_multiple_objects(model, requirements)