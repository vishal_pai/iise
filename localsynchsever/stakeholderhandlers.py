import json
from datetime import datetime
import utilities

def create_edit_stakeholders(request, model):
    print "enter stakeholder"
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    print "auth done in stakeholder"
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    stakeholders_json = json.loads(data)
    ids = ""
    
    stakeholder = None
    stakeholders = []
    for stakeholder_json in stakeholders_json:
        if stakeholder_json['cloudid'] == '':
            #It could happen that during sync the stakeholder was put into database but response did not reach client...
            stakeholder = model.StakeHolder.query.filter_by(user_db_id=str(stakeholder_json['id']),
                                                            createdby=email).first()
            
            if stakeholder is None:
                stakeholder = model.StakeHolder()
                stakeholder.createdby = email
                stakeholder.creationdate = dt
                stakeholder.synched = 0
                stakeholder.cloud_id = ""
        else:
            stakeholder = model.StakeHolder.query.filter_by(id=int(stakeholder_json['cloudid'])).first()
        
        project = model.Project.query.filter_by(id=int(stakeholder_json['project_cloudid'])).first()
        
        stakeholder.user_db_id = stakeholder_json['id']
        stakeholder.name = stakeholder_json['name']
        stakeholder.type = stakeholder_json['type']
        stakeholder.modifiedby = email
        stakeholder.modifieddate = dt
        stakeholder.project_id = project.id
        
        stakeholders.append(stakeholder)
        
    return utilities.save_multiple_objects(model, stakeholders)