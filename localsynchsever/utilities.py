def is_authorized_user(request, model):

    email = request.form['username']
    password = request.form['password']
    
    user = model.User.query.filter_by(email=email,password=password).first()
    if user is None:
        return False
    
    return True
    
    
def save_multiple_objects(model, objects):
    model.db.session.add_all(objects)
    model.db.session.commit()
    ids = ""
    
    for object in objects:
        if ids <> "":
            ids += ","
        
        ids += str(object.user_db_id)
        ids += ','
        ids += str(object.id)
    
    print "printing ids"
    print ids
    return ids
    
def get_numberlines_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_lines,unit.num_tokens + unit.num_fields + unit.num_methods]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_lines
            dict_aggregates[unit.analysisdate][1] += unit.num_tokens + unit.num_fields + unit.num_methods
    return dict_aggregates    
    
    
def get_tokens_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_lines,unit.num_tokens + unit.num_fields + unit.num_methods]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_lines
            dict_aggregates[unit.analysisdate][1] += unit.num_tokens + unit.num_fields + unit.num_methods
    return dict_aggregates
    
    
def get_comments_dict(units):    
    dict_aggregates = {}
    for unit in units:
        if unit.analysisdate not in dict_aggregates:
            dict_aggregates[unit.analysisdate] = [unit.num_comments]
        else:
            dict_aggregates[unit.analysisdate][0] += unit.num_comments
    return dict_aggregates