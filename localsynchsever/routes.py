import os
from flask import Flask, render_template, session, request, redirect, Response
from model import db
import model
from sqlalchemy.sql import func
import projecthandlers
import teamhandlers
import functionalrequirementhandlers
import feasibilityhandlers
import requirementhandlers
import deletehandlers
import masterquestionhandlers
import sourcefieldhandlers
import sourcemethodhandlers
import sourcetokenhandlers
import sourceunithandlers
import glossaryhandlers
import stakeholderhandlers
import utilities
from datetime import datetime

app=Flask(__name__)
app.secret_key = 'rfhHkjaihsfjasldmUHll'
app.config['SQLALCHEMY_DATABASE_URI']='postgresql://vpai:vpai@localhost/iise'
db.app = app
db.init_app(app) 

app.debug=True

@app.route('/',methods=['GET'])
def HomePage():
    if request.method == 'GET':
        return render_template('/home.html')

@app.route('/register',methods=['GET','POST'])
def RegistrationHandler():
    if request.method == 'GET':
        return render_template('register.html')
    else:
        email = request.form['email']
        password = request.form['password']
        
        user = model.User.query.filter_by(email=email).first()
        if user is None:
            user = model.User(email=email, password=password, type='student')
            model.db.session.add(user)
            model.db.session.commit()
            
            return redirect('/')
        else:
            return render_template('error.html', errormessage='User name already taken.')

@app.route('/login',methods=['GET','POST'])
def LoginHandler():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        email = request.form['email']
        password = request.form['password']
        
        user = model.User.query.filter_by(email = email).filter_by(password = password).filter_by(type = 'admin').first()
        if user is None:
            return render_template('error.html', errormessage='User unauthorized or Invalid user name / password')
        else:
            return redirect('/landing')

@app.route('/landing',methods=['GET'])
def LandingHandler():
    if request.method == 'GET':
        projects = model.Project.query.all()
        return render_template('landing.html', projects=projects)
        
@app.route('/logout',methods=['GET'])            
def LogoutHandler():
    if request.method == 'GET':
        session.clear()
        return redirect('/')

@app.route('/linechart',methods=['GET'])                    
def LineChartHandler():
    if request.method == 'GET':
        dict_aggregates = {}
        
        chart_type = request.args.get('type')
        project_id = request.args.get('project_id')
        project = model.Project.query.filter_by(id=int(project_id)).first()
        
        units = model.SourceUnit.query.filter_by(project_id = project.id).all()
        
        if chart_type == 'number_of_lines':
            dict_aggregates = utilities.get_numberlines_dict(units)
        elif chart_type == 'number_of_tokens':
            dict_aggregates = utilities.get_tokens_dict(units)
        elif chart_type == 'number_of_comments':
            dict_aggregates = utilities.get_comments_dict(units)
        else:
            return
        
        flag = False
        strdata = '{"cols":[{"type":"datetime"},{"type":"number"}],"rows":['
        for key in dict_aggregates:
            
            a = key
            
            if flag == True:
                strdata += ','
            else:
                flag = True
                
            strdata += '{"c": [{ "v": "Date('
            strdata += str(a.year)
            strdata += ','
            strdata += str(a.month - 1) #Google visualization uses 0 as Jan
            strdata += ','
            strdata += str(a.day)
            strdata += ','
            strdata += str(a.hour)
            strdata += ','
            strdata += str(a.minute)
            strdata += ','
            strdata += str(a.second)
            strdata += ')"},{"v":'
            strdata += str(dict_aggregates[key][0])
            strdata += '} ]}'
            
        strdata += ']}'
        
        print strdata
        return strdata
        
@app.route('/project',methods=['POST'])
def ProjectHandler():
    if request.method == 'POST':
        return projecthandlers.create_edit_project(request, model)
        
@app.route('/team',methods=['POST'])
def TeamHandler():
    if request.method == 'POST':
        return teamhandlers.create_edit_teammember(request, model)

@app.route('/functionalrequirement',methods=['POST'])
def FunctionalRequirementHandler():
    if request.method == 'POST':
        return functionalrequirementhandlers.create_edit_functionalrequirements(request, model)

@app.route('/feasibility',methods=['POST'])
def FeasibilityHandler():
    if request.method == 'POST':
        return feasibilityhandlers.create_edit_feasibility(request, model)

@app.route('/requirement',methods=['POST'])
def RequirementHandler():
    if request.method == 'POST':
        return requirementhandlers.create_edit_requirements(request, model)

@app.route('/glossary',methods=['POST'])
def GlossaryHandler():
    if request.method == 'POST':
        return glossaryhandlers.create_edit_glossary(request, model)

@app.route('/master',methods=['POST'])
def MasterHandler():
    if request.method == 'POST':
        return masterquestionhandlers.create_edit_masterquestions(request, model)

@app.route('/sourceunit',methods=['POST'])
def SourceUnitHandler():
    if request.method == 'POST':
        return sourceunithandlers.create_edit_sourceunits(request, model)

@app.route('/sourcefield',methods=['POST'])
def SourceFieldHandler():
    if request.method == 'POST':
        return sourcefieldhandlers.create_edit_sourcefields(request, model)
        
@app.route('/sourcemethod',methods=['POST'])
def SourceMethodHandler():
    if request.method == 'POST':
        return sourcemethodhandlers.create_edit_sourcemethods(request, model)
        
@app.route('/sourcetoken',methods=['POST'])
def SourceTokenHandler():
    if request.method == 'POST':
        return sourcetokenhandlers.create_edit_sourcetokens(request, model)

@app.route('/stakeholder',methods=['POST'])
def StakeHolderHandler():
    if request.method == 'POST':
        return stakeholderhandlers.create_edit_stakeholders(request, model)

@app.route('/delete',methods=['POST'])
def DeleteHandler():
    if request.method == 'POST':
        return deletehandlers.deletedata(request, model)
        
if __name__ == '__main__':  
    db.create_all()
    app.run(debug=True)
  