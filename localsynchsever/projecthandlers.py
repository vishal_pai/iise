import json
from datetime import datetime
import utilities

def create_edit_project(request, model):
    
    if utilities.is_authorized_user(request, model) == False:
        return ""
    
    data = request.form['data']
    email = request.form['username']
    
    dt = datetime.now()
    
    projects_json = json.loads(data)
    ids = ""
    
    project = None
    projects = []
    for project_json in projects_json:
        if project_json['cloudid'] == '':
            #It could happen that during sync the project was put into database but response did not reach client...
            project = model.Project.query.filter_by(user_db_id=str(project_json['id']),
                                                    createdby=email).first()
            
            if project is None:
                project = model.Project()
                project.createdby = email
                project.creationdate = dt
                project.synched = 0
                project.cloud_id = ""
        else:
            project = model.Project.query.filter_by(id=int(project_json['cloudid'])).first()
        
        project.user_db_id = project_json['id']
        project.name = project_json['name']
        project.problem = project_json['problem']
        project.solution = project_json['solution']
        project.start = project_json['startDate']
        project.end = project_json['endDate']
        project.modifiedby = email
        project.modifieddate = dt
        
        projects.append(project)
        
        
    return utilities.save_multiple_objects(model, projects)